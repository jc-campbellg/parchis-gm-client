/// @description Exit

var canExit = show_question("¿Seguro que queres salirte?");

if (canExit) {
	game_end();
}