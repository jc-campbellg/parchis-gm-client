{
    "id": "6dbf7a21-6baa-4c56-9a4d-d6de422eab52",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_exitButton",
    "eventList": [
        {
            "id": "cf25844f-714d-4e5c-af34-eeb432161bf7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "6dbf7a21-6baa-4c56-9a4d-d6de422eab52"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9ed91033-04fc-4694-95d0-51258a8cc712",
    "visible": true
}