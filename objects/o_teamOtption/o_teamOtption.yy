{
    "id": "20c617c6-2c66-4d83-a283-7d0ed35c4288",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_teamOtption",
    "eventList": [
        {
            "id": "ce8cb8ab-1403-4e39-9c26-6b95724d3a6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "20c617c6-2c66-4d83-a283-7d0ed35c4288"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "48b6aa04-6a5e-4320-b8d2-df69a953cbb6",
    "visible": true
}