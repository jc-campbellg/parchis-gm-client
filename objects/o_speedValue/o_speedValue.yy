{
    "id": "d9c6a65a-63c0-4ff7-bfe9-fc573ba3f6b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_speedValue",
    "eventList": [
        {
            "id": "872692ac-e807-42e8-996f-9e393ba17019",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "d9c6a65a-63c0-4ff7-bfe9-fc573ba3f6b0"
        },
        {
            "id": "291db430-771f-4dbe-a737-509705eede0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9c6a65a-63c0-4ff7-bfe9-fc573ba3f6b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "50741659-0207-4830-8919-420291b8db30",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "675af782-986b-4c04-bda0-87d1ce2528d9",
    "visible": true
}