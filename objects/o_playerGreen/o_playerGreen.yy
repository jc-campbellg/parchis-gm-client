{
    "id": "045bc002-c627-46ee-ade4-3a1fc02b79c0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_playerGreen",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "c58c782e-7e42-44b9-adeb-26c9e392a866",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
            "propertyId": "ad040125-eb05-47ac-810b-0d21ecc310b5",
            "value": "gameColor.green"
        },
        {
            "id": "99334e79-a0e0-4b4d-9a75-0f464dd6f3de",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
            "propertyId": "9b19b49a-e6c0-43ca-8360-7b5ba43c84bc",
            "value": "$FF4FA606"
        }
    ],
    "parentObjectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0a4cf9e7-04ad-4798-bf17-1ef1a8f2fbbc",
    "visible": true
}