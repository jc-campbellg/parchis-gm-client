{
    "id": "4e7090ce-37e5-4b63-b810-017367b2e892",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_playerBlue",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "8cb422bd-ef9a-405c-85c9-1cc339497b99",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
            "propertyId": "ad040125-eb05-47ac-810b-0d21ecc310b5",
            "value": "gameColor.blue"
        },
        {
            "id": "306be0de-df80-467f-9211-af0c56268257",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
            "propertyId": "9b19b49a-e6c0-43ca-8360-7b5ba43c84bc",
            "value": "$FFE2AC2B"
        }
    ],
    "parentObjectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0997a1b2-dbb4-46e0-bdf9-a11c5ecb3833",
    "visible": true
}