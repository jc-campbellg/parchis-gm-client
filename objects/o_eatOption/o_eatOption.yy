{
    "id": "61a14066-bf3d-46ba-b3b6-91485518dede",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_eatOption",
    "eventList": [
        {
            "id": "c22a6bab-c20b-4f45-b5e5-f8d0b6a75441",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "61a14066-bf3d-46ba-b3b6-91485518dede"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9904674b-7b99-4700-bcbd-60b4830466e8",
    "visible": true
}