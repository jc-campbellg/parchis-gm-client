{
    "id": "3481481f-9c28-4859-992d-7b8c732359ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_playerPurple",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "95db5464-9174-4c83-8a19-ffea0b7d19c3",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
            "propertyId": "ad040125-eb05-47ac-810b-0d21ecc310b5",
            "value": "gameColor.purple"
        },
        {
            "id": "a79b279a-22a7-423c-8bea-bce6b0281cea",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
            "propertyId": "9b19b49a-e6c0-43ca-8360-7b5ba43c84bc",
            "value": "$FF8C0DEB"
        }
    ],
    "parentObjectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bf718da7-56e1-4a10-884a-673f6d32577d",
    "visible": true
}