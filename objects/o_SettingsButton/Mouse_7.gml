var show = true;
if (layer_get_visible("instancesSettings")) {
	show = false;
}
layer_set_visible("instancesSettings", show);
layer_set_visible("backgroundWhiteBoard", show);
layer_set_visible("instancesPlayers", !show);