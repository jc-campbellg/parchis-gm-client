{
    "id": "3fc1bbef-e8d2-4642-9046-0f06e3d0b6d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_goalOption",
    "eventList": [
        {
            "id": "86e0c2bb-0343-429c-9baf-9abfa2c5cd56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "3fc1bbef-e8d2-4642-9046-0f06e3d0b6d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9c56e56c-4e52-44c2-8fa1-6146602c3d96",
    "visible": true
}