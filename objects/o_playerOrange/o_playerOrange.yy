{
    "id": "5a2bb80f-d4a2-434d-a191-9af75f8831f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_playerOrange",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "4a78563e-aff7-4bff-a3a1-f183decd093c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
            "propertyId": "ad040125-eb05-47ac-810b-0d21ecc310b5",
            "value": "gameColor.orange"
        },
        {
            "id": "b76ef373-af46-4d72-8e0b-e5d5906cda77",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
            "propertyId": "9b19b49a-e6c0-43ca-8360-7b5ba43c84bc",
            "value": "$FF2467F2"
        }
    ],
    "parentObjectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3dcb5d2a-bad2-4f45-a9a5-9481b8c7b045",
    "visible": true
}