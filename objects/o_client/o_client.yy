{
    "id": "7816349a-6dba-4bb3-b65d-8077fffc63f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_client",
    "eventList": [
        {
            "id": "6ebc87b3-bb49-4ae0-85f1-eb4a5d9ba9f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7816349a-6dba-4bb3-b65d-8077fffc63f7"
        },
        {
            "id": "7e3e343e-8ba6-449c-9353-e7070c79a060",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "7816349a-6dba-4bb3-b65d-8077fffc63f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0eb1a118-aaf8-46ac-804b-b97c50ea780a",
    "visible": false
}