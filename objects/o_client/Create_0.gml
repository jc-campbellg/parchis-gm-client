/// @description Connect to server
client = network_create_socket(network_socket_ws);
isOnline = network_connect(client, "127.0.0.1", 6510);
clientBuffer = buffer_create(2048, buffer_fixed, 1);

if (isOnline < 0) {
	show_message("No pude conectarme al servidor.");
	room_goto_previous();
	instance_destroy(self);
}