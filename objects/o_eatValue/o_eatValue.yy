{
    "id": "e95a66ee-8fff-432d-9d10-7d97bac03536",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_eatValue",
    "eventList": [
        {
            "id": "d0d1f7d7-f8a0-49de-b2cd-92827f07c4a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "e95a66ee-8fff-432d-9d10-7d97bac03536"
        },
        {
            "id": "d2f1eb0c-4923-402c-b2ef-872d2b3e22cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e95a66ee-8fff-432d-9d10-7d97bac03536"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "61a14066-bf3d-46ba-b3b6-91485518dede",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a70aa1a4-6670-4ead-bfb9-1fd942c9dbd2",
    "visible": true
}