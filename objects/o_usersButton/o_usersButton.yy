{
    "id": "7a6de8f0-8c99-4ecf-a73c-9fbe9c929922",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_usersButton",
    "eventList": [
        {
            "id": "204fbe9d-ad75-4374-b6e3-d8ee2850ee4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "7a6de8f0-8c99-4ecf-a73c-9fbe9c929922"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6b2e14b7-51a8-4749-bdb9-9c4212711d7a",
    "visible": true
}