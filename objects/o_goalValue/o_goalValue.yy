{
    "id": "bd5a0fa2-15a9-43ad-9f6b-62de487b2ba8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_goalValue",
    "eventList": [
        {
            "id": "61acd5cf-77b7-4180-8ff8-ddc7a21aa5d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "bd5a0fa2-15a9-43ad-9f6b-62de487b2ba8"
        },
        {
            "id": "58349ddf-5ff1-4093-ab64-9351e184fde4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd5a0fa2-15a9-43ad-9f6b-62de487b2ba8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3fc1bbef-e8d2-4642-9046-0f06e3d0b6d3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a70aa1a4-6670-4ead-bfb9-1fd942c9dbd2",
    "visible": true
}