{
    "id": "9fcdf286-57b1-4c85-a56e-28c550158eea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_turnOption",
    "eventList": [
        {
            "id": "4a82f51b-a219-45c2-945b-1884cf415945",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "9fcdf286-57b1-4c85-a56e-28c550158eea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4173582f-2896-4fc5-b278-43d18989486b",
    "visible": true
}