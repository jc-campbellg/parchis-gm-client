if (mouse_check_button_released(mb_left)) {
	var objectsInClick = ds_list_create();
	
	var n = 0;

	while (instance_find(o_piece, n)) {
		var inst = instance_find(o_piece, n);
		if (position_meeting(mouse_x, mouse_y, inst)) {
			ds_list_add(objectsInClick, inst);
		}
	    n = n + 1;
	}
	
	var size = ds_list_size(objectsInClick);
	var lastDepth = 0;
	var objectToClick = noone;
	for (var i = 0; i < size; i++) {
		var check = objectsInClick[| i];
		if (check.depth < lastDepth) {
			lastDepth = check.depth;
			objectToClick = check;
		}
	}

	with (o_piece) {
		selected = false;
		image_index = 0;
	}

	if (objectToClick) {
		with(objectToClick) {
			if (myColor == playerColor) {
				selected = true;
				image_index = 1;
			}
		}
	}
	
	ds_list_destroy(objectsInClick);
}