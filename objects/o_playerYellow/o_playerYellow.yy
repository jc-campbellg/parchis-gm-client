{
    "id": "37965b5d-2efb-4caf-9ecb-38e57725a38d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_playerYellow",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "e5a60b8d-125d-4846-9776-43d7d4311646",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
            "propertyId": "ad040125-eb05-47ac-810b-0d21ecc310b5",
            "value": "gameColor.yellow"
        },
        {
            "id": "818dde92-bc52-408c-b5fd-627d4fae501c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
            "propertyId": "9b19b49a-e6c0-43ca-8360-7b5ba43c84bc",
            "value": "$FF23EEFC"
        }
    ],
    "parentObjectId": "7c232bec-f0b3-459a-860e-7b05bc69b473",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "58155437-6b70-45f2-836c-e589798b754c",
    "visible": true
}