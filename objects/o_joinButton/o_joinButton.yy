{
    "id": "7005286c-ae53-4736-b334-76a3c56df322",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_joinButton",
    "eventList": [
        {
            "id": "6b13fc13-1f3b-4ef6-8a73-f5b8d599a7c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "7005286c-ae53-4736-b334-76a3c56df322"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f956c7de-d6ac-45a9-918f-8c499d6ffef3",
    "visible": true
}