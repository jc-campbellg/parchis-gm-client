/// @description Ask for IP
ip = get_string("IP Address:", ip);

port = get_string("Port:", port);

playerName = string_copy(get_string("Tu nombre", playerName), 0, 9);

if (string_length(playerName) < 3) {
	show_message("Tu nombre es muy corto");
} else {
	// Connect
	room_goto_next();
}