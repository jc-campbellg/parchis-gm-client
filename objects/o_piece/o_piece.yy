{
    "id": "48916e4d-edbe-43ee-bbcc-5ba271381a0a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_piece",
    "eventList": [
        {
            "id": "22f4b19b-bf22-470a-bf37-ff1b0d96c04e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "48916e4d-edbe-43ee-bbcc-5ba271381a0a"
        },
        {
            "id": "627b8011-f290-448b-af07-15b207174a64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "48916e4d-edbe-43ee-bbcc-5ba271381a0a"
        },
        {
            "id": "bbaf123a-3e27-462d-96b9-c3dae6d81496",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "48916e4d-edbe-43ee-bbcc-5ba271381a0a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "37b141a9-7e49-4f2a-b193-efd68dc8f31c",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "image_speed",
            "varType": 0
        },
        {
            "id": "d989d4ad-d8a8-4341-8917-b9491bf8cb2b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "selected",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "e0825460-c9b0-4eed-a03b-9df9ba10244e",
    "visible": true
}