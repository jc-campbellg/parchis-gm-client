/// @description set color
myNumber = get_instance_number(o_piece);
switch (myNumber) {
    case 0:
	case 1:
	case 2:
	case 3:
        sprite_index = s_pieceRed;
		myColor = gameColor.red;
        break;
	case 4:
	case 5:
	case 6:
	case 7:
        sprite_index = s_pieceBlue;
		myColor = gameColor.blue;
        break;
	case 8:
	case 9:
	case 10:
	case 11:
        sprite_index = s_pieceYellow;
		myColor = gameColor.yellow;
        break;
	case 12:
	case 13:
	case 14:
	case 15:
        sprite_index = s_pieceGreen;
		myColor = gameColor.green;
        break;
	case 16:
	case 17:
	case 18:
	case 19:
        sprite_index = s_piecePurple;
		myColor = gameColor.purple;
        break;
	case 20:
	case 21:
	case 22:
	case 23:
        sprite_index = s_pieceOrange;
		myColor = gameColor.orange;
        break;
}