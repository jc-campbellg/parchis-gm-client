draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_set_font(ft_warning);

draw_set_color(c_black);
draw_set_alpha(opacity/2);
draw_rectangle(0, y-80, 1066, y+80, false);

switch (showPlayer) {
	case showPlayerName.atStart:
		draw_set_color(c_white);
		draw_set_alpha(opacity);
        draw_text(x, y-60, name);
	case showPlayerName.atEnd:
    case showPlayerName.dontShow:
		draw_set_color(c_white);
		draw_set_alpha(opacity);
        draw_text(x, y, message);
		if showPlayer != showPlayerName.atEnd then break;

		draw_set_color(colorRGB);
		draw_set_alpha(opacity);
        draw_text(x, y+60, name);
        break;
}
draw_set_alpha(1);
draw_set_valign(fa_top);
draw_set_halign(fa_left);