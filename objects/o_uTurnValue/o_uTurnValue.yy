{
    "id": "92e8893c-7d5b-4e77-b71d-d0d128dce0a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_uTurnValue",
    "eventList": [
        {
            "id": "972908a3-27c3-46c3-aed8-fc48fddce36a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "92e8893c-7d5b-4e77-b71d-d0d128dce0a6"
        },
        {
            "id": "33b160e7-17d9-4683-908c-aa71b51a78e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "92e8893c-7d5b-4e77-b71d-d0d128dce0a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9fcdf286-57b1-4c85-a56e-28c550158eea",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6f4a178e-46e6-44e4-ab46-40dcf284ee38",
    "visible": true
}