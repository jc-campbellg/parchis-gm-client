{
    "id": "7311c12f-8628-4dc8-b348-8324a07cd9b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_teamValue",
    "eventList": [
        {
            "id": "85f79bdf-cb8d-4105-82b1-6a5da27fe8ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "7311c12f-8628-4dc8-b348-8324a07cd9b0"
        },
        {
            "id": "196ab156-7e6b-44d8-9237-1054f0c6632a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7311c12f-8628-4dc8-b348-8324a07cd9b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20c617c6-2c66-4d83-a283-7d0ed35c4288",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6f4a178e-46e6-44e4-ab46-40dcf284ee38",
    "visible": true
}