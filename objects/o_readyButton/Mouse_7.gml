/// @description Send ready
for (var i = 0; i < 6; ++i) {
    var p = playerInstances[i];
	if (p.isYou) {
		p.ready = true;
		send_ready(p.buttonColor);
	}
}