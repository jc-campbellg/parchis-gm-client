{
    "id": "e5034456-e213-4a18-9d1a-0e04870c8572",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_readyButton",
    "eventList": [
        {
            "id": "c38de296-8bf0-4395-a99d-ab6d4779cf33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "e5034456-e213-4a18-9d1a-0e04870c8572"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "721082fc-3d44-4fa3-a6c7-67b723635623",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "image_speed",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "9d28cf79-5402-4ed3-ba3f-0685f4179d91",
    "visible": true
}