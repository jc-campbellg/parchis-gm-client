{
    "id": "0ae06df4-7f9f-4b8f-a810-089cfc3211ad",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "ft_regular",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Tahoma",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "67057c9e-0d09-4afe-8aee-10d727f85f88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8dc1d633-b291-4715-b11f-e2c2fa33b324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 447,
                "y": 54
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "91932609-eaba-4048-a586-bdbaba0c9e97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 452,
                "y": 54
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5b0567ed-5e0f-4eca-b81b-61f92d95d1d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 460,
                "y": 54
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7c63ac94-a7b3-4d25-8c45-1897cf760b68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 474,
                "y": 54
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e35e0ee6-eb3b-4d02-a8db-b2a32a394b26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 486,
                "y": 54
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "89a098ab-26c8-45fc-96b5-0737cdaa3c7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "508c40a2-080e-4551-9434-740efc3860b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 18,
                "y": 80
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "69aed5ba-3a93-4f43-8a17-0314fe47bac2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 24,
                "y": 80
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "263548ca-6f09-44ee-b1b1-b88c348533c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 32,
                "y": 80
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cd9b6229-7d16-4269-94a9-fc6199e4e460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 53,
                "y": 80
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6935f87c-81a0-43cb-afa9-bc85af23dcd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 151,
                "y": 80
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4d69d806-bb85-4b4e-b239-304619e93ef5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 64,
                "y": 80
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b6023e0f-fba7-4dc2-b791-2aacd87dc08e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 71,
                "y": 80
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8a456bb2-0f67-40d3-b2b8-6886ab557004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 80,
                "y": 80
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "cbce903d-a15a-41f4-950c-6e5a0ab7d4e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 85,
                "y": 80
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "613c5137-e9a4-44bb-95c3-e788824e59f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 95,
                "y": 80
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a5fc3f93-062a-4fd5-a520-b3fc3d1beec2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 107,
                "y": 80
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e561bef8-628f-4424-9372-c40383d45dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 116,
                "y": 80
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "beb20458-fb1b-41cb-8ea0-5f18a01dac62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 127,
                "y": 80
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a74fb98c-7043-4151-b436-b7c518aef632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 139,
                "y": 80
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "24876350-65a8-43ce-bc21-099da6c2de73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 436,
                "y": 54
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3affc628-2299-4e74-80e5-708cbc9b0694",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 41,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "40c51ce7-37ab-4ef8-963d-7577450e72f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 424,
                "y": 54
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cd7f983d-46dc-454b-94a5-8c6b81bb4771",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 265,
                "y": 54
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d3a1b641-dc3a-4f49-8d4b-2f3617015fc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 159,
                "y": 54
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7f48dda8-f010-414b-8c25-32593395ce49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 171,
                "y": 54
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2389e2a7-bff8-4b3f-8635-1537e97343ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 176,
                "y": 54
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "92042a0c-720f-40fe-8612-5f261ce6ccf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 183,
                "y": 54
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "84e1e544-72d3-4014-8e3a-90890df10561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 196,
                "y": 54
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e8f09a1e-99c5-4d2e-9d02-e187e4165898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 210,
                "y": 54
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "da451022-0f5a-42b5-94cc-e3ff989b589d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 222,
                "y": 54
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c7237f6e-562e-44f1-a3cb-c69700309050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 232,
                "y": 54
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "09f59f1d-dfb5-4f77-9f77-dc68a931d28e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 250,
                "y": 54
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ae31699b-1bc3-4d0e-8569-45a4eaa7b419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 277,
                "y": 54
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1aaa8b66-5b75-47b7-8259-423e21fcedd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 395,
                "y": 54
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "74c53965-5566-49ef-ad3d-78538bd8acca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 289,
                "y": 54
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1087e5a1-a377-4b22-bbcb-64e6093b5755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 303,
                "y": 54
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "56a2cd21-08fa-44e8-a5f3-7dfa9eec730a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 315,
                "y": 54
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ee4541a3-d5fc-43f6-879a-e0729ed71721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 326,
                "y": 54
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "eb214bdf-99e7-4690-8560-e0249f59d043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 340,
                "y": 54
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "aded0738-ba41-41df-bb57-11cf89aedd63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 353,
                "y": 54
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6dcd0303-6068-49ac-9d33-ba93da93032f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 362,
                "y": 54
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a1d7911a-69db-4b91-87f6-641d885f39ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 371,
                "y": 54
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4446078d-b439-423f-989a-fe22dfbc1ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 384,
                "y": 54
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a1406378-eb15-4c55-be74-54ba64d98149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 409,
                "y": 54
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2b17ba6c-09b8-4c36-81ac-c54126880c32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 165,
                "y": 80
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "555fe93a-f278-4189-839c-f7ce0cb646c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 178,
                "y": 80
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c9819ddd-da08-486e-952b-514224162b82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 193,
                "y": 80
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6f982206-0610-4692-88b0-7c0f9ecc9d02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 457,
                "y": 80
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ea77fbc6-97d4-4e97-9e27-488669c58c50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 472,
                "y": 80
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "94c64d4c-9bac-48ee-8802-7abe419211e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 486,
                "y": 80
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2c00da50-c6bf-493d-af95-6e4b9578b9b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b4d911cf-0731-4d58-98c5-f9358c896d38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 17,
                "y": 106
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "aa7c07b3-31c4-4eee-aeb0-18d8b6002cc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 30,
                "y": 106
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "64be04f3-a8e1-4672-85fb-afbd7736e5af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 45,
                "y": 106
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8957847f-5742-4809-866d-45ffcc5d27c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 64,
                "y": 106
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "70b189fa-c2ba-4158-a4c8-db4846710412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 77,
                "y": 106
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0e6da832-6e68-41f2-be2b-7758d186ce8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 92,
                "y": 106
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "913e8463-b788-4b6d-aac9-0dd5f3b4de81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 105,
                "y": 106
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d91f247c-038e-4e60-9268-7cc4b3b62a69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 113,
                "y": 106
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f20ebb95-1906-4083-aea4-54e0f80983ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 123,
                "y": 106
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "193274d8-5aae-4556-b3b1-57b8622093df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 131,
                "y": 106
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "6922dafd-c343-48de-9638-c1b5d8fbd732",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 145,
                "y": 106
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f3e0f860-ba08-4cc0-8b60-eb83391d95dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 159,
                "y": 106
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a5699f8f-2f84-4832-9967-724a6d6d8c7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 166,
                "y": 106
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "06f8dec7-df0a-433e-b31c-2e3b15379a0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 177,
                "y": 106
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6d3f6dd9-aaaf-43ef-8118-3b59513cca7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 188,
                "y": 106
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a3de0fff-f960-4f4a-8c95-b75a9ef29b30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 199,
                "y": 106
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "536ca217-fcde-41ef-af05-e298a63423af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 211,
                "y": 106
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e25dcf60-94c5-435e-b767-2a7395a2cb66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 448,
                "y": 80
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6f6581e1-a5ec-4e61-83e5-711e03596569",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 436,
                "y": 80
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f7581db6-8ef4-499a-a5c0-f7e5472e2de3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 425,
                "y": 80
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "74e8f08c-6795-44c8-8f53-73d61ce8c0f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 299,
                "y": 80
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "059e8151-03d7-4f76-beb1-077b5cf16061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 205,
                "y": 80
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ee290dfe-d0df-4284-a8a5-fd54beec0201",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 213,
                "y": 80
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8bcf6a32-aef9-4d2b-8bca-a4d670ce8c34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 224,
                "y": 80
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c7acc5ac-7b9f-4723-9aa5-baf5a44997dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 229,
                "y": 80
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1ad40d88-0245-4dee-834a-f62eb745101d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 245,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f9ec379a-2b53-4f5f-a4af-3d48226c54b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 256,
                "y": 80
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6a9e48b3-ec60-4127-87e9-2534bd7a4966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 268,
                "y": 80
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "09797e50-bd9f-4e32-87d5-6cdbd7e66ed0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 279,
                "y": 80
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "51274773-9e30-416f-8627-918e7459aed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 291,
                "y": 80
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "81f41226-f359-4128-bb72-794d9bde573c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 304,
                "y": 80
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3de21d24-addc-476a-9fbb-aecc93999a7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 416,
                "y": 80
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c46b6097-1694-438f-b51d-fed05c05fb22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 315,
                "y": 80
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c718ad6b-6d16-43f9-847d-b84ff516c15b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 326,
                "y": 80
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "059819ec-58d2-4b37-9fef-83325b059864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 338,
                "y": 80
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "11a2322b-f014-45ec-8840-7c77989598f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 354,
                "y": 80
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4dfa21c6-f929-4811-9ef0-386169c42a29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 366,
                "y": 80
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "97260557-598a-433a-81c0-e3b7eed2fc23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 378,
                "y": 80
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "dffbe06e-91a6-4833-a6c0-30989a4b7b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 389,
                "y": 80
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e1c1f51c-41f9-478a-8681-5de802c1f4d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 400,
                "y": 80
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "35ea5a83-82ee-47b3-9e63-d3fee0ee9868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 405,
                "y": 80
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1bc94767-f223-441f-b53f-8b02c047e675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 145,
                "y": 54
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "28a94897-6daf-4ced-aa95-69a51785e235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 0,
                "x": 143,
                "y": 54
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "1f9f6cee-f3b5-49ac-9680-d6741b188226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 24,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 138,
                "y": 54
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "6210a855-86b6-4244-8e25-0540c041f114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 60,
                "y": 28
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "9cb97d54-7b14-4c06-b364-fe8782496fed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 321,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "7eb3f5b2-7a92-43ad-974a-85ffc397d570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "ecc80f71-f3eb-491e-9da4-731e5146affa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 345,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "9f8ef761-8464-480a-8717-b6108032a491",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 24,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 357,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "be7e2306-3bfd-4561-9890-bbaff4886c79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 362,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "4cffca93-9d4c-4bde-8ecd-f56e675bc3e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 24,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "ad3eb568-cfa9-441f-938a-3e5b65c25425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 382,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "d3d841a6-c1c5-4c19-94c9-142ed154770f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 401,
                "y": 2
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "15cee0bf-1d11-49c7-b912-ae95f4a54fcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "a525a47e-c2fb-4029-a5d6-224d93b372e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 431,
                "y": 2
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "a4458a7c-a24c-454a-aed2-460e5a6e50f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 51,
                "y": 28
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "10f4dffc-2892-45e2-8f1e-a2ff566ee581",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 445,
                "y": 2
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "f4ca07d0-32e5-4790-a0b5-c1b8175bb92e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 24,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 464,
                "y": 2
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "916cd3dc-555e-47f6-96a3-4f63a5d7143d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "6b8a9c38-55f6-4f4a-b1a2-b4cb77607efa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 488,
                "y": 2
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "1848151c-b8ca-4058-8b09-5782e3b930b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "4d57c66f-ce53-4053-8cdf-26aa5aa6ad37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 12,
                "y": 28
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "398066c3-0565-49d6-8994-1459c80186da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 24,
                "offset": 4,
                "shift": 10,
                "w": 5,
                "x": 21,
                "y": 28
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "0768614a-6e57-44e0-acb5-2e31b537d0ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 28,
                "y": 28
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "17a069dc-e7ea-4b47-b6c1-0b66183f7844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 39,
                "y": 28
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "904669cd-f6cd-4859-89f5-6beeb148d057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 24,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 316,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "153e2a1c-8c15-4942-87e2-fc763c2aed1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 24,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 423,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "70fd80b7-7971-450a-bb77-21853482c885",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 24,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 308,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "91e3525d-a8aa-4dd4-9a60-ed20e5b66342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "33c2ef03-bb9e-46bc-8daf-ffffc5f1a00d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "d2a39070-3761-4d47-a11a-1b1003b9c8f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 24,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "f1b598e5-ff48-42f3-82ce-9c3a69217485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 24,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "b4718412-29cf-44da-8d43-5350a2615fd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 24,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "d92f1bf6-3f87-4812-b025-86cc7d63ea99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "65170b54-0790-4638-9c69-0f0320d0fac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 24,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "f477e3c6-8289-47f0-b0da-c5bc6ad7c63c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 24,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "759303f6-2df1-4709-8536-8def85dcc43a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 24,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "9294c104-0530-4142-862d-a126610d7198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 24,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "5f1a5ac9-eb8f-4ca2-9efe-f73bbadd92fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 24,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "3ad45632-e7c0-426d-b9e5-181acbae57ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 24,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 284,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "4ab0b5d7-683e-42ee-868a-aa193f1028a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 24,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "b213cbec-eae4-4ddf-bcf3-c8ec15446a64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "cef268d1-8da2-4e57-b246-4a53ab76f83d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "a0d88ba0-991b-477e-bfe3-ef172e6b81dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "21f1b9ea-402a-4dff-9ffc-6a9040a381e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "b3551b4f-3f43-491a-bb1c-805cf960eed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "a104794a-adba-438e-9379-747638f91bab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 257,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "0ca77855-d6e0-4892-bc7b-8df0a3d74365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 266,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "1c049981-50ae-440d-b182-0aa13a6ef546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 275,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "31f879c1-3f94-45be-90f6-04da711a6953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 299,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "6cfedd69-4211-4d77-9780-5932fab12302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 71,
                "y": 28
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "f0291e8c-6e45-42ae-b49c-15bb23481200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 331,
                "y": 28
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "da5e42ed-07df-4b9c-9ff3-db3a4ec87ea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 86,
                "y": 28
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "c06ce0af-9b9a-4730-8fff-1078570da47f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 367,
                "y": 28
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "4bea748d-252a-4285-833f-0dc7bbc78f43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 382,
                "y": 28
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "61707d80-1241-4d75-99d2-68c507e0c64e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 397,
                "y": 28
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "1ecded68-af08-432f-b9bc-b485c7701ea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 412,
                "y": 28
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "ee579e33-724c-4548-9156-7ac037e0d94d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 427,
                "y": 28
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "90cfb818-b126-4a06-a93a-5b70011f3422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 440,
                "y": 28
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "aa9b8682-fb19-40b3-bf79-9216ea8bb928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 455,
                "y": 28
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "00952d46-82eb-42b9-918d-2f6c825f472c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 468,
                "y": 28
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "1a8f32e9-5947-4792-a52a-697fa373982e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 481,
                "y": 28
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "e1c46255-5287-4c12-909e-831372730677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "38dc71bf-a694-4de0-8a89-8a96d53b0fc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 24,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 123,
                "y": 54
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "6b447ace-16cc-4afd-bae1-efa8e3b2f324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 15,
                "y": 54
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "0da8f78a-8673-4106-9b2f-8a2e0807ca2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 27,
                "y": 54
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "07f3d2e2-711a-450e-8ad5-c87ef4b8d417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 38,
                "y": 54
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "df2bcb19-7a15-43b7-94c4-bdb9d569e610",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 49,
                "y": 54
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "4bf8144d-8890-4390-8bf3-0d29497ff918",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 60,
                "y": 54
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "c13e5402-6b71-460c-8b37-2aadd7ceec22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 71,
                "y": 54
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "19c7050d-b9e6-495d-b289-55db5cab0dc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 82,
                "y": 54
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "4d426679-cf51-45ca-97dc-280526d6afb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 93,
                "y": 54
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "eba79b1b-4826-470f-b617-49a5c32950a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 104,
                "y": 54
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "fe6bd88c-fcab-4493-b45f-91d001b4edf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 356,
                "y": 28
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "c916b8e4-0cd4-4dff-a0ef-9d1539453e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 494,
                "y": 28
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "327ab810-7335-401c-97a1-e7ac6158c077",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 344,
                "y": 28
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "55aa4d53-fb98-43bf-9f52-660d37a9d1e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 28
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "9d36bc07-cbda-44e2-a37b-4826c95c4510",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 101,
                "y": 28
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "1d2f7585-557a-46cb-a232-9a45b7ef6dff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 24,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 113,
                "y": 28
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "90a4ec42-c499-4d17-8c2d-142f231b0c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 24,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 120,
                "y": 28
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "0a02c73c-e812-4b59-8562-b0ef0517366c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 24,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 127,
                "y": 28
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "138c43e1-d5b9-4a0c-bdda-6869825068ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 24,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 135,
                "y": 28
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "aa90dfbb-b0f4-4945-b7a4-82c2f5d6c26d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 143,
                "y": 28
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "cf7d909a-e1e1-4ca7-bb29-5101aa4ae1fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 155,
                "y": 28
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "51d54063-ca83-4c66-a2c4-6da0b92a0a4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 166,
                "y": 28
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "b93b29df-a44e-49f6-9028-f4caa8210600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 178,
                "y": 28
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "5af803b3-fde8-4f82-b5b3-c275726f5929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 202,
                "y": 28
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "44f52a4f-06f0-4dd1-84a3-36342327dc1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 319,
                "y": 28
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "76376a06-b63e-429f-a306-9e374f1c4080",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 214,
                "y": 28
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "ee099787-1f28-4823-abf2-93aa21e8eb95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 226,
                "y": 28
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "8328f317-6ac1-4542-8a9a-47f18586c666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 240,
                "y": 28
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "8188abd1-8a20-4904-ae4b-ad4c9ffaadf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 252,
                "y": 28
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "a0264372-a953-4258-b25a-a410d0757397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 263,
                "y": 28
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "876d4e0a-d09b-457b-9631-1bd701fa25fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 274,
                "y": 28
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "d09dc11c-dfa7-4085-9dd1-f2cec38c314d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 285,
                "y": 28
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "a08bbcfc-ed08-4753-9dbd-da4c182dd03e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 296,
                "y": 28
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "5c141634-cbdd-43dd-ba99-3f679e0af3da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 308,
                "y": 28
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "2acb0354-ba8d-43ba-a9e9-4086dcb4cee3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 223,
                "y": 106
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "41996d23-750c-4099-9b21-c9aeb7412359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 24,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 235,
                "y": 106
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "6f82a9ec-bcb6-4d7a-aa00-f49929cbdcda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 8217
        },
        {
            "id": "6cd5b2e8-67d6-45a3-a601-ba3ddd3c22db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 8221
        },
        {
            "id": "e6c5640f-1b38-447c-aa2f-ca9984f02441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 65
        },
        {
            "id": "dee5cde1-17bf-4b0b-946d-47876314eadd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 198
        },
        {
            "id": "9a992094-5325-4478-be3e-d5fdfee0e526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 41
        },
        {
            "id": "9625b8c8-ac3c-4770-9af9-ffd9b372192d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 93
        },
        {
            "id": "248004bc-8ca2-4046-8aa8-9dba975200de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 125
        },
        {
            "id": "bab9d911-f97c-43b2-b41a-407348dd322c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8217
        },
        {
            "id": "9c7cc995-ef77-4b1f-ac3d-0d239f3fc296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8221
        },
        {
            "id": "66c58287-44e7-4415-b4d6-2907fd87555e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 74
        },
        {
            "id": "a39dcc0b-c5d6-41e9-bb19-cf4fceeaa89c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 84
        },
        {
            "id": "fccf54f0-c421-4ecd-ae52-75bc7ee9f146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 88
        },
        {
            "id": "28a77eea-ca52-4d6a-8dac-d23565487596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "1d8b09f7-e766-455e-a5b2-8896cbd99185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 41
        },
        {
            "id": "193bed91-1caf-4e48-8844-461ce5c8a66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 44
        },
        {
            "id": "6b2db479-40f7-4603-9901-fb474428fe63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 45
        },
        {
            "id": "571f1ff9-d623-4147-96c6-db88ff198ae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 93
        },
        {
            "id": "e6c395d3-9c09-4b0a-939a-da839a5ca435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 125
        },
        {
            "id": "378b987b-5985-4b8b-9fb9-5b5a28378e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 173
        },
        {
            "id": "5045c46b-ed05-4c69-812b-de709e2179a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8211
        },
        {
            "id": "d4054da6-2572-425e-aa6b-89b5c955c39b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8212
        },
        {
            "id": "42e6e19d-7aaa-4114-82fb-325bdea18526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8217
        },
        {
            "id": "cd0d1907-499e-4f4f-83e1-b2753bbb4d57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8221
        },
        {
            "id": "a7d74d3e-f0c6-48f2-9330-db2d1499cb6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 44
        },
        {
            "id": "23acad70-571b-4e33-ae30-5bdfaa5d5e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 46
        },
        {
            "id": "28e03db5-5a5d-4277-94ad-2ac64f7fc6e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 8212
        },
        {
            "id": "ebb72753-6b45-4abc-9cd8-c1ce95918cd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "b1e8d818-b45d-45ec-8cdb-3cdb38229042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "821a4d2f-4ace-4bd5-aa1a-1e4f9450e9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "33b7a099-16cd-4ebc-b665-d344e53e3101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "4c652ae0-932a-46b4-a7a2-cbb3abdb02e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "832a8789-ddcc-42eb-af70-b7b0b071e111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "3d377680-a32c-44da-9d42-74bc58ee73da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "04434883-a44e-47ba-888a-48cfa2861961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "eecada4a-bf19-4ea7-9864-2e62db566504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "168f6b15-43db-4a89-9390-018acc9c667d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 230
        },
        {
            "id": "455ee92d-65b5-4c3c-9bf7-d0c367578f2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "17e536af-1afc-46c9-9cd5-c58b27a5619a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 45
        },
        {
            "id": "4ac45d47-12d5-4a27-abbd-029b90a33727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 173
        },
        {
            "id": "ca4ed13b-6816-45d0-99b2-3ac290524eb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8211
        },
        {
            "id": "6da96418-95ab-49d4-8a79-1abdaa85373e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8212
        },
        {
            "id": "756bfdf8-481a-4538-bbf6-b2f0429b20cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 39
        },
        {
            "id": "dc217158-211a-4435-8a9f-908fba7f9c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 45
        },
        {
            "id": "72f20431-7d7c-405d-bd29-d8e2d6dc6320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 74
        },
        {
            "id": "43ff9c73-04b8-43b2-8723-a3ac09c8c774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "3e52f7e8-2c57-45dc-b626-e1fa6621beb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "75a00cb1-a403-435f-9a5b-bdad94c67bfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "4dd0bec0-c096-46bd-87f9-2975a12be6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "8513e881-b4b5-414d-a616-b9d628fe017e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "076f540c-8140-4761-afdf-cd52093c4101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "f0e0f27a-83d7-4c4b-a763-b6d6eb3951bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 173
        },
        {
            "id": "1327dd16-c0bc-4ce6-9dba-73750917c923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8211
        },
        {
            "id": "34dfb1ce-f1b6-433a-9df8-e03a4cc090f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8212
        },
        {
            "id": "615bb3b6-11e2-4306-938f-8117423952d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "65ee7391-2f02-48ca-928d-0dcf78b39cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "6152db7b-25c7-4f0b-9a08-3a10a36fe9b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "b5d53dfb-127f-4511-86ca-8d8a8c4f8d9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "6b0ed4e9-142c-4e06-8a25-793423f83e30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8230
        },
        {
            "id": "d5cad77e-1ff6-45b3-8c28-cfae4e6227d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 45
        },
        {
            "id": "6a5c931c-1de5-48e8-b946-32a92a0c6f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 173
        },
        {
            "id": "7f9c13c0-2e9e-4468-9811-84c4a29f27bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8211
        },
        {
            "id": "a01de6cf-0edd-4cbf-a6cd-bf938226aaf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8212
        },
        {
            "id": "6d9317ba-c31e-471e-8f50-23b6ceac6bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "69942cda-b97e-4624-aa38-9fbb9d0c5c11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "4d2fae13-9e60-4235-accd-e4ffe0cc2959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "7891f642-7132-49a3-807a-8abcac19e7ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "2226fa3d-54d9-4554-8882-e85573781b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "b827322d-3c3a-4c28-83ee-9bae26d7a74c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "b47a32af-6abb-4f72-a6d5-b8db71715da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 84
        },
        {
            "id": "bd7dcfda-f94e-42df-abb3-e1d1395a4e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "d77c476a-98b2-4ae9-ba9c-af1c64e038bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "4662bc59-1277-4da0-83f8-98dbf9755105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "a7615766-b6bf-4093-beb5-5ad531cdc3b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "2469faeb-2b83-4afc-a978-cb8e76883596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "3d90b067-9cb1-4579-b71d-9f1c98796e4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "dcdf0da4-ea75-492b-9b00-a1072ff36c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "8a622e7a-0879-4b3e-a988-999ffbed962d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "b633b1d1-db32-4d9d-9cad-dc052c1375c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "0fa78dce-ff73-454b-bf71-225a7e173bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "e5ceecdb-9574-489a-bf3e-9e562ae91414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "621ef426-b10a-473d-a52f-6167942e74ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "4b1aca4f-fed7-4f63-be91-bf242aa49500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "7fb0cce1-2bc3-4294-88e9-3c973c260780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 198
        },
        {
            "id": "c16e6e7f-3176-4fb7-b7a1-7f71435e7c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 230
        },
        {
            "id": "ce885885-4372-457a-809b-0d99ceb1ac74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 248
        },
        {
            "id": "25db352a-a8b2-413e-950e-fbd1eb7c7fe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 339
        },
        {
            "id": "cd4fcc78-dccc-43a9-9465-e239cbd80f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "bb8d539a-7de5-47d4-bc0d-cdca068162f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8211
        },
        {
            "id": "5b6e9ff6-b549-4cd0-ad54-798fa47485b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8212
        },
        {
            "id": "adecea6a-c531-425d-99f4-caea77a0f28f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8230
        },
        {
            "id": "6e1d37e5-65a8-4deb-a429-0ec46ce98ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "d30b9809-026d-48f8-899c-1f011af2e22d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "933b1dc8-0f43-48ac-a1b2-d8974b400bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "3b71e3cb-f01c-41b4-8c9e-ea10140446d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "0d3458cd-4171-4023-8c9e-b5256c2f9214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "259dd720-e8c6-4b5c-b058-5b8f9711d70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "72e7b55d-8913-480c-ad7d-afad3d3d2c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "a51a5b38-1d54-4166-9c90-7db1af64b853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 230
        },
        {
            "id": "b47e2aef-bcd8-43d9-8cef-d89cf9202eec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "3e6029e9-2b57-4911-b255-c7e8bf44ba0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "5e74cb96-8852-4726-954f-89c0a26b77f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "51564d9d-9ef4-4ebc-b0cc-198ec123bffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8230
        },
        {
            "id": "07b345bc-8887-4971-b844-2ecdc056b00c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "5643c648-9520-4090-8617-71edb55aa011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "9055ec6e-3b9f-4fa8-813c-c490b3fe32a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "a674c973-8d96-42d1-bb41-5be8bebf199a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "bf94f8d6-558e-4e90-9596-581227534a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "098d4832-8803-4d6f-aeee-d6050127a6aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "2dfee4a3-cfb6-4769-9af5-8f2410cca95e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "2855446b-b685-4e08-b0e9-b4e960068988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "ef40dfb5-2c33-47cd-8d85-e9ba9529f496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "8eeb294d-3564-450c-8f00-c63defd9879a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "889e6d93-a150-4c38-8db2-4c9edb19252e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "237d4e78-8a35-43b1-8f63-2433b8d08a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8230
        },
        {
            "id": "2d7fd627-173c-456d-aafe-6745c8962f17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 45
        },
        {
            "id": "086a94b8-9363-494c-b7dd-8d11a446a4cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 173
        },
        {
            "id": "e3d779af-0804-4176-9230-5593a4bc4b60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8211
        },
        {
            "id": "daa1c4c6-1a92-4d40-9b4e-9b1f8c3bb26f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8212
        },
        {
            "id": "e1ff8109-0c41-48de-9c4f-a816493e5c95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "0e3d1d8b-bb24-4605-b18c-f5e75567dfac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "77179221-4263-4892-964d-e02f19210802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "407fa8d5-98ff-461d-874d-d441b76382d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "54bb3021-44a4-41d6-b8eb-5a9f1ec972ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "cd68893f-236d-429c-8595-e911dc25557b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "72ca817a-0a8d-43fb-ab33-876652866d89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "820e5e4b-b113-4899-b3c1-46a6ee56a223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "d1e2514a-e9e2-44eb-ac5d-39597253523e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "8b3e8415-40c7-4118-9f68-da8968a77381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "009c1db9-0697-4a52-b83e-c9222c7deedf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "934880cc-cd82-42de-a1f0-38a0f45b7cbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "a83d4180-ec92-4d77-a2cb-b86dc1ef0f72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "a7c30ed3-d37a-411c-ba5b-14e8f7d6898d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "b7b10883-00d1-4e7b-aebb-b538805bb145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "3a2bab5b-ce0b-4f2e-8235-3c447178bcb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "ed6f0828-0f6a-4dbd-bcdd-d7169596ca00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "28245cd2-e217-4b95-b90d-8b5e6900693a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "daeb4cca-fe68-42c1-a512-30e2160bc369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "52a6d3de-a3d3-4d33-99d2-033a2f924d4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "2a947662-4a44-491a-b925-a472f032d8f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 198
        },
        {
            "id": "ee0fa2ab-6cdc-410b-9866-f62bbeb079a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 230
        },
        {
            "id": "fd86907f-5708-49e1-bd44-77293c33b22a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "c07fef33-71fb-4bf5-9c11-d4abd74ff4c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "dc8abbd3-b13f-4d59-8779-cd0e2e56c4d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "c5e9a455-41e7-4bd3-a8ac-2025c00d9c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8211
        },
        {
            "id": "51ef4191-8349-40f7-80ef-64caf93da7bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8212
        },
        {
            "id": "b417b1ef-5ed3-4040-989f-5c99da6fac2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8230
        },
        {
            "id": "75c1a7b8-ccab-42c1-aeec-6fe39deda9cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "86fcf7a9-ef8e-47f4-a4d4-4132bcce97b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 84
        },
        {
            "id": "09f02242-5083-46dc-9491-8b7cd0c0d61c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 34
        },
        {
            "id": "3a5b3eed-a43c-4be1-a8d7-9243700aa425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 39
        },
        {
            "id": "d37d694e-4456-43d0-88ce-9c1d37fbae4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 41
        },
        {
            "id": "0b5f64c4-d1e3-4f7a-a5a4-a868f9cb2912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 44
        },
        {
            "id": "187e5abb-8e86-484f-9997-5063accc7c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 46
        },
        {
            "id": "8c1578cd-762a-4beb-a6c9-a4b37120b7e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "8c87dbc9-7012-48f8-8640-291b1bf5220c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 92
        },
        {
            "id": "f4ba0ca6-f3b1-466b-8398-55bbc3205146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 93
        },
        {
            "id": "3fbd57cd-1d5b-4c6d-8196-b285eb6605c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 125
        },
        {
            "id": "f12100ff-e6c9-4ae2-92ea-b27aa1a85100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 238
        },
        {
            "id": "6918a6cd-f745-467a-a89b-bdf35d91c206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8230
        },
        {
            "id": "90faea08-3774-45e2-b9ec-9d267168578b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 45
        },
        {
            "id": "e2498aee-a569-494e-bd40-a808a49f3527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 173
        },
        {
            "id": "e8e75530-9a00-4227-87da-448a8529ea30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 8211
        },
        {
            "id": "c22156a3-bec8-493a-9843-dc8b7cf121b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 8212
        },
        {
            "id": "97533b3e-8e5b-4298-bb78-fba872ab8593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 8729
        },
        {
            "id": "c4c7b228-87c3-46e6-b35a-5ab3fe380782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "cf3a5814-10c0-452e-a8fa-43c007ae24ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "e2567bd9-b0ad-42f3-8fad-d3ae2831a404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8230
        },
        {
            "id": "46eb695d-6104-49ff-aec3-b848732785e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "d4323b21-8bfb-4672-b4d4-fdde32d4725f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "5417079e-cfeb-41a5-aa2f-eba9b8dcfc25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8230
        },
        {
            "id": "78db7a98-ea85-426f-9b0e-becc5e9589a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "aa5ad003-695b-4333-850b-ad75b3447b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "0b5eb15b-6fb5-4fac-a323-6a1007becb31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8230
        },
        {
            "id": "734ece8b-efed-4c0d-b1e1-10c71a3ad1e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 74
        },
        {
            "id": "6ee1aa10-4800-4a66-a98c-3b001c56e1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 84
        },
        {
            "id": "0d744892-183e-4157-96a9-ab3b7644a48d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 88
        },
        {
            "id": "9553d3b6-cd3c-44f2-b405-cdb8430a5e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 89
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?as {{[ñññ\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}