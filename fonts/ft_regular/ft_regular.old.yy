{
    "id": "0ae06df4-7f9f-4b8f-a810-089cfc3211ad",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "ft_regular",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Tahoma",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f9134571-835a-4dc8-a0e3-0788620bd660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b89a2e2c-f665-4fb1-957c-456420092914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 23,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 24,
                "y": 77
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3ed19e0c-5065-4e63-a9e6-0eb9a85358e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 23,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 16,
                "y": 77
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "83abad76-f168-45ab-90fe-6f1b25f5bfed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "953e6958-f1bc-4fec-8bb4-9b9cbbfb031c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 243,
                "y": 52
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8ac02aff-caef-43ec-a110-ade3294d21b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 23,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 223,
                "y": 52
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3161310a-5ae3-447c-9863-5071e198ed65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 207,
                "y": 52
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "34324841-65d0-4470-a162-9039246a13b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 201,
                "y": 52
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "dbc09ffd-bb47-4f29-950c-69756e6badcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 193,
                "y": 52
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a5efa592-dccb-44ec-b391-e0768801919c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 184,
                "y": 52
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "91a2dbc3-b565-4cc7-bf70-106d7c9a9655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 29,
                "y": 77
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8d37a5f0-a1be-4faf-9902-cbae81c134cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 170,
                "y": 52
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f74f6cf0-bf5e-43d3-b390-8d5f7f108a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 152,
                "y": 52
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1c0ad270-910d-4d1b-9dc8-e285691fbf52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 143,
                "y": 52
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "683d62eb-197e-48fd-84cb-1e3704a6ac04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 138,
                "y": 52
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "63fcaefb-73d8-4a3d-87fe-84e61ab913e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 23,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 128,
                "y": 52
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "adbe7e7c-7a53-4aa9-a58b-2f406ddb1030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 116,
                "y": 52
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "14bfff44-bde7-4458-9802-8f8611ab93d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 23,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 107,
                "y": 52
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3408b543-4ae6-4416-8c97-217fd1a0c2e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 96,
                "y": 52
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "33869acc-385d-4a0c-9fbf-e2f7c4a24181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 84,
                "y": 52
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5f6853f1-ae79-47c3-9d15-a76ba0df15fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 72,
                "y": 52
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "00c2119f-62b7-4a52-87fd-752712c74234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 159,
                "y": 52
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8340e25d-a0b9-4692-900d-54c034c030d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 40,
                "y": 77
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "db261a3d-704b-4a1b-bff3-be3d7d1d90bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 52,
                "y": 77
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4a71727b-a48e-4cd2-8bb4-9f92ab1f52d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 64,
                "y": 77
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "11418c11-8f8e-46e6-a063-1d5264c70a26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 81,
                "y": 102
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6cbe1069-3b88-42d3-8abe-51edc519f87a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 23,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 76,
                "y": 102
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "eb652bbc-3f06-43ee-b1dc-6e765287a46f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 69,
                "y": 102
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "073c32b9-1501-4b71-82dc-be2c07ccf72e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 56,
                "y": 102
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "642a14eb-06ea-454f-9793-c6fe63dba5b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 42,
                "y": 102
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "510a4e7d-2a60-4a8c-922c-22c9727a14a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 23,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 30,
                "y": 102
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "26cfa0de-b591-42a2-aa4d-fc89cb55c444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 23,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 20,
                "y": 102
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "107f22ae-db88-4e51-a029-48c3b5c8ec6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 23,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a322a192-b084-4b65-886d-1243964f62ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 223,
                "y": 77
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "29fa27fc-37b8-4ae2-a18d-548524510983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 211,
                "y": 77
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a5d4df10-19a5-4d4b-b41c-4e835148994e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 197,
                "y": 77
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "83a54454-43e8-4bf5-9026-7cb744b384b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 183,
                "y": 77
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "82f91ec7-e02f-4a41-84fd-086dee5e2e44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 171,
                "y": 77
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b66609e1-55d9-4926-a29d-3375002f52d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 160,
                "y": 77
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "29aa4a13-da4e-444c-9b34-3fdb7c0c187b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 146,
                "y": 77
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "12739832-e432-49b3-abc5-3fc8b2b7f23e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 133,
                "y": 77
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c69f6f70-d24e-4b93-890b-2511347b0647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 124,
                "y": 77
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5b64dd3f-1af4-41a5-a2dc-964841fbf30d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 115,
                "y": 77
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a0caff5e-001f-40b1-8bc2-72f44148a22b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 102,
                "y": 77
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9c45c97f-450e-49fc-ac91-d1ce14e2199d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 23,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 91,
                "y": 77
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9f1426bd-d158-4906-bff9-97b2f85494ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 23,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 76,
                "y": 77
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1ac32b6e-9b68-4eee-9a0f-01c695ccdbcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 59,
                "y": 52
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7f083036-804d-40d2-82dc-464a8752dda7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 44,
                "y": 52
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e35a2346-9914-4764-8c84-712d1e67c0e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 32,
                "y": 52
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "cecb8102-a09b-44ca-89bf-388f1a4d6945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 24,
                "y": 27
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "20df5503-2afa-4fd6-9256-535147c6491d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 27
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "39428e3b-58a0-4e07-9df8-5c9929723926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fe7a12d0-7624-42bd-a8cb-5e84b0282136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "52c087f5-bd02-42a8-8518-5f88ae5715d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2cf841bf-e858-4a3c-8ad3-0e8c1d34e203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "49034240-d9c9-4c7b-ae1c-3c1e9f2a08c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 23,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0fad8f58-69ac-4634-aa49-0762623374ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5c55a19d-c0e1-42e6-b4ff-b3052a96552a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8b7ba67f-a950-485e-b4f9-0adc64805b35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "860462f7-68cc-4b9a-8204-8dfcfb1a5200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 16,
                "y": 27
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fca4d5c7-f645-4eb7-be8f-fa550b1316b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "cfd0051b-6d19-4495-92d3-82a503c90921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "290b64d4-9612-4aa2-a2c2-e443f88ee396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "06c6cfcb-3a2e-45fa-b613-9864920a92c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 23,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "04351a1b-ddd3-4415-a58f-117d09d83ab7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 23,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ba82afb1-c359-457f-abc6-c543092a4a02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "06181637-1f9a-4728-bcad-36744ffe9e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "32ccd00e-95b8-450a-af88-5eadfb32ac7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c8a9c50d-52b8-47b4-a962-3231975590c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "66070a9c-beec-4db5-a852-61dbbdc1bf63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7dcc5e77-12bf-4a18-b8df-c87cf4aca14d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "dc8ef340-2df7-4dcc-bcab-c9435cd2cffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 39,
                "y": 27
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "30135cb6-687a-4726-a000-015035ce789d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 157,
                "y": 27
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "36b8a3d6-63aa-42ce-a1f4-81c8172b83c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 23,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 51,
                "y": 27
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6492c185-68d6-4b4c-9839-570d6f6bf3c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 23,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 13,
                "y": 52
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "008cfdb3-f7f0-42f5-b8c0-50d458e1b046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 23,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6896a37f-fe7d-4893-93ec-4a76e15314b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 23,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 247,
                "y": 27
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d548738f-aec6-41a1-ae9b-b0cf955f87e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 23,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 231,
                "y": 27
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c11cf0e9-c07d-4560-83aa-13da99ec324e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 220,
                "y": 27
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5cc9064c-4e04-4ebb-9ba2-727e848cdc56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 208,
                "y": 27
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ddbd104d-ca8e-4346-9610-2b8d8c31f521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 197,
                "y": 27
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d3230954-a711-4144-8311-88191da7d502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 185,
                "y": 27
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2ef48f0b-61f5-4d9b-b931-ebf7694ebc0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 177,
                "y": 27
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b227afa5-45a8-4f1b-b3d8-b426ed6ebbba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 21,
                "y": 52
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ee87a747-9cf5-48a5-a337-0f3dcbfe8a0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 168,
                "y": 27
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "909017b4-0eaa-419e-9426-e191216e34c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 146,
                "y": 27
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "671eb9f2-518f-4283-a904-e31368120e67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 134,
                "y": 27
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d4f4287d-5a43-4277-a6a0-e02463e4a1dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 23,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 118,
                "y": 27
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6ca58482-c119-4486-a731-c04c39f06cd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 106,
                "y": 27
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "cc7b67b8-444c-423d-bb6f-bf1c268da831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 94,
                "y": 27
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b5f14e7b-0dfc-4487-bed0-b026ab9dba12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 83,
                "y": 27
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4f1b854d-5a6f-4b27-811f-15a7574508fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 72,
                "y": 27
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "58268448-3703-4d23-8df1-a9de5e9dae25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 23,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 67,
                "y": 27
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f446fd5c-fd05-462f-90dd-d0ce6565ce0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 56,
                "y": 27
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "00e6f06f-70b5-44f4-aaac-7a91bac1e28c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 93,
                "y": 102
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4f21a760-2cf1-4c6e-b14c-74a97093c3a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 23,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 107,
                "y": 102
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "45c2eb33-fb1b-4ebe-810a-4c7e6331c33c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 8217
        },
        {
            "id": "afe371ad-ffcd-40a7-8269-0b9c976ee6ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 8221
        },
        {
            "id": "55a684f0-66fa-4c59-86b8-7cc1b9f2889d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 65
        },
        {
            "id": "2476b8e5-d352-48ac-87fa-0c470024272b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 198
        },
        {
            "id": "f2060a5f-0f67-43b4-8831-060cc05ae444",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 41
        },
        {
            "id": "789372d9-e616-4cb8-bc26-1251af309659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 93
        },
        {
            "id": "69ef78a0-881b-46c5-94cc-e2ed12f6c4e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 125
        },
        {
            "id": "2a6c8af4-488a-4f0e-be30-4666788e7971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8217
        },
        {
            "id": "3c83831b-d278-449d-ab46-e6ded9896b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8221
        },
        {
            "id": "75fe74f1-b88e-449c-9c56-445f01fed520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 74
        },
        {
            "id": "82b98973-c345-42d1-90d6-b742f4669874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 84
        },
        {
            "id": "4ec6bbe5-1832-404b-82d7-3228a4a255fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 88
        },
        {
            "id": "1c3e7b7c-1678-493c-bc94-f32a188a0a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "67f0fe6c-3ec6-4675-ab2e-63d23f0a517f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 41
        },
        {
            "id": "223b918b-5e06-4cc4-8fc2-64a0fb70503d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 44
        },
        {
            "id": "97307f1a-3c84-4450-bd54-fcf9ef833784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 45
        },
        {
            "id": "bf43a411-04aa-46a1-bdb0-a9db97d77413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 93
        },
        {
            "id": "fc51a92f-7b36-450e-a625-cb27217ba262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 125
        },
        {
            "id": "d00d8748-099d-4b66-979e-e912bcd24ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 173
        },
        {
            "id": "37c4a633-b516-4bc9-8ad9-b0baf2e231bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8211
        },
        {
            "id": "4d0d9597-235e-4097-bdde-c208331ea02f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8212
        },
        {
            "id": "bdc12a06-3abc-4d8e-9bc4-58d44d491805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8217
        },
        {
            "id": "b75062c3-30df-4adb-a981-279dbd1665b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8221
        },
        {
            "id": "60070454-8fd0-4e20-9b59-d0db698beca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 44
        },
        {
            "id": "0d5d6361-b4a7-4767-8a29-4ae73c6314d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 46
        },
        {
            "id": "77341f7a-1046-436b-b34b-aa4856123b2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 8212
        },
        {
            "id": "dac8a87a-8746-4c3b-8162-2952b12508f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "8fda7e5d-6d4e-47e4-abf5-551406bc9533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "b31d7b3d-120c-442b-99d4-0aa0c58e6a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "8b08d82a-65cd-4c82-8612-77a91d8f5803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "35d1892a-46d0-4227-9210-b7445f5b22b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "c63bf4cf-9135-400a-80df-facbe200acc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "b117c123-a497-4ac0-8ed9-672f6dab9886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "cd96a738-0b56-457d-9719-640da99f03e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "25fb7738-d7cb-441f-af53-0fbb03334713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "8e68aea1-94e5-43cd-ad53-829526598aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 230
        },
        {
            "id": "7f79ea4e-3559-4f35-ae55-7290ce6ff6f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "e5245f2c-bb05-4162-b04f-d2e497bd6432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 45
        },
        {
            "id": "a0bdfc9a-3203-4fce-9753-356d70e72cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 173
        },
        {
            "id": "75109752-cbe7-4c65-9095-eb0c1b28d066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8211
        },
        {
            "id": "2bf04545-ac19-4503-be6e-917f0560bfac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8212
        },
        {
            "id": "0d72ac29-27e5-48a8-a8a7-731469056bc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 39
        },
        {
            "id": "26560a17-76ca-477e-9d16-2a89d20eaab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 45
        },
        {
            "id": "8ef6a5fc-69bc-4b37-8087-6ca3ccc74d96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 74
        },
        {
            "id": "3c6fcc33-1f8c-44b7-aef4-0de996270f3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "dfb9a1fc-2551-4644-9276-b36e6ca6f465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "37b77af2-2f25-4744-8e64-7529af2062fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "f90b3c99-50c2-4485-b4a5-abbbba75c55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "de50c90e-adb2-4e0c-ba2a-1d5f70852ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "c043f6c1-e0c2-4998-b061-e29e03c252bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "20fb1150-de54-4082-a80d-c1a63d183ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 173
        },
        {
            "id": "3d258023-96b6-4c81-9113-10b8cb9d96ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8211
        },
        {
            "id": "0cf67c08-32ff-49d2-8b7a-c1eaa974b37a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8212
        },
        {
            "id": "e4e0f694-3a7e-4fb1-b193-361549be1528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "2f3bccfd-2925-4460-91e6-784f9722fc35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "b97474ce-b063-45f8-95fd-5425603979ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "24d4f9ef-15a5-4827-9f1f-6d8b3f5b7219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "38d66896-737e-4d84-bdff-cf7bc499f5a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8230
        },
        {
            "id": "719e155d-d549-4daa-a508-9f7023fb460d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 45
        },
        {
            "id": "0828d5fc-f85d-4e02-b8cc-8a2f913af0cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 173
        },
        {
            "id": "23b771cc-c1b7-426f-b23f-606c81fbfbba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8211
        },
        {
            "id": "eff2b785-7b1b-4625-a18d-2f07a87c80ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8212
        },
        {
            "id": "1fa7dca6-c2a0-40d4-8873-f58a7ff92864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "d4133181-6a85-4288-a2ff-5ff0be4f9006",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "e6e8aa93-084e-44af-b735-d3633aa272fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "de01836f-29f8-430c-b8ea-14e242928052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "0b187f53-b081-455c-b930-d4eb8d60e0b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "413661ff-ab8a-46de-b12e-ff0f2183e4a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "2b88e468-94d1-4346-9d99-3e9bdfdb7959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 84
        },
        {
            "id": "f1567f9c-fd4c-49c9-be9c-e5404be68256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "5ac18264-cd21-4dad-b64b-f743052c3fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "a15a4914-7f52-4297-931f-c5ea5c22bcbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "7bb6a3b5-79f8-42a9-baf6-35ad4e992fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "796d6e60-ef59-4ac5-a331-09eff0a1e78f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "441bddd4-6084-4543-bdf6-0a6942397020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "47cbc556-051d-47c8-8699-930b1695c4a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "c18a6fc5-fc90-44f0-a396-7b50e531c378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "2b696387-6653-4bce-a532-ce909b4ccdfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "2c14589c-94e6-4875-bc22-7feaffdbc17f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "0489914c-db57-4713-a4ce-75f8af2669f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "8a7728fa-6b24-4091-b69f-276d0dfcd340",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "e9890ddf-1cca-4296-8d4f-ababa582369b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "60feb5db-1c9e-424b-b647-8a1fa9545791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 198
        },
        {
            "id": "70639007-07eb-4b07-8420-4795b8a32741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 230
        },
        {
            "id": "a41db5e7-0b53-4614-8794-de2a5cb784bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 248
        },
        {
            "id": "8b4ce663-6d73-445a-a604-7fa4ef91162d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 339
        },
        {
            "id": "bc232bce-7f44-4e44-956d-7ce8a8da6444",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "08fc8fde-c2ae-46e1-af7b-238dcf65f2b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8211
        },
        {
            "id": "f4c50f3f-cd7d-42cb-a8f1-54a3df2c9e69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8212
        },
        {
            "id": "ffbc4da8-f94d-4e87-85eb-325d1739615d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8230
        },
        {
            "id": "77acd34a-2a10-42a5-ac2e-bde919106383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "954e4637-3578-4845-bb7d-72974c47042a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "85ac9c81-e151-4783-a13f-cc314e41c018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "a5796ae0-6888-4ac3-90d1-8f2b0694265e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "5099ae2c-9b6f-4e70-af89-4c585dd2e49a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "5f424f5a-7553-4348-b271-3eae1b815133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "256d4544-2b93-4807-81a6-747b46842360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "8ebb80d8-de7f-4d12-b31f-1912427b4eae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 230
        },
        {
            "id": "2edf3441-02ed-4074-9078-d0d79bf2ac3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "b395dc7e-2bfc-46a3-9aa6-72198dd1df0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "e63f6ebf-e9fc-43a7-968f-08cf87dbf431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "256cf190-68fe-4385-a3ab-c65d9bc81911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8230
        },
        {
            "id": "0185c61c-db49-4be3-a210-2b7a3c022cd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "3d3ce090-3c4a-4aef-8f70-5b7f811b1d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "42f2864a-6c5e-458b-829a-1b384beea32b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "e7685fe3-afdb-4e88-b929-8b05272ab8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "9513f4ae-7a5b-4fe7-ad89-c937ce202bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "94aaf1c0-6ccd-4240-b59f-9b8e2a17290f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "2d2ebd61-9cb8-4436-abd3-6a36f912b4ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "9b64b456-fd85-4148-9bba-dd7f53ef3b3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "523b522d-64e8-43cb-8609-750e2660dce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "9d7bf433-41a1-4f90-9d50-3e409510d615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "024488bc-e1ac-45dc-bf4f-46d6366d5742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "25c61571-d360-49c9-9c40-89b5ced58a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8230
        },
        {
            "id": "99725092-1369-4a84-8945-d2fbdfce7a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 45
        },
        {
            "id": "7eaadf62-9232-49f9-b426-cb339e07ae91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 173
        },
        {
            "id": "51538b56-f5c5-4f3d-b5d6-69c947b9cdab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8211
        },
        {
            "id": "2778a9c9-9a3f-4902-84ba-de9704259a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8212
        },
        {
            "id": "4d399d60-6239-43c7-a70a-c327974ec1b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "7953dd75-0a56-40bc-b7ed-580a63f12761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "6215f998-0f83-4019-a6f2-94714c4e4985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "6f75a0de-f5cd-47cd-b926-d67e8a39a63f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "a0a794e8-e49f-4759-bef3-a3b046f612cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "2a91941a-223d-4476-90f0-87241ade7cfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "f8213cc0-ccc9-4435-bae0-4eb104e8c5f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "48256e01-80aa-450d-aeb4-a26251e4f307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "489f9e13-949c-4f0b-912f-a8a16fe5e1f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "1b2e99f0-cde4-4ec9-8b95-41a3b19a4901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "7c09382f-a564-4cec-a34b-95e6f70ce8c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "795cf381-aab6-4209-8a49-071ff39bfec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "13fde3a8-40f2-46c1-bfd5-5599240093c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "93776064-99e5-46ed-9783-50ddab43ea60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "ee884667-0522-4668-a131-e5fd03e8961c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "76f77cbe-ea9d-4e55-8526-a46d0dcb8bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "bffe9d48-d9b6-49cd-8e5f-1909fdaa625e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "2894290d-93b3-4001-be06-6ebd12da8130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "f67926c0-348e-4bb8-bafb-c545d7de1a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "3d2751d6-b404-41f7-ab54-67669e1edab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "5b91d055-1594-454f-917e-34b9272c70c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 198
        },
        {
            "id": "c9883800-553b-47a7-a2a6-bc3657d4316e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 230
        },
        {
            "id": "cd84d837-40ec-4501-9d40-de74a78a9a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "53713b09-612f-464f-96af-241001917e6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "5ffe6feb-debd-498b-80fe-3d7991f05fc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "2a131eda-a4ce-4e71-9c75-d8a956f26f5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8211
        },
        {
            "id": "a039f5e3-006a-4266-86ea-efcde0fe1344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8212
        },
        {
            "id": "d7c55102-6dd0-4683-9b87-f2a63f66382e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8230
        },
        {
            "id": "050bb711-f31b-4e0c-a260-f8db4745a241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "4e0297b3-81d0-436d-b0f8-e0d41bc068a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 84
        },
        {
            "id": "5c37506b-92bf-4b5d-830b-f83428677bfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 34
        },
        {
            "id": "d024aea7-bc1d-4101-8311-d770d9c5632f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 39
        },
        {
            "id": "7fc10245-9d4f-4661-87e5-11d877360ca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 41
        },
        {
            "id": "4c52882f-1c8a-4ace-8fca-69ec96c62824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 44
        },
        {
            "id": "5906ccbc-cff8-43e7-9dcc-281d0eae70fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 46
        },
        {
            "id": "798eff92-d04f-41c3-8f18-fa1b0c1cee9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "78f3b567-93a4-467d-ae6c-6dd10c756c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 92
        },
        {
            "id": "7696687e-e4c0-4104-ac9d-0ee59541a362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 93
        },
        {
            "id": "1a5ded8a-dcec-4eff-9cb2-1b5fe1855c57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 125
        },
        {
            "id": "26f5ab85-b02e-498e-a18a-f7269a72fff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 238
        },
        {
            "id": "4d5af40f-3926-4c50-897d-2df1de65f211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8230
        },
        {
            "id": "053eaa36-7e61-418e-8049-d188567fae2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 45
        },
        {
            "id": "e3a582ed-c8e4-451c-93b5-8c9f3f6865d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 173
        },
        {
            "id": "4502bd52-ff1b-432e-8a43-2754548e3af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 8211
        },
        {
            "id": "87b07a3c-c209-4c15-b5c2-e5249c8ef348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 8212
        },
        {
            "id": "42044492-5867-4a37-b493-b139d71e4450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 8729
        },
        {
            "id": "94a6eca1-24df-46e9-a462-ac7f0caded43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "a986b0ab-7336-4800-b25b-b83f4e44cf0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "190d4908-d7b9-487f-8347-c0244f9872b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8230
        },
        {
            "id": "772549e4-79ad-49aa-b77a-c435d9b6ed0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "19d0789f-3792-41f9-9d2d-e888325b11be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "7595ac73-6b24-4617-b8b1-0846f0f8d63a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8230
        },
        {
            "id": "6c25ee7b-b551-47ba-b2f5-92e46686203f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "0ef97c34-0b72-457a-ab7f-85e125ac0e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "119b0c42-5004-4e47-9daf-3fcac027f327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8230
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}