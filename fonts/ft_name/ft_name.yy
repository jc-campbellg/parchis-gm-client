{
    "id": "0ce6995e-1021-4619-a4c0-e2fad2744362",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "ft_name",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Tahoma",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "5a901ba1-cee7-4e73-a65d-03e86fe6e35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9bd9c6a3-098f-4688-a172-89a969ab0262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 470,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "395cfd30-e10d-4d55-a8d0-12d5588d46ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 478,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "10bbdf14-7665-4db2-9844-b0efcbe63630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 30,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 490,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a758843f-53f1-4fce-a27c-3dbc06c1f886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 130
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3f222b87-862a-493d-b26f-daed8e78f2ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 18,
                "y": 130
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "410a5af6-2522-487c-9ff4-03efe031eec2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 19,
                "x": 47,
                "y": 130
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "34c41d2e-fbf6-4004-b532-d3361b0c77ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 68,
                "y": 130
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b39340e9-2fd8-43c2-a164-feba6897a5c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 75,
                "y": 130
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "460c36ed-9ffd-4f3b-9ac5-917496525745",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 130
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d874491f-2dc8-4ee5-961e-43c186e8c6c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 114,
                "y": 130
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a973d341-5dfc-4498-b5af-aeb84f3cbf9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 250,
                "y": 130
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6ffbd27a-b61c-4a8e-acc7-bc0ddf2683ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 129,
                "y": 130
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "06fc18bb-9b5c-41c2-8e2b-f6e77c91a800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 139,
                "y": 130
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e3edfcc2-ed62-452e-8a16-4357f5a8dc29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 150,
                "y": 130
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1e964b67-b840-4bcb-82e2-716d0ac4ef13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 157,
                "y": 130
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "70831118-783e-4e03-a8f9-a0039fb869c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 170,
                "y": 130
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d8e08701-3c5d-4fdb-9549-3d61a51e8796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 187,
                "y": 130
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a884d219-0f6a-4476-8196-2777b9650235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 201,
                "y": 130
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a1fe6d45-78f5-4d06-b364-63475454d287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 217,
                "y": 130
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "244f839e-d318-44f3-8aa6-66adc7d8ad50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 233,
                "y": 130
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c50f43a5-4a05-42b9-a83d-8e7a26ce979e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 454,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f298347b-425e-48b1-80a3-47c8d1ce4ab6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 98,
                "y": 130
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b5a2b036-17ce-4580-b6bf-8d443538af81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 438,
                "y": 98
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9be6bbc2-b53f-44d3-9f1d-df9fc3d8b669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 220,
                "y": 98
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "578ced46-0d01-425c-bce5-967e5f694368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 76,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "eaca8d77-b5e6-49a2-b0a2-a5fb0f1a23f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 93,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "997058ec-b3b8-4d34-a193-5614029fb85a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 100,
                "y": 98
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "50f8f15f-c785-4389-aad4-959c8ee8072a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 110,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2d376645-649b-472b-98cc-3f1f3802777b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 128,
                "y": 98
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f52310f2-a547-4545-badf-7a8a1c058cb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 146,
                "y": 98
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "93152c1c-6d4e-408e-bb34-f0a57c95a9cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 164,
                "y": 98
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8dd83895-b838-4c9c-bef6-c745ce4e71ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 178,
                "y": 98
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b8a32e5f-fe27-4957-a724-66f6b9503184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 200,
                "y": 98
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9bf3b625-b6cd-4e69-8527-c1f9b9d60830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 237,
                "y": 98
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9f6eeb7a-99dd-4b59-91cb-a3551e738c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 399,
                "y": 98
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b6f3342d-ae90-46a3-8f57-11d37a61f142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 254,
                "y": 98
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9800de40-c3dd-4d4b-a0fe-a22b6393082a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 273,
                "y": 98
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e845e5c7-3884-4b33-addd-08d005b79a0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 288,
                "y": 98
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "11b3bf64-435d-4ccb-aa0c-ecdd7c97f254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 303,
                "y": 98
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3b0372d0-2a1e-43db-8d0c-5689fc0ae974",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 322,
                "y": 98
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b99fd9ca-5f1f-49cd-8efd-4f52caa0b254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 340,
                "y": 98
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f62ee4e2-d22b-4d2d-b23a-ba8dfbf893f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 352,
                "y": 98
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "cb5f628d-c08d-486d-bddc-ffe52997e346",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 365,
                "y": 98
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "628ba092-b582-4a11-aef8-dc5b15ec5f1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 384,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "dc04ec74-9891-4f1c-a711-9ff05866efcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 417,
                "y": 98
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0c9dacfe-3dce-4f4b-83bb-b79570f22afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 268,
                "y": 130
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2c553784-03ed-47ad-8792-86812e7670c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 286,
                "y": 130
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d70a1768-0560-4695-b570-b0abc2b82436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 306,
                "y": 130
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "35799962-595a-4635-bdb6-183c02274a33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 170,
                "y": 162
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d18c0c5f-9d69-4240-abf1-fd7e88296122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 190,
                "y": 162
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4562bb68-10b2-48db-a170-a4fece60c0bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 209,
                "y": 162
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "74c9aaa5-1242-4bd8-a9c1-d44bba6eca7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 226,
                "y": 162
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "db7ffa3f-6b3b-4b33-9232-fe75d40af6c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 243,
                "y": 162
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "52f0bfb9-2c64-4f32-b600-e68bbeda00b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 261,
                "y": 162
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "73e13dcd-947b-4680-bf00-62748ab9aefe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 281,
                "y": 162
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1383341b-40eb-4232-86f9-18f87b953ea2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 308,
                "y": 162
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "56cb4df0-b43c-4517-b1f6-fe7ff4323497",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 328,
                "y": 162
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a7f2dd87-6461-47ea-bad7-8c9368ed849f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 348,
                "y": 162
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d0d1030e-7eec-4f15-b5c3-34b3b002ef93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 365,
                "y": 162
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "85c55c3d-2624-478e-babb-e59d6d4584da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 376,
                "y": 162
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "15783227-8ee7-4434-be4d-5f6a8de56bfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 389,
                "y": 162
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "717ffc8a-7ee3-4b1a-b5e6-e4b5709eb3a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 399,
                "y": 162
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "6a1a5824-3a3e-4e66-a16a-07ecf7eb76cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 417,
                "y": 162
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "00b78d8d-cddd-42f7-9ec3-5f4fa969be91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 436,
                "y": 162
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "83573db2-3081-4e45-bb4e-a04dfe729972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 445,
                "y": 162
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "783ecdac-cab1-48ab-a914-33f996e90084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 460,
                "y": 162
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0b97d413-8897-4cfe-a5a5-f7bb55894cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 476,
                "y": 162
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b45f436b-29e3-4451-9ab0-b35eb7f78c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 491,
                "y": 162
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c730bbaf-c545-4238-9edc-c2b086c4dc18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 194
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "35fa726f-5bf6-46a1-b358-c50d5e141181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 158,
                "y": 162
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c1efb175-3b27-4299-9f40-0513d4836acb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 142,
                "y": 162
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4fefbf84-62dd-4b71-965c-59b1e5ed2249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 127,
                "y": 162
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "96704e54-2fa4-4b03-89aa-77d977a8d438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 456,
                "y": 130
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8e0268c1-3a93-4c58-aeb5-d57b81c18940",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": -1,
                "shift": 9,
                "w": 9,
                "x": 323,
                "y": 130
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4b4ecf59-5645-4105-adde-80a8a7d8e208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 334,
                "y": 130
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "52bb3ac8-351b-423a-a5f3-c1e9e6bd8024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 350,
                "y": 130
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6904fa53-f4e7-4a29-959c-275a5bc796a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 357,
                "y": 130
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2e329ee7-8b45-48a3-bbf7-1f4741f9bc9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 380,
                "y": 130
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a8ed6bfc-5c6f-455d-8818-6b5f04fb18b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 395,
                "y": 130
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b14d7069-d3c2-47e0-bc01-a122bc65d363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 412,
                "y": 130
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5e237acd-d66f-435a-b983-64960d4beb22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 428,
                "y": 130
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1de4c064-0f66-4adc-9e4a-a3ce652c518c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 444,
                "y": 130
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "fcf3bd2c-64bd-4ca8-ab9f-4169b82d30e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 463,
                "y": 130
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b4cfdbf5-60c0-437b-ab62-a2b806a243c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 30,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 115,
                "y": 162
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "378af425-4588-4d46-94ef-34e4bb9eb2d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 477,
                "y": 130
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "344e977e-efb8-474e-b055-906e59a6e8c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 492,
                "y": 130
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5f906af9-a14e-432d-983a-4712b08a986e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ab8beb54-d68b-4e7b-a8be-1f99a1f84d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 26,
                "y": 162
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3d6bb759-7fc8-4249-8520-5653515f98a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 44,
                "y": 162
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "67cee9e5-2202-4764-9399-f9e49acfe76b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 61,
                "y": 162
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f3597f1f-36e2-4dbd-9f23-1573585ffd98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 76,
                "y": 162
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e3c18a53-0f64-49b9-bfed-4653f34388d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 5,
                "shift": 15,
                "w": 5,
                "x": 92,
                "y": 162
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2869b0b8-08e9-425d-a03a-3a46c97ee6db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 99,
                "y": 162
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ed9da071-0c3b-4769-99f0-6fefcd312a21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 30,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 56,
                "y": 98
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "89fca49d-724b-4dd6-bbb9-656cae225d7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 0,
                "x": 54,
                "y": 98
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "02b112f7-b780-4f83-809d-92776d7e5e32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 46,
                "y": 98
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "9bd26842-effc-48fa-9603-2ba4778873e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 241,
                "y": 34
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "b5a961e5-58e0-4251-8073-27741ef76a51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 429,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "eecb7240-ff1e-43d7-adc7-db4464ba6e85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 445,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "2707d0ab-0c29-47d6-b92f-ec0b18709285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 460,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "0f8bc04d-b120-48c2-b3c0-eae3cb543c04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 30,
                "offset": 5,
                "shift": 15,
                "w": 5,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "63c63e80-1565-442f-bb86-fd4709a20ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 485,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "641f3908-66ea-4026-b879-384e97ca62ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "e2ce3800-1d43-4cb0-804c-f0c026c294a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 14,
                "y": 34
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "0c67e653-5e27-4620-a348-58037a996df7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 34
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "54ad212f-f881-4d58-a31f-b3068979fc42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 50,
                "y": 34
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "8efcb172-7adf-4dc3-91f7-9cc359184e2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 75,
                "y": 34
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "5e3a39ea-d3a0-47cb-a2e1-1207c611cc77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 34
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "93c04c84-2ef7-48de-9896-eba6fc57a490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 93,
                "y": 34
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "ec15fcc9-cd5a-44d5-adbe-fb9d15b9e0e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 116,
                "y": 34
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "82f39fb5-8ead-47a3-8cc2-5a5fac2522d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 135,
                "y": 34
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "d561d1b6-7256-45f1-939d-44ce28738a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 147,
                "y": 34
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "2f1ecaef-7af6-4be4-a477-d8423990263a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 165,
                "y": 34
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "f60252c5-eff8-48f4-8fb8-a0b1b79ed22f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 177,
                "y": 34
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "98611321-bdcc-4af5-9f59-72972df9349a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 30,
                "offset": 4,
                "shift": 13,
                "w": 7,
                "x": 189,
                "y": 34
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "2c2aeb24-4797-4118-bb74-65d3bc4e6c53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 198,
                "y": 34
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "612f9995-321d-443f-9b61-10749e4dc3b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 214,
                "y": 34
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "50d1cb3c-b05c-487b-940b-36fdb1075d4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "90d5b544-da87-430f-801d-91a8b4d7e968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 66,
                "y": 34
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "ba9eea63-91b3-448f-a098-6ce8440818bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "c0e45454-c0fc-4581-b750-b70d6a3b8774",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "9a98762d-ae92-40cb-827c-265b62490d6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 30,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "344a57d7-e67c-442d-9c3b-068dd7949f8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "f997a9cb-3fb2-482c-bb75-e04e65f190f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "2145beb4-739c-4dc3-9630-28618312db79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "6feecd62-937e-47c8-b968-cb9713cfe550",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "6142003c-2ed3-490f-af01-1aeac77dde6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "30ef846c-43f5-4176-a8b9-5a351c0c6799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "877ac0fa-a00f-4528-af84-16a7196a330a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "a6e5b44c-52a1-4049-bf26-bc07e220f59e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "9caf4dc5-948e-4eca-8b8e-fcdbd1bdb5c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "89bbcc4d-86d9-4155-be3d-8940355608f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 379,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "3c0fbcde-c77a-4b00-8bae-4b92dbd84aa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 30,
                "offset": -1,
                "shift": 24,
                "w": 24,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "b4aa4ecf-cb56-473c-94a8-2310415798b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 264,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "a92a925b-1ce2-4f42-837f-e28d32f12cf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "03cecb61-2cc6-4df3-8376-f3bb79c3879c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 297,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "cae639ac-8e4c-443b-b1f0-a6cd9b185424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "122a84f3-ef10-4ec4-b8e3-ed08dbaef493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 327,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "c2923b25-d866-41df-b459-d05b6c63ae3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "3bca2cfe-1196-4bdd-af87-77ceaba33f0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 354,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "26cffb2d-e291-4688-bec0-2a827afc29f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "cd86a5e9-12fe-4dd9-993e-db6140203352",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "d0154e1d-2208-439a-8b04-14d55207e864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 30,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 256,
                "y": 34
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "d8eac5f5-aef9-459b-8524-4d63599574b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 112,
                "y": 66
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "22c1bf61-fc36-4374-b5d0-9fc9776a3edd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 276,
                "y": 34
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "69dd4b38-d042-4283-8a4a-a75d7def1abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 161,
                "y": 66
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "c1b16dfc-f68d-4110-a924-a5d0795f109e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 181,
                "y": 66
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "b5071d57-4957-4f9d-a949-b2c1e77c3308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 201,
                "y": 66
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "d489fe43-3763-4b30-beb5-b70020e716f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 221,
                "y": 66
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "120c4a88-fcfd-4454-9249-5ddfc31b128e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 241,
                "y": 66
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "440bc145-0172-48bd-aaa4-ae4920dc6d7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 258,
                "y": 66
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "c0a87bf5-1670-43d5-8aa0-51a95ee34af6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 278,
                "y": 66
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "f8b1b925-842f-4c29-9316-b5f862ab4c4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 296,
                "y": 66
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "e2566ab6-93e6-435f-bcd1-59f78669e602",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 314,
                "y": 66
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "fdd045c7-7eae-4613-a9d1-4f6b9e2a9560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 348,
                "y": 66
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "6ef41900-74c1-431b-b1b1-98ec3e4c67a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 26,
                "y": 98
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "76706dbd-c236-46e0-a9c0-6e5c1861c484",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 366,
                "y": 66
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "7df0e34e-8761-4321-ba8d-4ebd12ddbf8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 383,
                "y": 66
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "b7215561-09d4-4a60-b34d-8a6bb98feee6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 399,
                "y": 66
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "fe2aa086-9f03-4e5d-9302-44565ea1df07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 414,
                "y": 66
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "29b1c303-e2cb-4668-8467-a683ddc7ec17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 429,
                "y": 66
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "2e24b8bc-f94e-4922-8041-ab1eeb68529d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 444,
                "y": 66
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "6b5f53f3-1960-49da-9380-e8be59e100ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 459,
                "y": 66
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "502d4fcf-8687-4984-a0a8-233f019a0ac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 474,
                "y": 66
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "61f406de-5ce6-47d6-9fb5-483b49912262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 30,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "9167c307-5831-4b97-a78e-b9d50239ecf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 146,
                "y": 66
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "0fb5a600-5995-42f3-a054-c30ad06ed360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 332,
                "y": 66
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "e2edd8c3-8ed7-4099-84bd-6a55aa6c7562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 130,
                "y": 66
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "601403b8-a5d4-49b2-b42a-2ed1d8b5229e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 418,
                "y": 34
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "f41803e7-96d4-47a2-9586-b5eb6accc140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 296,
                "y": 34
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "43efaecf-cead-403e-a54f-8c2f9520afdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 312,
                "y": 34
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "94f7228c-f2fb-49b2-b6d3-f07525525d22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 321,
                "y": 34
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "58acc6e2-4a30-42b1-a5e9-04b01103f4c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 30,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 330,
                "y": 34
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "5a3e3b83-c99d-45e7-a80a-48cb7a30677e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 30,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 341,
                "y": 34
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "ec679f1e-e727-4269-becc-0582d4711bd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 352,
                "y": 34
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "5e00a076-db9b-4b08-a37e-86240f10dd0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 369,
                "y": 34
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "d2f6b631-8eca-4ced-90a4-afabef99f14d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 384,
                "y": 34
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "54c2d8e6-3b55-444c-9289-e5218b043ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 401,
                "y": 34
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "3c30082d-3d28-4ad0-9574-26e690481224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 434,
                "y": 34
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "f3f5cbf5-61ec-441c-b854-ade741fe3b95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 95,
                "y": 66
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "2a2e1d59-b650-40ab-b07a-80f913641e13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 451,
                "y": 34
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "3841535a-beaf-4368-ada0-2e9eba50b716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 468,
                "y": 34
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "2583b4a6-f35a-46bd-8ce0-3be27bdc73de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 486,
                "y": 34
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "8af53272-68d0-43f6-a98d-f9e8729efee6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "15ae777f-7447-46f8-a92e-d9f0f91da280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 66
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "0331939f-860a-4165-bed6-84a548efc9ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 66
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "78e98326-b00f-4858-ab8a-d45c976bedb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 66
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "7e93461c-e3a5-4977-a1be-f2145456a5ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 62,
                "y": 66
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "ca0063e5-a083-4def-a397-ca29e9f7b673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 79,
                "y": 66
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "1ee2331b-f4ba-48c7-81b2-0b235b617881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 18,
                "y": 194
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "6b6fc622-2bb6-4cda-95df-c698a7b110c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 30,
                "offset": 5,
                "shift": 23,
                "w": 15,
                "x": 35,
                "y": 194
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 18,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}