{
    "id": "0ce6995e-1021-4619-a4c0-e2fad2744362",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "ft_name",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 2,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Tahoma",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9875c7e8-831b-41ed-962e-2b3e6b95c01a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2cbb97d1-9311-42a6-96c1-928e2adbb7b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 290,
                "y": 158
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f8aaea8a-56f2-4018-ba75-f97175156e50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 298,
                "y": 158
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "29b0f96e-66d3-42d2-96cc-1ee6059a4ae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 312,
                "y": 158
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "00bcc437-e2dc-4c90-9d47-671dc63b148e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 335,
                "y": 158
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0002a838-1b31-49fb-81c4-ab3305ba9f3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 354,
                "y": 158
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4bfc0b6a-8f6c-4c23-9301-0b03f4685b80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 1,
                "shift": 23,
                "w": 23,
                "x": 389,
                "y": 158
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3de641de-b076-44ae-9174-539ca93df1c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 414,
                "y": 158
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "32cf95db-9d86-4513-849b-5b2567738ec1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 422,
                "y": 158
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3f6f98f3-6db0-4c04-9b8d-db590ca20362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 436,
                "y": 158
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1bb16955-5991-4e32-baf3-f9ab5c70e200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 468,
                "y": 158
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a00e6b0d-12ef-4282-acbf-f9fa2badb5cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 119,
                "y": 197
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "91e4ed19-e94f-47dc-9655-241d6439d55c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": -1,
                "shift": 9,
                "w": 9,
                "x": 486,
                "y": 158
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "eb8583b3-bd56-4dc7-8adb-76d2655d87f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 497,
                "y": 158
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4ebbf588-2433-4641-b7de-23995a83053b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 2,
                "y": 197
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9a4e9e05-f7dc-4957-bff5-23ab329ec747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 10,
                "y": 197
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ec08400e-1e89-4236-9829-14cc0c75d4f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 26,
                "y": 197
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cf09acbf-cb3c-48a2-b1fd-63b2e5e6c6a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 45,
                "y": 197
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "906b43d4-3481-4f27-a4cf-f043ea8c17d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 62,
                "y": 197
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b009eb2e-6d4f-4a9c-beea-8096bd6831a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 80,
                "y": 197
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d1bc80b0-bf5d-4916-954a-95916dc92e05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 99,
                "y": 197
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "de74aa1f-8691-4dfd-87d9-656557d21a6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 271,
                "y": 158
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3cd570e9-9ab6-4104-b400-c13e6d4c8448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 449,
                "y": 158
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7242f3c5-86c0-4d3f-93fa-5eb2916990ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 252,
                "y": 158
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "fa2d7b69-e65b-41cb-8609-847804b1b0f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "498b78bf-6df6-4b01-909b-8387578d38fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 336,
                "y": 119
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8718a86c-865f-4321-b768-f451ac0585f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 356,
                "y": 119
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "12722e89-3f8e-4d63-8ce9-dcceee87dbc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 364,
                "y": 119
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1f0f0774-d2d6-44ff-a1c3-206b773f2dd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 375,
                "y": 119
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5be07002-ccd3-4741-8472-83fd715d8091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 396,
                "y": 119
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "43b50141-fdf2-426d-b505-1db22997b597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 416,
                "y": 119
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8bfaaabe-7c87-4b84-a6c0-9eee63670ed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 438,
                "y": 119
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4d88518c-f22c-4a3a-84e6-b081017a9543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 455,
                "y": 119
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "30371876-1aaf-4d1b-9f3c-72edb819a798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 481,
                "y": 119
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b3eb11b6-fc60-49f0-8dd0-cdbb45b0035a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 22,
                "y": 158
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0e35e6c2-5483-4f4b-ac9a-c5c1e85e5a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 207,
                "y": 158
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "36b419cd-a215-4560-a2f1-681389af8ba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 42,
                "y": 158
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1233842c-1afc-46ce-aa20-d1051e04fb50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 64,
                "y": 158
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c53795af-baef-4ae7-b4ee-46a319794a6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 81,
                "y": 158
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0672e412-500c-4f60-8a24-5aa3079881c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 98,
                "y": 158
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0589e4a8-8afe-46b4-ba89-d790782d8b8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 120,
                "y": 158
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e263fec8-95ec-4df8-a592-f7bb2aa8f5a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 140,
                "y": 158
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ae76dd08-5de6-431d-9156-5c7da2957dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 154,
                "y": 158
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1628f7bd-6e83-4047-8499-7cf30e3ebd84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 19,
                "x": 169,
                "y": 158
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fe360a8f-f452-495e-85e4-57cdd670d1d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 190,
                "y": 158
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b52e9e2a-cd0f-48e6-ade5-0c2df4a8bcf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 228,
                "y": 158
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "96a21d85-9a97-460e-9d32-9fb13591b0a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 140,
                "y": 197
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "550faed6-57de-48a2-bb94-10abb20f63b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 161,
                "y": 197
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "db0bb5d5-1759-46b0-8e7b-de9d211579c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 185,
                "y": 197
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5cfd11e3-70da-4dff-9328-a83c68e90777",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 113,
                "y": 236
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "444eab57-f3b5-4e7c-853f-5025e7b7d478",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 20,
                "x": 137,
                "y": 236
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8f0ac868-e859-4186-8e0a-6b55c290b736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 159,
                "y": 236
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "aac68b2e-73b1-4a75-88dc-d459c950b61f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 178,
                "y": 236
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8d855dad-9b0b-40c3-b747-c418ba77b154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 198,
                "y": 236
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2938848f-8173-4681-b0f2-c00c78b8a379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 219,
                "y": 236
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a7a0daf8-2152-4202-bcbd-25d208ce69c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 242,
                "y": 236
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7b593471-9966-426b-8173-f699608a9128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 274,
                "y": 236
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4501653e-6838-462d-9e1a-97c8c19522ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 298,
                "y": 236
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "089bfc84-ee2d-475a-8299-59af97dd52fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 321,
                "y": 236
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a289a3ae-c537-4971-91b2-24d3a68bc6d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 341,
                "y": 236
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fdf32958-37d6-4727-a446-82125995e92d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 353,
                "y": 236
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "067a84bb-133e-4f62-b41d-8d12e24c3dba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 369,
                "y": 236
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "efbfd635-7abe-45ea-b4d9-73c9443fb46c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 381,
                "y": 236
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ba9a799f-1e18-4984-9647-2f7e6fe2efe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 403,
                "y": 236
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "14be4633-d565-46b1-beee-186489e59d54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 425,
                "y": 236
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2f987a8d-5734-441e-aac6-b756a67b3dc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 436,
                "y": 236
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "24d7d726-8ca2-4b41-a1f4-a67bbf5e50a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 454,
                "y": 236
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d9467ace-4f7d-4248-a080-deb869b9398e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 473,
                "y": 236
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "76ab66cf-d4d0-4a1c-927f-79dae5a16599",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 490,
                "y": 236
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ff526103-1066-4426-931e-a803e9df2770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 275
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1a1f7c2c-add1-4f6f-a0c7-6f136dfcfe50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 13,
                "x": 98,
                "y": 236
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5e7a9377-bc82-404d-a651-62b83ffba6f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 79,
                "y": 236
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4edd4563-c925-4e6a-ba84-999f9572d680",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 61,
                "y": 236
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "11333cde-7bb4-4041-929c-c18828fc0cca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 361,
                "y": 197
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "91479834-a91f-4397-affb-d1b60eb29902",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": -2,
                "shift": 11,
                "w": 11,
                "x": 204,
                "y": 197
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ea51d3a5-d4d3-4b7c-aafa-5e07c3ccd05c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 217,
                "y": 197
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "483b27e9-57fd-4b6c-8b3c-52a78773996d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 236,
                "y": 197
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a687b564-1b26-40a5-90a1-b4c7c005b47a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 244,
                "y": 197
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2a83bf17-d74e-40c9-9add-a1c4a942b7f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 271,
                "y": 197
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f0f3023c-02ff-4ca6-8475-51b179aad467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 289,
                "y": 197
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6647344c-aade-4590-8113-c3f157d27e3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 309,
                "y": 197
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6e38469b-ca88-4a86-a237-f18753ecf857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 328,
                "y": 197
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fcad5854-94f3-4c1d-beb2-9acfbc186c20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 347,
                "y": 197
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ef2c9476-8cbc-4e07-b77b-614f76cfab9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 369,
                "y": 197
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "70f4127c-bb66-4b3f-bae3-ae302e0d6cd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 47,
                "y": 236
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ee77e79f-1a7d-496a-8eac-e68217fd8613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 386,
                "y": 197
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4e39c912-d2ca-4170-83d2-52e5d2337773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 404,
                "y": 197
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6c45077c-5cbb-44e0-9891-991bd968d9e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 423,
                "y": 197
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f718fad8-fa57-43e6-afd5-91c59f408c7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": -1,
                "shift": 18,
                "w": 19,
                "x": 451,
                "y": 197
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e032ae31-8a3a-4f56-8a55-972334d6d8f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 472,
                "y": 197
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "79886f2f-26e5-4521-9a51-e2a83b9c0d14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 492,
                "y": 197
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5e4f4ec5-ec21-4877-af43-20427ba1a876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 236
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "77161be2-f2ba-47b7-a4c5-2d90d74d4f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 7,
                "shift": 18,
                "w": 5,
                "x": 21,
                "y": 236
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c26a8b20-ea49-4f54-a80d-1ae18ac84a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 28,
                "y": 236
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6f545da1-0635-431e-adff-7becda43734d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 313,
                "y": 119
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "44127730-4ce6-4834-8e09-bc1c0d8b9171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 311,
                "y": 119
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "bb897a4d-4037-4722-8863-40bb060d072e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 303,
                "y": 119
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "927723f8-4d12-4b7c-a0d0-3a5990fd181b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 364,
                "y": 41
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "b48af5ee-b8d7-45fc-8f9f-7ad8ba8c84b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "23b5b1be-e5e1-478f-97b2-4c7935d0fa0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 21,
                "y": 41
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "3046622d-c60e-40c6-8471-c2b1b43f5d50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 39,
                "y": 41
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "48032707-012a-4ef6-a8e5-f26999965358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 37,
                "offset": 7,
                "shift": 18,
                "w": 5,
                "x": 60,
                "y": 41
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "bc99e2fc-0183-4ebe-98e5-268c29e25eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 67,
                "y": 41
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "df28482f-134a-4e46-9839-089ff752910d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 86,
                "y": 41
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "cac1b715-e3c0-43a8-bcc1-7dca704aba22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 37,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 100,
                "y": 41
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "f2afc0d1-d788-42dc-9b46-0a06a734795c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 127,
                "y": 41
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "d52d99f8-3c2e-4932-91e5-1e4e820f23f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 141,
                "y": 41
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "cc899270-abd1-4811-a414-aad3f719cf87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 170,
                "y": 41
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "6f4c4a23-575a-479d-9065-8d16c35cf3a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 352,
                "y": 41
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "6254cb2c-f4cc-4e43-847f-c6ca3f9fad23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 37,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 191,
                "y": 41
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "2207aa9f-e232-42fd-b3f7-e181a6f2330e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 37,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 218,
                "y": 41
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "6f2c95cd-0022-4b06-bdbe-894f4b1ce8b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 240,
                "y": 41
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "8c31f187-ac50-4786-bdba-2ae810b6728b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 255,
                "y": 41
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "b10e372f-38a9-43d4-a284-f23ba08aa2fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 276,
                "y": 41
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "7d8875c0-2af3-459c-a68d-a52b3b2d5366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 290,
                "y": 41
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "930ad509-3981-4e1d-821c-3d037722fbbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 37,
                "offset": 4,
                "shift": 16,
                "w": 9,
                "x": 304,
                "y": 41
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "51a85ea6-e230-4664-a934-1b26ba1c4d76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 315,
                "y": 41
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "c496c82f-7725-4e40-9bcc-181c059cf6fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 333,
                "y": 41
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "248122b3-0bbd-484c-92a1-2dc1b53f5c98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 37,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "a5aa2c90-b7bc-425c-adb3-92edf14878c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 37,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 160,
                "y": 41
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "924b3741-a9e2-4819-ba02-859edaa02e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 37,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 474,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "0bfc4188-36f9-4fa0-996e-3159627eabd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "5ec307a3-5d72-4070-929a-2b6dedadf633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "227f4397-d9ca-481b-ba17-67ddb6c82efe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 37,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "fc95b728-9933-4e42-864c-19469f041c2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 37,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "2ec79ed7-665a-4267-8481-3830782c9954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 37,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "c0167259-f5dc-41dd-8d24-8b6e566eade1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "c4206652-2996-438e-87d7-dc3379c404dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "2a52df88-4ee6-49f9-8e6d-ec76d51ad6aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "e2c805b2-74c6-49da-928f-0e7f869f9584",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "1ea9d566-f474-4dc6-9e5f-5840c836d3ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "091d6027-15a6-432c-8c34-84bd5a2915a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 251,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "9e914563-45d6-40e4-92f8-5241ea7f1526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 437,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "c4aff1a4-8c85-415c-ae30-1282146f144d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 37,
                "offset": -1,
                "shift": 29,
                "w": 29,
                "x": 274,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "e52f962f-ef47-43d8-af32-23c7f35afca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 37,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "0ed584c7-1b4f-4bba-8596-1cd92f304f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 326,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "87d91f9a-bc6c-4db0-bba7-b0b84e87c181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 343,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "00a3994e-3a49-4940-a0c3-945c1bb1070b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 360,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "bb3e3066-0c03-4e6b-9d7c-33bbddbc0c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 377,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "e465fe4c-dd1f-464a-8e68-2ffca61319f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "4098fe61-6ff0-4656-92eb-9d333dc75427",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 408,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "3a38516d-3d10-498c-9daf-4089fd4c6a39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "65fc2fea-3ad9-4785-843d-b184d5d3b62c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 460,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "4601c803-a382-48cf-a4c0-f8a83002718b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 381,
                "y": 41
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "14aa4ca7-2950-48b9-a323-c5d3c41baab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 311,
                "y": 80
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "d0d5a2c6-e3af-42a4-9a4d-51c0d91efe16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 405,
                "y": 41
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "99c1150d-385b-46f2-ac20-ace326802f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 368,
                "y": 80
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "f4b41bae-1286-4fcd-afbb-4aae266a4c7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 392,
                "y": 80
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "e2cb5a1d-5d96-43cf-a158-4177a5b96cb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 416,
                "y": 80
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "bd72b21f-5074-4e20-ad7f-d6d4214ee5a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 440,
                "y": 80
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "1451fbf8-7b14-4823-976c-e59d4812650e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 37,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 464,
                "y": 80
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "401e7346-4826-4d1f-92e9-1a19acd72336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 484,
                "y": 80
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "53d9feec-10f8-4e61-9a8e-5dfb6465c89d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "65a8d279-51c6-4b2b-bb21-4ff29ff3dd3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 23,
                "y": 119
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "1d0ef979-04a2-482d-a106-9c6223bda70f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 44,
                "y": 119
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "5a2c25a3-55be-4a58-a5af-dff5684781cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 84,
                "y": 119
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "c4919244-14a4-48b9-b030-400b4639ea21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 37,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 280,
                "y": 119
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "cea201e1-dce4-4cd8-8a98-12fddf856019",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 37,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 105,
                "y": 119
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "1a45de47-ddba-4053-bfa7-a18a1b9b7492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 124,
                "y": 119
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "b95fd9f5-64c9-4e07-ae3a-53b19947139a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 143,
                "y": 119
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "8b28dc9e-4b1e-49ee-b3fe-b42cf32cf2d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 161,
                "y": 119
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "bb987721-a36e-4790-840e-074c375e84b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 179,
                "y": 119
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "e9eb367c-73f3-40da-98fe-a32eac0c98b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 197,
                "y": 119
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "436c4137-9bbc-491a-9ca7-bc11c8ce3378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 215,
                "y": 119
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "52b256e8-8082-4a31-9528-1173c91a83ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 233,
                "y": 119
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "0a97027c-b829-4da4-b9dc-ad128856cc37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 37,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 251,
                "y": 119
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "dbabe274-106f-47d4-809f-d50971c55fc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 351,
                "y": 80
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "847da3e2-3073-4666-b619-f072f8023f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 65,
                "y": 119
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "36bdf082-7934-488f-828d-b9f2bbdeb75e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 332,
                "y": 80
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "a0d77d1a-a6f0-491e-9431-45a930ac6654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 80,
                "y": 80
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "82380093-5126-4922-8e0e-393f01d86e5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 429,
                "y": 41
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "6294d0c5-8df2-4c94-b7e0-7654b04a854a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 448,
                "y": 41
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "900678ad-29c4-4da8-befb-5ecb2729091b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 37,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 458,
                "y": 41
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "ae704983-0720-4805-863e-9cffc238b9d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 37,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 468,
                "y": 41
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "2445c5e8-232a-41f1-898f-c8d05dbfa106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 37,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 481,
                "y": 41
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "d879b7f7-2e51-4f8e-a0e6-146a17d2579a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "f53618ec-4f0d-4267-a738-784989b2ad06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 22,
                "y": 80
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "53e3a635-a61a-4598-a44d-5fcdc2cea1c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 40,
                "y": 80
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "b86a8665-e2db-4f88-b23b-73bf3984514c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 60,
                "y": 80
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "d223c27f-b201-4898-8b50-78d489c29abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 99,
                "y": 80
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "3771f45e-e182-41e3-9e70-c8bf725a33cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 291,
                "y": 80
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "10735122-30c4-4ab0-9fa8-8d83a7ad3a90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 119,
                "y": 80
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "43b0941e-de4b-46a9-a07e-7bc74ab13d74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 139,
                "y": 80
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "a279def8-fe45-41ec-ba27-4ad332ff8a5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 160,
                "y": 80
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "bf4fa3ee-bdbd-4c95-ab3e-a2b3b9db4d2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 180,
                "y": 80
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "df28e9f4-49ea-425c-adb7-104c0a59c276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 198,
                "y": 80
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "b9c99205-729e-46bd-9083-c8a8a85681b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 216,
                "y": 80
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "9c9703e6-5b75-4519-b648-c1ccf3a26a05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 234,
                "y": 80
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "1de3c38e-1bd7-4e6a-b79d-1a25396d87a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 37,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 252,
                "y": 80
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "58670bbf-4264-4aff-b9d3-2e8b24ea58a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 272,
                "y": 80
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "b0473086-6062-4ce6-b5d8-8761ff472d3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 37,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 21,
                "y": 275
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "2bf32db7-233a-418b-8931-84aa6d01024f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 37,
                "offset": 5,
                "shift": 28,
                "w": 18,
                "x": 41,
                "y": 275
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 22,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}