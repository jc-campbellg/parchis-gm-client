{
    "id": "1e61d92e-be7f-4887-a80d-7dcb559265b0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "ft_nameInitial",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Tahoma",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "90885db7-da3b-482a-a768-262af72f37a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c696cfec-4b0a-4e47-a3a2-1650ae3ad013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 470,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "66533fb3-809d-440e-95b1-93293fc6014b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 478,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "735d119e-522f-44da-ad1e-0b490f34a402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 30,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 490,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ad6f1f84-8cc2-40dd-95c9-d21dbf462ac2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 130
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f403b479-8206-44e1-8cdc-8a73e2951abc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 18,
                "y": 130
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "845c2346-b06a-4d70-a242-3fe5ebac5184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 19,
                "x": 47,
                "y": 130
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "37b46efc-a70e-429d-9d6e-d96653ca08a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 68,
                "y": 130
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6f762453-e2f0-4d81-bc7c-d2f658ff13c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 75,
                "y": 130
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b5ab8bfe-d690-4909-bba2-317e8b9b70cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 130
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "087f19be-13ec-490d-8e73-c1d42cb5a601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 114,
                "y": 130
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c0e65340-cf0c-4096-91e8-9a144d6111c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 250,
                "y": 130
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1c70f014-2538-4a24-bee7-89de03f14fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 129,
                "y": 130
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "830f63e4-a038-4768-9557-36b892424733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 139,
                "y": 130
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ea663891-e343-4bbb-99f2-c86f19a69e99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 150,
                "y": 130
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f00fae02-6053-47a7-ba91-8221458fcf89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 157,
                "y": 130
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7ca532af-a713-42bf-a06b-dd625f619514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 170,
                "y": 130
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "54b48729-e7ac-4cc0-bc0e-ec8a829a978a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 187,
                "y": 130
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "89adda8e-fe4b-44c0-ac30-b51126bf7fc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 201,
                "y": 130
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "66a6513b-8212-4d0f-8684-da5f6a88a62a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 217,
                "y": 130
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3cfc4f50-1aa6-42f2-b6a0-7244efb139d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 233,
                "y": 130
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b7572eb3-a04c-4cac-a5bd-79dcbbbbc019",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 454,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b546c1d6-00b9-4d6e-af83-b43b2832de48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 98,
                "y": 130
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fda2097f-163d-46fe-b061-262ea17260fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 438,
                "y": 98
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3f55f5a3-90f8-4a70-9dd4-9cd0303dbc51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 220,
                "y": 98
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "66413b7f-e3f7-488c-a37f-d84527a048d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 76,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3b4c59e2-e344-44be-88bb-95bbde3e9ded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 93,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "03943158-e4ab-4a83-9aeb-80f9bcc9414c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 100,
                "y": 98
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4d76cdec-8abe-483a-8bf5-8d9e1ebd4d67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 110,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "929ace1c-76a6-4c98-a989-ed5446c697e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 128,
                "y": 98
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c3d5b423-cf07-4b69-96f5-4dcb5ba0f62d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 146,
                "y": 98
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "15d65e65-2494-4021-a277-22a52ca1ce4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 164,
                "y": 98
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e3e77647-2e9d-4bf6-876a-b982080b5d8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 178,
                "y": 98
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "699c4b2a-e040-4d72-9de7-461eab800f0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 200,
                "y": 98
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0cc34d2c-ff43-433c-bd01-dc2cd99b8b85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 237,
                "y": 98
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b90b90ac-5b32-4a33-b718-49aa86860dbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 399,
                "y": 98
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ef02902a-377c-4dd0-b3d2-51f6ebec61b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 254,
                "y": 98
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6a482ef9-020a-4a4b-8c23-89bde8e2a56b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 273,
                "y": 98
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "099977f8-b19b-48dc-b04d-729efe2cbee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 288,
                "y": 98
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2263e402-09d1-49ee-acc0-cadccd2a1398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 303,
                "y": 98
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bebb9ad6-b38f-472a-ba0a-1228dbd9bb6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 322,
                "y": 98
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6da2d749-f561-4645-a971-051041469346",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 340,
                "y": 98
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4c3e3886-59f8-4604-bd00-3080a37c95b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 352,
                "y": 98
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9ace1f70-e266-40b8-bb21-4cb05db85906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 365,
                "y": 98
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e5fd954a-360d-4d1a-acbf-d87e577fa364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 384,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c379b667-d7f8-443b-9dd4-ac3d69dc378d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 417,
                "y": 98
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "703c5845-e2f8-4fb5-a648-f0cf53e8ca05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 268,
                "y": 130
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1d7ff759-3524-4e4d-a4d4-acaa98713536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 286,
                "y": 130
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7bf710dc-9557-4613-994a-d6387b477e01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 306,
                "y": 130
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3e8ed89b-755f-4447-afea-9167f42246b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 170,
                "y": 162
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "504a233c-a2a1-4be4-9c66-6adcf2150850",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 190,
                "y": 162
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1ffd9758-bc2a-4c50-b71b-889e8c690a7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 209,
                "y": 162
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7619ef67-9dcb-4e9b-ad33-9a5ceaf12909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 226,
                "y": 162
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0a29ca67-bf9f-41dc-bd5c-2cdec8272d79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 243,
                "y": 162
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "944cf685-5a2d-4899-98dd-7f5188e3daaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 261,
                "y": 162
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2bfe6575-564a-4a00-9f4b-d1eaaf7c4ccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 281,
                "y": 162
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c06ab426-7e43-4d5c-9a92-51605e657f02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 308,
                "y": 162
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c9604381-f748-4c20-aeb5-c70cae195b4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 328,
                "y": 162
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0f61f718-c033-4476-ba96-dbe63f7302f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 348,
                "y": 162
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e504f3b8-ec36-4db6-97b2-0a41025ca081",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 365,
                "y": 162
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "409683d1-644e-4b5a-b222-4181f66ada84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 376,
                "y": 162
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8538a9dd-5328-4598-8e5d-b593bf3c65da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 389,
                "y": 162
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "031d77a6-187a-4c83-8472-f4e389e93815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 399,
                "y": 162
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "34144d61-6e0e-4088-bdf2-addf4ad84ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 417,
                "y": 162
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5e601c5e-bb37-4996-a48a-765bd8028007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 436,
                "y": 162
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9c05fb5b-fc90-4190-860e-322d8a3f6f62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 445,
                "y": 162
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "726b2636-3863-4a4e-a780-fe0ce2403afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 460,
                "y": 162
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "adc5c0c6-7b68-4294-a0cd-557615da0a27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 476,
                "y": 162
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "56fa64f5-f1b7-42c3-b518-f52049885f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 491,
                "y": 162
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1818235a-b997-4d8e-b451-437a6af47690",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 194
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d1ad4d35-cdcf-44cc-ae78-072248064e09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 158,
                "y": 162
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e2c48f4a-a890-4d32-ae74-d1a3ec94ca64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 142,
                "y": 162
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ff576923-9014-4a13-acac-7dbd143a60c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 127,
                "y": 162
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "330adf19-086d-4364-be43-93d0310f86bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 456,
                "y": 130
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "228b02be-1f0c-4466-a0a4-495620947548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": -1,
                "shift": 9,
                "w": 9,
                "x": 323,
                "y": 130
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "df790018-c65f-445a-97da-dc43006e604d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 334,
                "y": 130
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "01b21abf-365a-4b5f-b7a8-9bf70a1c06fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 350,
                "y": 130
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "71e8498e-09b3-4b0d-ac41-e46440ea28e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 357,
                "y": 130
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3b8ae377-7bd2-4d96-a2e9-c1101ebf1c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 380,
                "y": 130
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "95285b59-5510-43bb-bd19-30c1c18d264b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 395,
                "y": 130
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c1d7c1a7-96ba-4158-99a6-89794d98ed03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 412,
                "y": 130
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1be41f94-56f8-481d-97d5-b740fb8f5274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 428,
                "y": 130
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a1305065-6f20-474d-a25d-57f7830a23b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 444,
                "y": 130
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "aaf4685c-e2ec-46a4-8bb5-e8070d718682",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 463,
                "y": 130
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "879eed6b-06b8-4612-86c9-f0f999da153f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 30,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 115,
                "y": 162
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "61bb4c87-8632-49a3-9cb4-26347b4a82c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 477,
                "y": 130
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2234f7f3-f329-4e27-8d45-85d9a7ec135d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 492,
                "y": 130
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "57366920-f26e-42c3-a8bc-332e6c04a177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7f6651fe-740e-4fbe-a577-84b12bd234a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 26,
                "y": 162
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3e7d947b-428c-483f-ae95-78ffc3fcc852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 44,
                "y": 162
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8589b817-f260-4f05-9e91-7426ae713490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 61,
                "y": 162
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b85a0e96-d990-4604-bae8-2b403f194eb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 76,
                "y": 162
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0f9f53b8-2789-40ce-b24b-0e0244692d02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 5,
                "shift": 15,
                "w": 5,
                "x": 92,
                "y": 162
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bc73a1ae-18cb-4507-97d3-a3adf32b7a02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 99,
                "y": 162
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1254f1dc-ea28-44b1-9e2a-7e42fd5ca555",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 30,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 56,
                "y": 98
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "f3e43119-6ba0-426c-96f4-b21dca88a3ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 0,
                "x": 54,
                "y": 98
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "55205592-4e3f-48f2-b707-bca1fd2b14d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 46,
                "y": 98
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "78309143-84d6-4cea-b3b7-2912b2850e43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 241,
                "y": 34
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "f5a2608c-f5f7-4fcf-952c-45e7c36ad9fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 429,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "856f4969-8ebf-43c7-9e3f-ec1c7e5d5fd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 445,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "d0b587d0-dd1c-490d-8ab9-9997360ea4ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 460,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "53641a15-37a6-4386-a7aa-7fd53e20d198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 30,
                "offset": 5,
                "shift": 15,
                "w": 5,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "6ff1260b-70dd-4f2d-92ac-94c81bc371ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 485,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "cb8a89ed-b6d2-4d3b-ba9e-dce5a9be1212",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "3cbe4ede-125a-453a-a7e3-c6507a024fcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 14,
                "y": 34
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "926f9e2c-f9cb-4704-9bc1-572650c6148b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 34
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "3e1316d1-336d-45a2-888a-b1ba9bd15a54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 50,
                "y": 34
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "b6549f3d-84dc-4882-bbcc-1d2a1cf64f49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 75,
                "y": 34
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "54f0d38e-ded9-4c15-8137-106366673f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 34
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "fa276e86-6a44-408b-804a-99cd839c8e39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 93,
                "y": 34
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "7be109d3-5ab2-4962-9747-36fd68962fce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 116,
                "y": 34
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "331f0502-9b75-41b8-9b0d-b440fa5133c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 135,
                "y": 34
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "20501024-59c8-4aa6-a0a1-9a4e1f0305fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 147,
                "y": 34
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "bf67d542-179b-4416-af9c-cc3120405c90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 165,
                "y": 34
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "9a234cbc-3290-4ac5-ba90-2a329af8a792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 177,
                "y": 34
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "2abe6011-d53e-4938-9040-2ee518f0eb74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 30,
                "offset": 4,
                "shift": 13,
                "w": 7,
                "x": 189,
                "y": 34
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "8a5bd624-b814-40b1-bf1f-b5fbbed98f2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 198,
                "y": 34
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "a4924a45-5a07-486d-a2d3-3b81339e8df6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 214,
                "y": 34
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "4047a6f3-3f7f-48c5-b028-8474b741c5a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "83d44198-338f-4ee4-b45f-bd205a247373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 66,
                "y": 34
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "dc4a6fc5-c3db-431f-a109-7d169f5d596f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "bb60f5bd-90c7-43b3-b143-3bcd30ad16cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "8856dbf4-111b-4772-9426-94a2ecd8e1f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 30,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "cb5b1361-7f18-4cea-bf41-9b0058776b66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "7d4c36f5-02b2-48ab-ad69-58abbc9f57ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "39c957be-6ed0-4d60-89b9-c9f05df2a7ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "44035f25-2cd8-4aa2-a3b2-c059638b862d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "130a348d-f39f-45e8-84ef-f9c44f179bea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "0d0af504-631f-4516-b527-5939584cbf02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "6e1fa0aa-7934-4471-bfe6-21fe49d93cb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "b9162d4d-4b7b-485b-8831-b60a214d98fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "e2395372-29bb-4676-ac34-83a803bac740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "5ba13d58-da28-473e-baad-ce3aed3cc3af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 379,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "0d1abaef-61cd-452f-acbf-84d2d093a565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 30,
                "offset": -1,
                "shift": 24,
                "w": 24,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "964271a8-3c1f-4855-be9f-5c10b2f67ec2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 264,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "47355bd8-d7d1-4fd3-b1ac-8abafb3b169d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "e9eeaf24-f774-41de-826e-9bb3c2ec7642",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 297,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "8e345d2e-b23d-4b07-982d-b5339874a42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "0f8654cf-f254-4fc0-aa60-60683bbe9010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 327,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "8ff34eb0-11b1-4889-8a0a-46444ada5165",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "3d541c15-4bef-487d-a3c5-e1d547925f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 354,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "831f4ce3-3515-475b-bcde-e304aac1d0d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "147f9c47-e00a-46d4-bd03-f47a0ef16faf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "17a8860c-ad42-40ac-b495-bd8d944fe5bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 30,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 256,
                "y": 34
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "2b6cafaa-7d74-4495-96c2-d53691e0afd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 112,
                "y": 66
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "74266f17-a6ca-4283-ad55-86047ce875a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 276,
                "y": 34
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "1b99a239-5a13-486a-a212-ac474607eb31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 161,
                "y": 66
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "7ba30630-dfa3-4c8b-97b2-1bfb2ae3f67a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 181,
                "y": 66
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "642b6c76-7500-46ed-9676-445c2abbe8c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 201,
                "y": 66
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "55ece319-1a27-486c-999a-4c3c050f9187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 221,
                "y": 66
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "9c3acf15-3efa-4ea1-9eae-5c7fe46726f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 241,
                "y": 66
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "5cce2ca2-d016-4759-9dc9-194f326a8815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 258,
                "y": 66
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "9cf42f49-ff94-4741-9a75-a5e6d9925a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 278,
                "y": 66
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "5fc5c3ba-c77e-45f1-b8bc-797e3b42d7ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 296,
                "y": 66
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "b3f71436-671b-4a71-b5b5-79a2e5d126ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 314,
                "y": 66
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "303a2dc4-3f3b-43b2-98b7-8e1702fe7e33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 348,
                "y": 66
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "e0164e90-c109-4caf-9256-df537e636e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 26,
                "y": 98
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "5b6a6696-60fa-4fd9-bbed-5e89cbaaab0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 366,
                "y": 66
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "d5bfa797-34d7-4bbb-96d5-1a7a188ffde8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 383,
                "y": 66
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "cf957ffd-3760-40c6-a344-7bdaa97f04e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 399,
                "y": 66
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "42a9ca80-f523-45da-9044-84e3e4c6dcd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 414,
                "y": 66
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "12e41074-1d8b-45ed-90fc-58dc2c64e296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 429,
                "y": 66
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "b9679b96-277e-4af2-afbf-27cf5a44a965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 444,
                "y": 66
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "50549dc2-3782-47ca-b1f6-6261bd85ecd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 459,
                "y": 66
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "fda1b0bc-b50b-4d49-869e-9415f0e7b723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 474,
                "y": 66
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "79a46386-959c-4040-bc05-5f1a7362c6a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 30,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "237d14f8-4663-4877-92ac-0a1bed275558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 146,
                "y": 66
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "378d9d1e-0cf1-4842-837e-ea9d293790ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 332,
                "y": 66
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "a853a22a-b51c-4d0a-9509-5c707d99f7fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 130,
                "y": 66
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "9025eca0-c3f1-48bc-be20-6709b5ff3bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 418,
                "y": 34
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "e6ca9686-4fb5-493f-b1f9-01edd0a28b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 296,
                "y": 34
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "2355ef58-2ca0-4a87-ba33-af1c8c4db6f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 312,
                "y": 34
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "a874f480-7c81-402c-a841-773fe5c9450f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 321,
                "y": 34
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "4aa9b4d6-ea80-4512-9a22-b4d3a3c7077d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 30,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 330,
                "y": 34
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "63d1e098-f1bd-45ff-b02a-845897a0071d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 30,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 341,
                "y": 34
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "a86e5fee-02f3-4bbb-80c5-033ee4309040",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 352,
                "y": 34
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "ca2a69b5-7c3a-48eb-a7ec-8fcf8c1074c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 369,
                "y": 34
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "61ae249a-4271-45ea-9c45-334d2eff0071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 384,
                "y": 34
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "858ce382-77aa-47ee-8c80-22075d9e6ba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 401,
                "y": 34
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "cbf99144-6b06-4b5b-a528-05e33bcc9d35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 434,
                "y": 34
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "ad6c3c7d-a219-4a35-bac7-766999799a3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 95,
                "y": 66
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "03d1effd-8b57-49c3-a358-83bee8f934b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 451,
                "y": 34
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "fb1bc157-64b3-428e-aa48-113e06b30e43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 468,
                "y": 34
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "8c35fe54-9909-43f0-bb9d-ba3c1dcf71fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 486,
                "y": 34
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "4344e5ec-376b-4a1d-b0b0-7ffe489be212",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "98db7b63-93e7-4431-a0cc-c7b37e4225e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 66
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "b14df640-469a-499a-8baf-05e71f847ef0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 66
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "6abae975-1b68-41b4-ad66-b0ccb6a1b9e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 66
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "917ad18a-ec33-4087-b988-8c87525d3564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 62,
                "y": 66
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "fa48bec3-2741-4074-bb87-4492e9cfe1b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 79,
                "y": 66
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "feac7294-36f9-4092-9ee9-cebabc83a024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 18,
                "y": 194
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "3f6dbb43-236b-4d08-af52-339925734bcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 30,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 35,
                "y": 194
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 18,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}