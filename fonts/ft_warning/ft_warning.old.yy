{
    "id": "1e61d92e-be7f-4887-a80d-7dcb559265b0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "ft_warning",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 4,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Tahoma",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "65a9da50-897f-4821-b4fc-514ef681109b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 84,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "dbd3ef70-961d-4927-9155-29563567b725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 84,
                "offset": 4,
                "shift": 23,
                "w": 14,
                "x": 746,
                "y": 346
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b8a825d6-2183-4b0e-8af0-d91f9adc7caa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 84,
                "offset": 4,
                "shift": 33,
                "w": 25,
                "x": 762,
                "y": 346
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "eabef6e2-cd05-45a7-b9a7-f8eb4ad44ef0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 84,
                "offset": 4,
                "shift": 55,
                "w": 47,
                "x": 789,
                "y": 346
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "61a81ad4-5754-42ec-bdad-d746c24036d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 84,
                "offset": 3,
                "shift": 43,
                "w": 38,
                "x": 838,
                "y": 346
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3f24cacd-f024-4bf1-a910-7d91ed37b7a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 84,
                "offset": 3,
                "shift": 80,
                "w": 74,
                "x": 878,
                "y": 346
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "11995c21-41ee-47d1-a758-f854052d1566",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 84,
                "offset": 2,
                "shift": 52,
                "w": 53,
                "x": 954,
                "y": 346
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "90543876-f718-4441-97bb-16d8d0ab94e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 84,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 1009,
                "y": 346
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "039e0da7-a4fd-4609-a0b4-422b23d7a30b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 84,
                "offset": 3,
                "shift": 30,
                "w": 25,
                "x": 2,
                "y": 432
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "28f76096-c042-4090-b9b2-39b9ffb17151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 84,
                "offset": 2,
                "shift": 30,
                "w": 25,
                "x": 29,
                "y": 432
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b2c231f8-cd9e-4f5a-983c-048c2751f43a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 84,
                "offset": 4,
                "shift": 43,
                "w": 34,
                "x": 96,
                "y": 432
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1bb4fc22-a9f1-4bd0-b0a6-7afbc5e75082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 84,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 423,
                "y": 432
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "09711604-32e5-4e55-a02d-8b4070085b69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 84,
                "offset": -1,
                "shift": 21,
                "w": 20,
                "x": 132,
                "y": 432
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1e8f45b9-ee71-46dd-8073-05943fa7525b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 84,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 154,
                "y": 432
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "cfa8ecc2-7b50-46f1-9792-1bf833d41893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 84,
                "offset": 4,
                "shift": 21,
                "w": 13,
                "x": 179,
                "y": 432
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e4f9635d-92fd-4b2a-9be0-6dd418288f42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 84,
                "offset": 3,
                "shift": 39,
                "w": 30,
                "x": 194,
                "y": 432
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a09db464-61f1-45e1-94bc-fd68a4f06462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 84,
                "offset": 2,
                "shift": 43,
                "w": 39,
                "x": 226,
                "y": 432
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8caf5e60-4eec-47cc-86e8-55380d3280f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 84,
                "offset": 6,
                "shift": 43,
                "w": 33,
                "x": 267,
                "y": 432
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cbc5b1f6-ebf8-4520-be0b-ac5a7ec52377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 84,
                "offset": 4,
                "shift": 43,
                "w": 37,
                "x": 302,
                "y": 432
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d48c8748-0752-4e80-ae7e-9d7cae4e6bd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 84,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 341,
                "y": 432
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "30257e89-de07-4afe-829a-d9142bcb28f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 84,
                "offset": 1,
                "shift": 43,
                "w": 41,
                "x": 380,
                "y": 432
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5d157b35-5205-4783-a666-a3013925ce92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 84,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 707,
                "y": 346
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "168d770f-f0ec-4c12-a07d-8aa0660af7dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 84,
                "offset": 3,
                "shift": 43,
                "w": 38,
                "x": 56,
                "y": 432
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "655fc732-4d6d-4b59-bc63-dd43373f8395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 84,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 668,
                "y": 346
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c955cea4-5b71-4f55-b3da-403a0da3b806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 84,
                "offset": 2,
                "shift": 43,
                "w": 39,
                "x": 144,
                "y": 346
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c0ea40c1-d0fa-4157-b66c-77cf0331f008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 84,
                "offset": 2,
                "shift": 43,
                "w": 38,
                "x": 798,
                "y": 260
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7d622802-2b61-4222-91c2-24c40ca03146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 84,
                "offset": 6,
                "shift": 24,
                "w": 12,
                "x": 838,
                "y": 260
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f5b504f8-56e9-4fa6-8462-7012b3c54803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 84,
                "offset": 1,
                "shift": 24,
                "w": 19,
                "x": 852,
                "y": 260
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "fef803b2-b717-4ad3-8970-647adeec6235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 84,
                "offset": 5,
                "shift": 55,
                "w": 43,
                "x": 873,
                "y": 260
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7c55eb5d-9b6a-48b8-aacc-65db3788891d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 84,
                "offset": 7,
                "shift": 55,
                "w": 41,
                "x": 918,
                "y": 260
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "537bc3bc-d9b7-48b4-9ba6-f1c4d8dae988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 84,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 961,
                "y": 260
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "02f52afe-e77a-4905-ad8a-d26ad0bd904b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 84,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 2,
                "y": 346
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7c1af296-4c37-413b-81be-76b1757d87bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 84,
                "offset": 3,
                "shift": 62,
                "w": 55,
                "x": 37,
                "y": 346
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "dfa61d0b-1883-43bb-9ba4-d59eb59e1593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 84,
                "offset": -1,
                "shift": 46,
                "w": 48,
                "x": 94,
                "y": 346
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5bd98d33-bad7-4b33-b65e-d923e1a4e411",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 84,
                "offset": 5,
                "shift": 46,
                "w": 39,
                "x": 185,
                "y": 346
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a31e334b-531c-4224-895a-0dadadb8f27b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 84,
                "offset": 2,
                "shift": 45,
                "w": 41,
                "x": 573,
                "y": 346
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f8ef6789-00a8-47e9-8a67-68593268116b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 84,
                "offset": 5,
                "shift": 51,
                "w": 44,
                "x": 226,
                "y": 346
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "849cad01-b5f8-4f86-90df-29df341bb526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 84,
                "offset": 5,
                "shift": 41,
                "w": 33,
                "x": 272,
                "y": 346
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "85bbc35c-8532-4567-9ecc-5dd3b726ac95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 84,
                "offset": 5,
                "shift": 39,
                "w": 33,
                "x": 307,
                "y": 346
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "62801aec-704c-47a9-b719-9a5cab09d9af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 84,
                "offset": 2,
                "shift": 50,
                "w": 44,
                "x": 342,
                "y": 346
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cd7f2556-8566-4a7e-9a3b-0387e16cb1b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 84,
                "offset": 5,
                "shift": 51,
                "w": 42,
                "x": 388,
                "y": 346
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0988b82c-ff8d-4ec4-aa25-2ff0a0481079",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 84,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 432,
                "y": 346
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "37e1b54a-5ce6-45fa-823a-6a6eaad9cc75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 84,
                "offset": 0,
                "shift": 34,
                "w": 30,
                "x": 461,
                "y": 346
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d827ed32-1e1b-47e6-ad69-77251827a06e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 84,
                "offset": 5,
                "shift": 47,
                "w": 43,
                "x": 493,
                "y": 346
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fb2a45fb-65ba-4503-b0a9-9b6ff95a45f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 84,
                "offset": 5,
                "shift": 38,
                "w": 33,
                "x": 538,
                "y": 346
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "56d1f5b9-05ac-44ae-9296-594e2d4096ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 84,
                "offset": 5,
                "shift": 60,
                "w": 50,
                "x": 616,
                "y": 346
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "05c33f33-7c01-4de2-a4be-e4c426c22ebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 84,
                "offset": 5,
                "shift": 52,
                "w": 42,
                "x": 468,
                "y": 432
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1204cb34-ffdd-45f5-9313-b29fa94aa1ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 84,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 512,
                "y": 432
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8000dc44-0253-4c87-9abc-21a2b2a23326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 84,
                "offset": 5,
                "shift": 44,
                "w": 38,
                "x": 562,
                "y": 432
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8f3b4a33-2b20-416a-a4bd-0123589f0c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 84,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 451,
                "y": 518
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4eaa755d-f47a-4301-afeb-4345b2713757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 84,
                "offset": 5,
                "shift": 49,
                "w": 46,
                "x": 501,
                "y": 518
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "156a264e-9479-4b05-bf25-4fa6c5be240b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 84,
                "offset": 2,
                "shift": 42,
                "w": 40,
                "x": 549,
                "y": 518
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "55098672-8e6c-40c7-a9cc-cb5ccd678856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 84,
                "offset": 0,
                "shift": 41,
                "w": 42,
                "x": 591,
                "y": 518
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b4fa2448-a5c4-496a-9990-e497e9e26418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 84,
                "offset": 4,
                "shift": 50,
                "w": 42,
                "x": 635,
                "y": 518
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6d26b6a6-bf7d-428a-b4ea-3d7e32889500",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 84,
                "offset": -1,
                "shift": 45,
                "w": 47,
                "x": 679,
                "y": 518
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b1aad770-a52f-478d-8cf4-a068d106fdc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 84,
                "offset": 0,
                "shift": 69,
                "w": 68,
                "x": 728,
                "y": 518
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8852a8cc-0faf-474e-81c7-07b9c4aee081",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 84,
                "offset": -1,
                "shift": 46,
                "w": 48,
                "x": 798,
                "y": 518
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "242a1089-ada2-4347-aa22-9daba3d0ec70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 84,
                "offset": -1,
                "shift": 45,
                "w": 47,
                "x": 848,
                "y": 518
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4557b758-3aeb-49cf-a207-1aeba177eab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 84,
                "offset": 1,
                "shift": 42,
                "w": 40,
                "x": 897,
                "y": 518
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6c9ea5e6-725d-4627-b3d2-8ea1de669dd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 84,
                "offset": 5,
                "shift": 30,
                "w": 21,
                "x": 939,
                "y": 518
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b81cf161-5f47-422b-aefa-b6c768d8f877",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 84,
                "offset": 6,
                "shift": 39,
                "w": 30,
                "x": 962,
                "y": 518
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "083a225e-2c55-457f-9247-e83636fb4098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 84,
                "offset": 4,
                "shift": 30,
                "w": 21,
                "x": 994,
                "y": 518
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9c6be7de-c2c5-4381-985a-df95b9d74e9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 84,
                "offset": 5,
                "shift": 55,
                "w": 45,
                "x": 2,
                "y": 604
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b8b8477f-ef6b-40f1-b04a-f22f096f7135",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 84,
                "offset": -1,
                "shift": 43,
                "w": 45,
                "x": 49,
                "y": 604
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c29de2f0-08b5-4dc9-8ad1-bcd0a4573f78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 84,
                "offset": 8,
                "shift": 37,
                "w": 19,
                "x": 96,
                "y": 604
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "080e9b25-30f9-4e07-a4a8-d6f19a6efafe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 84,
                "offset": 1,
                "shift": 40,
                "w": 36,
                "x": 117,
                "y": 604
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "786c15fb-7856-4044-96dc-d9cc962e9c14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 84,
                "offset": 4,
                "shift": 42,
                "w": 37,
                "x": 155,
                "y": 604
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "938e2c34-a953-4fef-b918-f8261205d316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 84,
                "offset": 1,
                "shift": 35,
                "w": 34,
                "x": 194,
                "y": 604
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3b4ba07f-d9be-4996-a878-92e5f493aacf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 84,
                "offset": 1,
                "shift": 42,
                "w": 37,
                "x": 230,
                "y": 604
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "45adad1d-e041-4526-9d87-4bfcfaf56190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 84,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 269,
                "y": 604
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e2ab69a0-90b2-4058-b405-0a49cec0f0f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 84,
                "offset": 0,
                "shift": 26,
                "w": 28,
                "x": 421,
                "y": 518
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d069004a-12cd-46c7-88d3-d7bea80d8436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 84,
                "offset": 1,
                "shift": 42,
                "w": 37,
                "x": 382,
                "y": 518
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1362b2b7-4971-4472-8c5e-f11491ff94e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 84,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 345,
                "y": 518
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "adbc0d8c-b3e3-4e24-91e4-7774a774bd16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 84,
                "offset": 4,
                "shift": 20,
                "w": 13,
                "x": 923,
                "y": 432
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "442fc5a2-7ce2-4997-87b1-6d98802f2afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 84,
                "offset": -3,
                "shift": 24,
                "w": 23,
                "x": 602,
                "y": 432
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "cbbbf6fd-34b6-4cb8-b6d7-b43fb6238d4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 84,
                "offset": 4,
                "shift": 40,
                "w": 38,
                "x": 627,
                "y": 432
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9edaba6a-c760-431b-a68b-5423cc0a0f97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 84,
                "offset": 4,
                "shift": 20,
                "w": 12,
                "x": 667,
                "y": 432
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0a389bf6-3b28-4522-8e73-f9eaef398f04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 84,
                "offset": 4,
                "shift": 64,
                "w": 56,
                "x": 681,
                "y": 432
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3bc98b0a-ca44-4438-a482-61dc4e7be88b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 84,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 739,
                "y": 432
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "95eb2cd4-f80c-4e5a-b70f-50aa2889b587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 84,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 776,
                "y": 432
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5e8ee1de-7f3e-4721-9444-5c7c63e64092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 84,
                "offset": 4,
                "shift": 42,
                "w": 37,
                "x": 817,
                "y": 432
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2c23bb83-d9bd-4637-931c-424ad0322de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 84,
                "offset": 1,
                "shift": 42,
                "w": 37,
                "x": 856,
                "y": 432
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9d23cd6e-2498-4da7-bc7e-941b3f8b3a4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 84,
                "offset": 4,
                "shift": 29,
                "w": 26,
                "x": 895,
                "y": 432
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4a7d5ada-0718-4a1a-aad9-9380a770a6e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 84,
                "offset": 1,
                "shift": 34,
                "w": 33,
                "x": 938,
                "y": 432
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "7e9d1c85-8b82-4572-b7b1-c37c6a3fa8e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 84,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 315,
                "y": 518
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3daf11db-6339-4525-a9c5-281622fceee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 84,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 973,
                "y": 432
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7b359e20-964c-42af-b2b4-8d78aec6a743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 84,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 2,
                "y": 518
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1f430b26-d98b-42a7-a631-9b8954e81f80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 84,
                "offset": 0,
                "shift": 60,
                "w": 60,
                "x": 43,
                "y": 518
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "015998db-7538-4856-9d05-341387d3c369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 84,
                "offset": -1,
                "shift": 41,
                "w": 42,
                "x": 105,
                "y": 518
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4e04ad6e-69a1-4168-ab3a-bb2712ddcee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 84,
                "offset": -1,
                "shift": 39,
                "w": 40,
                "x": 149,
                "y": 518
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "09830a0e-9a02-4154-a469-21340a56c103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 84,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 191,
                "y": 518
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c5a1cee8-347c-44fc-b8fa-181893c71c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 84,
                "offset": 1,
                "shift": 42,
                "w": 37,
                "x": 226,
                "y": 518
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "fc87f28b-cf7b-4494-a619-b3b5ff29647e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 84,
                "offset": 16,
                "shift": 43,
                "w": 10,
                "x": 265,
                "y": 518
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8a72f260-f290-4ad8-ac1b-5b5d9d5b7027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 84,
                "offset": 4,
                "shift": 42,
                "w": 36,
                "x": 277,
                "y": 518
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c8161191-d04c-43d9-9e7c-5fb083a5555b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 84,
                "offset": 4,
                "shift": 55,
                "w": 47,
                "x": 749,
                "y": 260
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "7eee4855-7d0b-4a76-9264-68db87c22cd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 84,
                "offset": 0,
                "shift": 20,
                "w": 0,
                "x": 747,
                "y": 260
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "e79bd718-f41c-46a4-9f7f-500dd05e4d86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 84,
                "offset": 4,
                "shift": 23,
                "w": 14,
                "x": 731,
                "y": 260
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "85f17657-5cef-4bc7-8cfc-a25ecc0ca144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 84,
                "offset": 4,
                "shift": 43,
                "w": 34,
                "x": 797,
                "y": 88
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "71777d1e-5ee5-4bd5-9402-61117dd7b138",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 84,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 41,
                "y": 88
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "c81c9a17-836b-4db6-9aa3-66c30c5fba74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 84,
                "offset": 3,
                "shift": 43,
                "w": 36,
                "x": 80,
                "y": 88
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "d0887c67-28a3-4487-8dde-93b262a68fe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 84,
                "offset": 0,
                "shift": 43,
                "w": 42,
                "x": 118,
                "y": 88
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "512c9304-ad6b-493b-8ef6-bbeb10049a46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 84,
                "offset": 16,
                "shift": 43,
                "w": 10,
                "x": 162,
                "y": 88
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "d08af8ed-c738-4b21-ac6a-cce9a35e236d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 84,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 174,
                "y": 88
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "8b275d63-4451-45b7-9469-da818c353906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 84,
                "offset": 5,
                "shift": 37,
                "w": 26,
                "x": 213,
                "y": 88
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "b4b18a7d-5574-4794-b47c-1d6bbf1addaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 84,
                "offset": 2,
                "shift": 62,
                "w": 58,
                "x": 241,
                "y": 88
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "21370d42-52f8-4949-a500-52b8f04e3061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 84,
                "offset": 2,
                "shift": 34,
                "w": 28,
                "x": 301,
                "y": 88
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "f5bb8f22-4472-4c61-ae47-5f1cf7a724a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 84,
                "offset": 4,
                "shift": 47,
                "w": 38,
                "x": 331,
                "y": 88
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "0aeb7779-0f47-4727-ab96-6d45abfc9cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 84,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 392,
                "y": 88
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "07a3fdc3-94e7-4553-baad-0b262ecd5fe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 84,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 772,
                "y": 88
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "e30c967e-70c5-4796-a98b-708ddac2ccb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 84,
                "offset": 2,
                "shift": 62,
                "w": 58,
                "x": 437,
                "y": 88
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "794c2f9e-9061-4e63-85bb-29ad721069e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 84,
                "offset": -1,
                "shift": 43,
                "w": 45,
                "x": 497,
                "y": 88
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "dbcf1627-bdb7-47a0-bcc6-103ba4d52d4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 84,
                "offset": 4,
                "shift": 35,
                "w": 27,
                "x": 544,
                "y": 88
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "50222c79-91b8-41da-9405-0c137ce0e926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 84,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 573,
                "y": 88
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "505b5276-137c-4480-b6c2-8ef03a2d6863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 84,
                "offset": 5,
                "shift": 36,
                "w": 27,
                "x": 618,
                "y": 88
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "d946b681-c582-473a-8377-b1271467366c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 84,
                "offset": 5,
                "shift": 36,
                "w": 26,
                "x": 647,
                "y": 88
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "2ac67c4c-228c-4ad3-bde4-d624200760d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 84,
                "offset": 11,
                "shift": 37,
                "w": 18,
                "x": 675,
                "y": 88
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "343b6ae1-b313-4733-a55c-23b3fe429bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 84,
                "offset": 4,
                "shift": 44,
                "w": 36,
                "x": 695,
                "y": 88
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "ce8408c7-7695-4071-b172-e74eda1d4557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 84,
                "offset": 2,
                "shift": 43,
                "w": 37,
                "x": 733,
                "y": 88
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "4336aaa7-e583-4618-81e1-833cc4db3fe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 84,
                "offset": 6,
                "shift": 24,
                "w": 12,
                "x": 27,
                "y": 88
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "1d0d0c2e-8fce-4b5d-96b5-5bd0112ef011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 84,
                "offset": 9,
                "shift": 37,
                "w": 19,
                "x": 371,
                "y": 88
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "d3b99ec5-1519-4e96-9d3a-0bb16962a2c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 84,
                "offset": 7,
                "shift": 36,
                "w": 23,
                "x": 2,
                "y": 88
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "0887bf4a-f790-4051-ba8e-597563ae1208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 84,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 511,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "de9f14c7-8a01-493d-93c8-5755840d2ba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 84,
                "offset": 5,
                "shift": 47,
                "w": 38,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "880c6ec8-5bc9-4afa-8566-f0c6c3e141a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 84,
                "offset": 5,
                "shift": 76,
                "w": 67,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "eb51d49c-bdb8-4e4e-a896-73143a5a1eb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 84,
                "offset": 4,
                "shift": 76,
                "w": 69,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "225f0e55-0fa4-4a89-ab3e-a61932d312e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 84,
                "offset": 4,
                "shift": 76,
                "w": 70,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "099fddaa-c385-4401-a260-37bd8c34bedb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 84,
                "offset": 2,
                "shift": 38,
                "w": 33,
                "x": 276,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "42dbf0c5-84ce-4f64-adb6-093cbd781ba3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 84,
                "offset": -1,
                "shift": 46,
                "w": 48,
                "x": 311,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "ab00b8e8-cc8c-4b54-80bd-4a925698afe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 84,
                "offset": -1,
                "shift": 46,
                "w": 48,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "b22f0f3b-58ec-43ca-b8ef-1742675882c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 84,
                "offset": -1,
                "shift": 46,
                "w": 48,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "5a2c75ce-995f-4607-88a1-d73bb42c6a63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 84,
                "offset": -1,
                "shift": 46,
                "w": 48,
                "x": 461,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "b0153b3e-49db-474e-9180-eeeddcfbd8b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 84,
                "offset": -1,
                "shift": 46,
                "w": 48,
                "x": 544,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "9f33a905-ee62-436d-aa6c-665b41270f4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 84,
                "offset": -1,
                "shift": 46,
                "w": 48,
                "x": 932,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "16db0771-668b-4252-a5b2-fb4c6e75e378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 84,
                "offset": -1,
                "shift": 66,
                "w": 64,
                "x": 594,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "96238f72-5767-4b72-bd29-9453b73e310a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 84,
                "offset": 2,
                "shift": 45,
                "w": 41,
                "x": 660,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "6543169d-1b52-46dc-a224-7592d80ddb4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 84,
                "offset": 5,
                "shift": 41,
                "w": 33,
                "x": 703,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "e250908a-9966-4739-9b82-9c5ef0a427cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 84,
                "offset": 5,
                "shift": 41,
                "w": 33,
                "x": 738,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "d2e62a7d-bc29-4925-a469-9c12c23565c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 84,
                "offset": 5,
                "shift": 41,
                "w": 33,
                "x": 773,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "c22fede3-2659-47dc-90e1-1325a62581ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 84,
                "offset": 5,
                "shift": 41,
                "w": 33,
                "x": 808,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "62717857-5e94-4540-b2a0-86839873c470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 84,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 843,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "57ba5b48-9717-4b5f-b629-9e2b4d1021d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 84,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 872,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "e79dd9c4-ae99-48b4-97fa-fe6626b8b743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 84,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 901,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "6ad9f419-cb08-4af1-adbd-f9b90cd3f857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 84,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 982,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "d837b014-786f-4a1f-a661-a32fc76564f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 84,
                "offset": 0,
                "shift": 52,
                "w": 50,
                "x": 833,
                "y": 88
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "dcfc8024-3e50-41e2-8fc6-12649aed0473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 84,
                "offset": 5,
                "shift": 52,
                "w": 42,
                "x": 692,
                "y": 174
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "fbc2f428-9be0-4b0c-a888-142fef866704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 84,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 885,
                "y": 88
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "bffe2f3d-9fda-4e76-82c4-23a49c096aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 84,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 812,
                "y": 174
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "03bcb5ed-335c-4d03-b1b5-dd903fb86e58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 84,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 862,
                "y": 174
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "f1625a77-63af-4262-a5f6-cefc63524172",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 84,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 912,
                "y": 174
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "66f7cf25-ca22-4c9d-b349-5a02a7fc007d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 84,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 962,
                "y": 174
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "b2455a96-0a3a-4b42-9ab7-b34848bc099f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 84,
                "offset": 7,
                "shift": 55,
                "w": 41,
                "x": 2,
                "y": 260
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "8fbbf2e2-9a44-4fc9-8777-2fba7f742ff7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 84,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 45,
                "y": 260
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "4952987c-c13e-428c-8a34-51a33e7f866f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 84,
                "offset": 4,
                "shift": 50,
                "w": 42,
                "x": 95,
                "y": 260
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "4c64d636-df80-4859-be79-28ce2bf50621",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 84,
                "offset": 4,
                "shift": 50,
                "w": 42,
                "x": 139,
                "y": 260
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "ee98803a-5609-41f2-967b-42c722c06a76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 84,
                "offset": 4,
                "shift": 50,
                "w": 42,
                "x": 183,
                "y": 260
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "414a2edc-4e15-446b-afe4-c50244bd8b81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 84,
                "offset": 4,
                "shift": 50,
                "w": 42,
                "x": 267,
                "y": 260
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "ced53bae-edab-4eab-9b40-0399a3b64169",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 84,
                "offset": -1,
                "shift": 45,
                "w": 47,
                "x": 682,
                "y": 260
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "ccab7ed7-24fe-49fe-9bb5-3b96e02e0e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 84,
                "offset": 5,
                "shift": 44,
                "w": 38,
                "x": 311,
                "y": 260
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "4a4db9da-f791-4c73-ab3e-f6b95a5afff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 84,
                "offset": 4,
                "shift": 43,
                "w": 38,
                "x": 351,
                "y": 260
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "1238400b-8bbe-4269-8735-0e966c181ae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 84,
                "offset": 1,
                "shift": 40,
                "w": 36,
                "x": 391,
                "y": 260
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "abf9d1ac-8c6b-4b69-a952-e3230de32ccc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 84,
                "offset": 1,
                "shift": 40,
                "w": 36,
                "x": 429,
                "y": 260
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "599fbff8-6145-41e4-8a31-53544e40e06c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 84,
                "offset": 1,
                "shift": 40,
                "w": 36,
                "x": 467,
                "y": 260
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "59d79eac-7e5f-4045-b0ac-641e28178203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 84,
                "offset": 1,
                "shift": 40,
                "w": 36,
                "x": 505,
                "y": 260
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "01383605-a2a6-4751-bca0-28627d0f54fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 84,
                "offset": 1,
                "shift": 40,
                "w": 36,
                "x": 543,
                "y": 260
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "4e6a0797-4054-416d-9b27-83ca7712e9cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 84,
                "offset": 1,
                "shift": 40,
                "w": 36,
                "x": 581,
                "y": 260
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "98819a32-ef14-4214-965a-d6f9973682b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 84,
                "offset": 1,
                "shift": 63,
                "w": 61,
                "x": 619,
                "y": 260
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "0d261c12-bf22-4db9-925a-feb418395102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 84,
                "offset": 1,
                "shift": 35,
                "w": 34,
                "x": 776,
                "y": 174
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "dd015f1c-d6ba-4d73-a81e-438d27ee0899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 84,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 227,
                "y": 260
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "8d207cde-449a-4157-a9b5-64ae06da8347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 84,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 736,
                "y": 174
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "76937f15-af20-4987-8e0b-5104a4274806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 84,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 214,
                "y": 174
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "0317e310-ce3f-414a-965c-18a04ed56da6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 84,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 935,
                "y": 88
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "a0f648ff-9d8d-4c90-a966-1f2ba4ed2f98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 84,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 975,
                "y": 88
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "6c9cd35e-1a12-4c02-a5a5-27c768d2956c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 84,
                "offset": 3,
                "shift": 20,
                "w": 18,
                "x": 995,
                "y": 88
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "500c8064-b41a-43b5-a474-9d409f2c5639",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 84,
                "offset": -2,
                "shift": 20,
                "w": 24,
                "x": 2,
                "y": 174
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "08775e9c-f0eb-407c-a383-d42aa72a2d5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 84,
                "offset": -2,
                "shift": 20,
                "w": 24,
                "x": 28,
                "y": 174
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "24eacdc8-e262-4c44-b2d8-9f9e3cfff582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 84,
                "offset": 1,
                "shift": 42,
                "w": 39,
                "x": 54,
                "y": 174
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "efe49194-58d4-4a9d-9657-d3bb79fe9741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 84,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 95,
                "y": 174
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "04d4c95b-4ca9-4250-9cc7-e6650d7611a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 84,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 132,
                "y": 174
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "43ddceb0-0379-4cf6-83a5-b73149bd148d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 84,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 173,
                "y": 174
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "d7ed8679-7a12-47ed-92af-5f63f8279286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 84,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 254,
                "y": 174
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "67fe188f-6cd2-43d5-a0d5-78e8e075d1ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 84,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 651,
                "y": 174
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "afe08fd2-16f7-46eb-8abe-668497de7e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 84,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 295,
                "y": 174
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "e06df7da-e96e-41a0-a305-6c5ed5cc1247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 84,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 336,
                "y": 174
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "36c4b9a0-934b-429f-ab55-ddffff8757c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 84,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 381,
                "y": 174
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "b79b27b3-851d-4f03-9ebd-7790b657cf01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 84,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 422,
                "y": 174
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "fa044610-978b-47ff-8cfa-105ee5595ece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 84,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 459,
                "y": 174
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "d2fc06bd-0c91-40cd-95f1-e4a53197d104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 84,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 496,
                "y": 174
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "487a61cf-22f7-4055-8e5e-7e4a99be93f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 84,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 533,
                "y": 174
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "1388aac1-7690-4588-a011-b5322c27fda4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 84,
                "offset": -1,
                "shift": 39,
                "w": 40,
                "x": 570,
                "y": 174
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "d18992f1-6eba-4442-a0fd-223be7fc7820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 84,
                "offset": 4,
                "shift": 42,
                "w": 37,
                "x": 612,
                "y": 174
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "0cafffa6-9d79-4394-9393-e1bd79fe2d62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 84,
                "offset": -1,
                "shift": 39,
                "w": 40,
                "x": 309,
                "y": 604
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "894c5b4a-8f01-4ce7-957a-9737bc4f02d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 84,
                "offset": 13,
                "shift": 65,
                "w": 39,
                "x": 351,
                "y": 604
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 50,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}