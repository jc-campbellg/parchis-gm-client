{
    "id": "1e61d92e-be7f-4887-a80d-7dcb559265b0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "ft_warning",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 3,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Tahoma",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ba508bdf-9040-4a77-ad28-32b4bbf45b95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 67,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f1286ead-49af-409f-93c9-7d180982fd07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 67,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 875,
                "y": 209
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a54396c3-8a7a-4ef1-9a6b-bb0ac68cf8c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 67,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 889,
                "y": 209
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d6a0a8ae-fccf-4ec1-b318-c28865266287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 67,
                "offset": 3,
                "shift": 43,
                "w": 38,
                "x": 911,
                "y": 209
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6fc381a4-c411-444a-ae9a-e9f3f50cc92a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 67,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 951,
                "y": 209
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "06712cb2-b994-41a9-b4bf-1064d3d8f763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 67,
                "offset": 2,
                "shift": 64,
                "w": 59,
                "x": 2,
                "y": 278
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "176cfa2e-8532-43f1-a73b-ca37432433ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 67,
                "offset": 2,
                "shift": 41,
                "w": 41,
                "x": 63,
                "y": 278
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a9a49ae1-fe30-4779-90af-463200d00b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 67,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 106,
                "y": 278
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "71e22894-53c5-415f-a250-a4f5369c44a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 67,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 117,
                "y": 278
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "35bc07cd-8f91-4ebd-9e6e-30c2e78516a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 67,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 139,
                "y": 278
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7e761274-7492-4674-b9f6-a9f26e8f77cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 27,
                "x": 193,
                "y": 278
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "988daa39-ed54-4a76-b809-2815344445f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 67,
                "offset": 5,
                "shift": 43,
                "w": 34,
                "x": 455,
                "y": 278
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d4c1b425-8489-456a-970a-4136cc8be1a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 67,
                "offset": -1,
                "shift": 17,
                "w": 16,
                "x": 222,
                "y": 278
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "85377aca-06b1-42d2-8ab3-855e1e23b8d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 67,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 240,
                "y": 278
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a1cd90e5-8cb3-4791-806f-12620e1cbf92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 67,
                "offset": 3,
                "shift": 17,
                "w": 10,
                "x": 260,
                "y": 278
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d66c284f-a303-4c3e-a568-3e7202c71b2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 67,
                "offset": 3,
                "shift": 31,
                "w": 23,
                "x": 272,
                "y": 278
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a0bddee5-e6cd-4caf-afd5-b1a1719ee49f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 67,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 297,
                "y": 278
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e5d57be1-85ca-46ea-8f31-1cfd8d19f979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 67,
                "offset": 5,
                "shift": 34,
                "w": 26,
                "x": 329,
                "y": 278
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "751c91e9-2273-4ace-8d5c-3c9d0ea7ca28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 30,
                "x": 357,
                "y": 278
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "873b2584-ce9f-4e74-8779-8acd8f35d386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 67,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 389,
                "y": 278
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "bcae5469-dddf-4cd2-85bf-5935c423e026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 67,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 421,
                "y": 278
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "48dbadc3-57d6-471c-aa91-136df79c721a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 29,
                "x": 844,
                "y": 209
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c0c757e5-3551-4333-a85e-10cb9d44a8bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 67,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 161,
                "y": 278
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "36d1c05f-f35f-438d-87a1-0ff9dea0a9dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 29,
                "x": 813,
                "y": 209
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "29ccaf2f-3452-468c-90b1-6f937d8d3ce6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 67,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 380,
                "y": 209
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f30be0fe-b996-4544-96f7-4b089ad22c03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 67,
                "offset": 1,
                "shift": 34,
                "w": 31,
                "x": 97,
                "y": 209
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1031f05f-1d4e-4024-948f-880a9da7ddfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 67,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 130,
                "y": 209
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9705a319-0293-4e76-a876-9fa7268c132b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 67,
                "offset": 1,
                "shift": 19,
                "w": 15,
                "x": 142,
                "y": 209
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2ee5915c-f9f3-4a27-aa35-ee6287e02e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 67,
                "offset": 4,
                "shift": 43,
                "w": 34,
                "x": 159,
                "y": 209
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c0fa0990-70d5-4dd9-a206-d43e457a933e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 67,
                "offset": 5,
                "shift": 43,
                "w": 33,
                "x": 195,
                "y": 209
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ad25aee3-d8d3-4c96-84a0-706e77ce05b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 67,
                "offset": 5,
                "shift": 43,
                "w": 34,
                "x": 230,
                "y": 209
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "0e2b3ab2-6444-4a89-a783-aa92f33804ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 67,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 266,
                "y": 209
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "501719a5-4255-4534-9283-fd037140d628",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 67,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 295,
                "y": 209
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e379c333-6c7b-4baa-beee-197ce64cc31c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 67,
                "offset": -1,
                "shift": 36,
                "w": 38,
                "x": 340,
                "y": 209
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "14093d1e-72ae-4e84-9c40-faff5799329c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 67,
                "offset": 3,
                "shift": 36,
                "w": 32,
                "x": 414,
                "y": 209
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d46188ad-9465-4244-a31b-d9022d68c4ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 67,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 735,
                "y": 209
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3d11fd25-7db7-42dd-9677-543d47cfe281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 67,
                "offset": 3,
                "shift": 40,
                "w": 36,
                "x": 448,
                "y": 209
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "93db330f-1695-4c85-bad1-48fffb7970de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 67,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 486,
                "y": 209
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5f569e14-ac0e-456f-84be-c274281274ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 67,
                "offset": 3,
                "shift": 31,
                "w": 27,
                "x": 516,
                "y": 209
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c12dfb49-a528-415c-a330-8d2507ef41e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 67,
                "offset": 1,
                "shift": 39,
                "w": 36,
                "x": 545,
                "y": 209
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "72f40d38-a681-49ec-bc25-ace9c61fd02e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 67,
                "offset": 3,
                "shift": 41,
                "w": 34,
                "x": 583,
                "y": 209
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5b305d0d-9b9b-428b-acc4-22a93f5eba33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 67,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 619,
                "y": 209
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "95589ee4-0275-4ffb-9093-c1d79f0ccc04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 67,
                "offset": 0,
                "shift": 27,
                "w": 24,
                "x": 643,
                "y": 209
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "20108e35-d97e-4084-bc48-cd20888fdbad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 67,
                "offset": 3,
                "shift": 37,
                "w": 35,
                "x": 669,
                "y": 209
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "48e62e35-ee3d-4294-988e-55aadb09974a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 67,
                "offset": 3,
                "shift": 30,
                "w": 27,
                "x": 706,
                "y": 209
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "143de452-2b38-4a85-8701-438d4062df44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 67,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 770,
                "y": 209
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ac04f616-41a1-4eea-9d4b-052940c2bdc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 67,
                "offset": 3,
                "shift": 41,
                "w": 34,
                "x": 491,
                "y": 278
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e87e79a0-89c2-495d-8132-2683827583ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 67,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 527,
                "y": 278
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0d9093eb-652c-43a9-a192-5ca9d6430cb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 67,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 568,
                "y": 278
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3e49a8ea-963b-432d-aaf7-227537093b8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 67,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 282,
                "y": 347
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e8ad1fb6-09fe-4153-91ea-1fe4edb6c297",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 67,
                "offset": 3,
                "shift": 38,
                "w": 37,
                "x": 323,
                "y": 347
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "04b01f59-8098-4549-a6b4-403069664b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 67,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 362,
                "y": 347
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "886cd9de-346e-41e1-87b8-85d649f00464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 67,
                "offset": 0,
                "shift": 32,
                "w": 33,
                "x": 396,
                "y": 347
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "34444e3e-d6ad-4beb-b256-cd7a1d647502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 67,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 431,
                "y": 347
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b385dedf-92b2-423e-b3b9-4a9ee2425ed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 67,
                "offset": -1,
                "shift": 36,
                "w": 38,
                "x": 466,
                "y": 347
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7c5a0723-0ce5-487a-bc29-94f9c3e76937",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 67,
                "offset": 0,
                "shift": 54,
                "w": 54,
                "x": 506,
                "y": 347
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a87ba3f5-3122-4cd8-a698-7d959da24c60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 67,
                "offset": -1,
                "shift": 36,
                "w": 38,
                "x": 562,
                "y": 347
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "494aba1c-7a19-451e-a735-52914448041a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 67,
                "offset": -1,
                "shift": 36,
                "w": 38,
                "x": 602,
                "y": 347
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9f5002cd-f8be-42cd-8b3c-bca36713b34d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 642,
                "y": 347
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "adc6e05d-43b5-4d75-aa49-65104f7ccc27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 67,
                "offset": 4,
                "shift": 24,
                "w": 17,
                "x": 675,
                "y": 347
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fef2d1e4-7529-42a4-83c2-532d87c937a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 67,
                "offset": 5,
                "shift": 31,
                "w": 23,
                "x": 694,
                "y": 347
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1178df94-e01d-4d4b-81f1-d36a08283413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 67,
                "offset": 3,
                "shift": 24,
                "w": 17,
                "x": 719,
                "y": 347
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "44ebfbc0-cd4e-4225-8170-8c36a121c223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 67,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 738,
                "y": 347
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c4792204-bd45-4a29-ae4b-c48abf8da34f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 67,
                "offset": -1,
                "shift": 34,
                "w": 36,
                "x": 775,
                "y": 347
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3c58d3d8-55e3-4a18-a957-d83cf9a00e9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 67,
                "offset": 6,
                "shift": 29,
                "w": 15,
                "x": 813,
                "y": 347
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "49507bdb-6676-4b88-a9eb-efc5c317feba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 67,
                "offset": 1,
                "shift": 32,
                "w": 28,
                "x": 830,
                "y": 347
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6693d0e9-8ba1-4bbc-aefe-1d27490143ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 67,
                "offset": 3,
                "shift": 33,
                "w": 29,
                "x": 860,
                "y": 347
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e3b358ac-0dd0-4b52-9262-0097e27adc17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 67,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 891,
                "y": 347
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "63c6eab7-a00d-4532-b010-4c94831b0dc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 29,
                "x": 920,
                "y": 347
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a3b44e22-3713-456b-a069-9d507befb9b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 67,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 951,
                "y": 347
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5293266d-f202-453a-963d-da5d1dc0fc5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 67,
                "offset": 0,
                "shift": 20,
                "w": 23,
                "x": 257,
                "y": 347
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3fb19882-d0a8-487c-b27e-3e03d41b2148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 29,
                "x": 226,
                "y": 347
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "54a475d5-4d50-44c7-8140-71130824b783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 196,
                "y": 347
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "dd622edd-5e8a-4470-8f51-96b9208e3e68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 67,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 860,
                "y": 278
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bd00529c-17a4-4c89-b6cb-4cc2485935ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 67,
                "offset": -3,
                "shift": 19,
                "w": 19,
                "x": 601,
                "y": 278
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "bd021dda-623c-45a8-aa15-53d3465344b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 67,
                "offset": 3,
                "shift": 32,
                "w": 30,
                "x": 622,
                "y": 278
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b439f575-60c7-44a3-85b5-e3ca64e78031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 67,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 654,
                "y": 278
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "cdc0adfb-0573-4402-ab78-cfc57d0c3225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 67,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 666,
                "y": 278
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5bee86cc-ef71-4969-b79e-a6ad6e4f345f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 713,
                "y": 278
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "315ddebf-73d2-4ddf-bcf2-eb418a7b1827",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 743,
                "y": 278
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "edf76c43-1adf-4af2-9e14-36c7876f289c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 67,
                "offset": 3,
                "shift": 33,
                "w": 29,
                "x": 776,
                "y": 278
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "902bcda4-d282-4eed-a604-48c92227a13b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 29,
                "x": 807,
                "y": 278
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b36c0ca5-2621-4434-ba77-c3b422d00825",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 67,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 838,
                "y": 278
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3571c257-4f7d-492d-9246-b0c9fbc91b5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 67,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 872,
                "y": 278
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d3348258-bdfd-4f46-aa7c-046a7eb9dd3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 67,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 172,
                "y": 347
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a193f29e-206c-419e-b452-b37ce2a817d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 900,
                "y": 278
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "410b50e0-ec91-49d0-9e72-7a69061ad73b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 67,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 930,
                "y": 278
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4efb386d-9380-44ed-ac29-4c3992aec81a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 67,
                "offset": 0,
                "shift": 47,
                "w": 47,
                "x": 963,
                "y": 278
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6e620c44-5c88-4f5f-8438-a04b50975465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 67,
                "offset": -1,
                "shift": 32,
                "w": 34,
                "x": 2,
                "y": 347
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "66ebf818-6ab4-4e3e-853a-2e95a741a92e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 67,
                "offset": -1,
                "shift": 31,
                "w": 32,
                "x": 38,
                "y": 347
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "06603dbe-10ec-49b5-9ac6-91223770422a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 67,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 72,
                "y": 347
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "aa1bbacb-ebe5-4968-adca-5aa50f7670c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 29,
                "x": 100,
                "y": 347
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d25defc2-b60b-4b9a-a831-f156696b5f84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 67,
                "offset": 13,
                "shift": 34,
                "w": 8,
                "x": 131,
                "y": 347
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "35d345bf-3d9f-4fa1-94e6-9825d177f86a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 67,
                "offset": 3,
                "shift": 33,
                "w": 29,
                "x": 141,
                "y": 347
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d089cab2-c355-488d-85cb-0ac5a6f36d68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 67,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 58,
                "y": 209
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "77463610-9e51-4e49-961d-eff7d45dbcfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 67,
                "offset": 0,
                "shift": 16,
                "w": 0,
                "x": 56,
                "y": 209
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "b5c6bd30-bf00-459d-8f0b-9960ee358a26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 67,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 42,
                "y": 209
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "17081086-d6f6-4cdc-ba56-2ed1adcdcfd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 27,
                "x": 451,
                "y": 71
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "24d7dbe3-0159-431f-acad-62dd6ca13447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 29,
                "x": 856,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "17fd43e2-bfb0-4511-b115-873f13bd3c12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 67,
                "offset": 2,
                "shift": 34,
                "w": 29,
                "x": 887,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "267e60f9-97c0-422b-a854-8f54fa74cf51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 67,
                "offset": 0,
                "shift": 34,
                "w": 34,
                "x": 918,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "01cc679d-4260-4e05-a5d9-391ffc0e2e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 67,
                "offset": 13,
                "shift": 34,
                "w": 8,
                "x": 954,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "98041555-2413-4ff6-b874-7d5dc9099b69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 67,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 964,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "cab2670c-2206-43bd-9505-58af069840b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 67,
                "offset": 4,
                "shift": 29,
                "w": 21,
                "x": 996,
                "y": 2
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "8f0011b2-c7a7-4f06-a47c-f993cdb428c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 67,
                "offset": 2,
                "shift": 49,
                "w": 45,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "41ea4d34-d968-40b3-8744-c9ec9525ba5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 67,
                "offset": 1,
                "shift": 27,
                "w": 23,
                "x": 49,
                "y": 71
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "183becef-7cc6-4bd5-98fc-9b8e897798d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 67,
                "offset": 3,
                "shift": 37,
                "w": 30,
                "x": 74,
                "y": 71
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "fbe59045-210d-4e53-a00d-dd00cc70da53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 67,
                "offset": 5,
                "shift": 43,
                "w": 34,
                "x": 123,
                "y": 71
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "21bab724-5a67-4f9a-81e8-92c056a23136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 67,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 431,
                "y": 71
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "39d08499-cd66-4e9a-a4d8-9c82a5f4a609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 67,
                "offset": 2,
                "shift": 49,
                "w": 45,
                "x": 159,
                "y": 71
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "939d962e-087c-4d7f-8689-f37818157eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 67,
                "offset": -1,
                "shift": 34,
                "w": 36,
                "x": 206,
                "y": 71
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "daf0dc43-5563-498a-b465-8a8cde172379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 67,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 244,
                "y": 71
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "bf4a71ae-a11d-45dd-8271-bd2542c4f185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 67,
                "offset": 5,
                "shift": 43,
                "w": 34,
                "x": 268,
                "y": 71
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "0c0d1365-f4f2-439f-8bea-5041048f3f5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 67,
                "offset": 4,
                "shift": 29,
                "w": 22,
                "x": 304,
                "y": 71
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "f2a13382-b593-4b0b-aba0-327a8b1feeb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 67,
                "offset": 4,
                "shift": 29,
                "w": 21,
                "x": 328,
                "y": 71
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "0e75e265-85ea-4bdd-ae5f-c2f0cde70284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 67,
                "offset": 8,
                "shift": 29,
                "w": 15,
                "x": 351,
                "y": 71
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "ddb0eaa9-1c2a-43dd-ae11-ab1e3c141cee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 67,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 368,
                "y": 71
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "e7410fd0-ccf9-4913-b929-5cfffc5c70e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 67,
                "offset": 1,
                "shift": 34,
                "w": 30,
                "x": 399,
                "y": 71
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "7481427f-ac14-4c6f-9516-1602e6238dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 67,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 844,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "ea038fd2-e051-4b7e-9c91-5234799c9ba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 67,
                "offset": 7,
                "shift": 29,
                "w": 15,
                "x": 106,
                "y": 71
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "8f7c9e7d-4365-4900-ba77-ba8ab8ea6cd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 67,
                "offset": 5,
                "shift": 29,
                "w": 19,
                "x": 823,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "98b81dd8-3041-42e9-a3d6-770e30bb1bdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 67,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "f78ff340-02e1-42ba-bd5d-63a16f1add40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 67,
                "offset": 4,
                "shift": 37,
                "w": 30,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "73cb87a0-6fd0-4ce7-a4ab-275d0bd3c564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 67,
                "offset": 4,
                "shift": 60,
                "w": 53,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "8e9e44d5-9095-409e-85f4-7c741ec27a7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 67,
                "offset": 3,
                "shift": 60,
                "w": 55,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "863e3aba-7a4a-4f2c-8e79-d49636635628",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 67,
                "offset": 3,
                "shift": 60,
                "w": 56,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "9664d59f-877d-44e0-a3d1-633b3bd3843e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 67,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "fe55cad8-22fc-44ed-9a17-123b06171178",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 67,
                "offset": -1,
                "shift": 36,
                "w": 38,
                "x": 251,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "f0ab2272-491b-4814-a434-fedeb5b5f59c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 67,
                "offset": -1,
                "shift": 36,
                "w": 38,
                "x": 291,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "a87a2f95-f537-4f33-9e8b-5bab248ef0d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 67,
                "offset": -1,
                "shift": 36,
                "w": 38,
                "x": 331,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "070bad89-47b8-4825-8df6-c75921d917bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 67,
                "offset": -1,
                "shift": 36,
                "w": 38,
                "x": 371,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "a1062cd9-3194-42f7-9829-23cf799afff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 67,
                "offset": -1,
                "shift": 36,
                "w": 38,
                "x": 438,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "72e48961-b8ba-4af0-8b37-af2d319bf54b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 67,
                "offset": -1,
                "shift": 36,
                "w": 38,
                "x": 759,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "72028f83-ae26-4cb8-b79d-d4446bf74e91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 67,
                "offset": -1,
                "shift": 52,
                "w": 51,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "6a2b9367-72f5-4da5-ae02-3708cfbdf70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 67,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 531,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "f98540eb-5256-4c60-8244-22a9ff235bb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 67,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 566,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "c58826ca-c060-4f47-8e7e-be1b812c0faa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 67,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 596,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "e71d8612-721b-4c8f-8eb5-dbb23f8977e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 67,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 626,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "bbcb5f76-e51b-410c-a046-eb7428d6314e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 67,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 656,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "32c2b80a-feb1-426e-ac7e-b639e17681e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 67,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 686,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "83921e2e-357f-48cc-b5dd-bd88851f03ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 67,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 710,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "f4ab0bea-438d-4b2a-9fb0-1f1cf56458bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 67,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 734,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "63e16b7c-cebf-404d-a1c1-8c47228e4907",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 67,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 799,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "3abda882-d2df-4f15-90d8-62503dacd3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 67,
                "offset": 0,
                "shift": 41,
                "w": 40,
                "x": 480,
                "y": 71
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "215826dd-d005-4594-8200-08a46a323d09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 67,
                "offset": 3,
                "shift": 41,
                "w": 34,
                "x": 190,
                "y": 140
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "40f66466-1c68-45fb-8644-cd90291c251c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 67,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 522,
                "y": 71
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "51e799a6-90fb-4115-8e0d-164fadc8a3c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 67,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 287,
                "y": 140
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "3ff531e3-038c-4414-b6b0-328922067ed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 67,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 328,
                "y": 140
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "3efae62e-c6da-484d-86f9-48a3960dfc35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 67,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 369,
                "y": 140
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "fd4c4921-e41c-4a82-9f86-17223000e99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 67,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 410,
                "y": 140
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "6bdf60f4-bc29-40b5-b662-e3e60ffe204e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 67,
                "offset": 6,
                "shift": 43,
                "w": 32,
                "x": 451,
                "y": 140
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "fbb63e9b-e9a9-4fef-b158-a75499b1ecb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 67,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 485,
                "y": 140
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "59824b4d-c1b4-4eb3-9bbe-b820100291c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 67,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 526,
                "y": 140
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "b45d8d4b-b656-4258-a77e-44565ce5e288",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 67,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 561,
                "y": 140
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "3a1863a9-f1f7-49c6-b6a0-860274a048ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 67,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 596,
                "y": 140
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "f75d5b8d-8ed6-43a3-90a3-850ed7d65bc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 67,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 663,
                "y": 140
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "7d514601-db31-448a-8e5c-7697d8487065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 67,
                "offset": -1,
                "shift": 36,
                "w": 38,
                "x": 2,
                "y": 209
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "7f18283a-0678-453d-a992-1818aa6dbb8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 67,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 698,
                "y": 140
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "61cd5d30-bb1d-4fb5-97b6-df5b39ba33f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 30,
                "x": 731,
                "y": 140
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "f9d32de0-b13e-46ba-aade-6b55fd0b7c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 67,
                "offset": 1,
                "shift": 32,
                "w": 28,
                "x": 763,
                "y": 140
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "8257337a-2b55-479f-9f09-5a8d39b7b832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 67,
                "offset": 1,
                "shift": 32,
                "w": 28,
                "x": 793,
                "y": 140
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "690e609c-20e4-4404-a20c-8b3322f4165e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 67,
                "offset": 1,
                "shift": 32,
                "w": 28,
                "x": 823,
                "y": 140
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "ae8ba502-4a3a-49d2-bfea-e8e2175bb713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 67,
                "offset": 1,
                "shift": 32,
                "w": 28,
                "x": 853,
                "y": 140
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "db978b3d-5dfe-4067-a7e8-df49c58747f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 67,
                "offset": 1,
                "shift": 32,
                "w": 28,
                "x": 883,
                "y": 140
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "2851aace-8322-4826-977c-35d0f81fbb06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 67,
                "offset": 1,
                "shift": 32,
                "w": 28,
                "x": 913,
                "y": 140
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "50078054-f26d-4b97-bb4b-5689d1433848",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 67,
                "offset": 1,
                "shift": 50,
                "w": 48,
                "x": 943,
                "y": 140
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "86f61c48-05b9-4a6c-9161-949a74fcdf0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 67,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 258,
                "y": 140
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "13d00939-3966-42f2-9280-efdb70edcdc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 67,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 631,
                "y": 140
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "5101ab68-13c8-495b-93b4-41b06ae98738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 67,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 226,
                "y": 140
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "5c81c3e7-9a7d-48ff-abac-4fdf5bf1b5e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 67,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 800,
                "y": 71
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "24efbc51-4c49-44b8-98b9-583f2ae7a845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 67,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 563,
                "y": 71
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "43156b21-54e2-4196-91bf-c111b7328100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 67,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 595,
                "y": 71
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "ec698b75-8af3-4b5a-b35a-8c1f2f10bdbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 67,
                "offset": 2,
                "shift": 16,
                "w": 15,
                "x": 611,
                "y": 71
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "ec3d0e6a-6bf2-473c-9407-d0b2603e72d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 67,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 628,
                "y": 71
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "b6e0ce92-5cd0-4b06-909e-89e6ff6a73b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 67,
                "offset": -1,
                "shift": 16,
                "w": 19,
                "x": 650,
                "y": 71
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "6001355a-30c7-4411-a97f-cb7ffbabd74f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 671,
                "y": 71
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "b938d5d5-7f6d-4fa1-8483-3d564152c69a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 704,
                "y": 71
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "9b1a0dc3-3182-4753-b7df-52ac2e2781a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 734,
                "y": 71
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "d930e201-f299-4476-a237-e8976a22a6d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 767,
                "y": 71
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "bf412f03-476e-4ff0-8de7-ea63bce1cf19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 832,
                "y": 71
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "0296a555-41e7-4e98-b350-848e1c8fc526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 157,
                "y": 140
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "0b21d217-3dd3-411c-9f1f-021843f38558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 865,
                "y": 71
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "6757fc33-352f-4429-a36b-fdcc88ff42e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 67,
                "offset": 5,
                "shift": 43,
                "w": 34,
                "x": 898,
                "y": 71
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "cf2f26bd-398b-4bb9-ac75-b680adb15d2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 67,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 934,
                "y": 71
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "11ea8a14-bf5f-4cd5-93b2-18af452346fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 967,
                "y": 71
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "08c8b2cc-82a9-418a-857c-ef689bcc5853",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 2,
                "y": 140
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "b6923ddc-9b27-4451-8ef9-55bcb09132cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 32,
                "y": 140
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "7599a740-421a-4f2b-ac60-847695733cff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 67,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 62,
                "y": 140
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "f562b2de-e5b0-4e81-af9e-4483c43120f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 67,
                "offset": -1,
                "shift": 31,
                "w": 32,
                "x": 92,
                "y": 140
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "6285db4f-58f8-4fdc-bf10-032b6125407e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 67,
                "offset": 3,
                "shift": 33,
                "w": 29,
                "x": 126,
                "y": 140
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "7efa67b2-32de-4a32-887b-1f929595008d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 67,
                "offset": -1,
                "shift": 31,
                "w": 32,
                "x": 983,
                "y": 347
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "a151b597-cb56-4e02-8283-c37ba8b2766f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 67,
                "offset": 10,
                "shift": 51,
                "w": 31,
                "x": 2,
                "y": 416
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 40,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}