var color = argument0;
var client = o_client.client;
var clientBuffer = o_client.clientBuffer;

buffer_seek(clientBuffer, buffer_seek_start, 0);
buffer_write(clientBuffer, buffer_u8, net.join);
buffer_write(clientBuffer, buffer_string, playerName);
buffer_write(clientBuffer, buffer_u8, color);
network_send_packet(client, clientBuffer, buffer_tell(clientBuffer));