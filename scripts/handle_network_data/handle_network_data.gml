var buffer = argument0;
var msgId = buffer_read(buffer, buffer_u8);

switch(msgId) {
	case net.syncNames:
		o_playerRed.available = buffer_read(buffer, buffer_u8);
		o_playerRed.name = buffer_read(buffer, buffer_string);
		o_playerRed.ready = buffer_read(buffer, buffer_bool);
		o_playerBlue.available = buffer_read(buffer, buffer_u8);
		o_playerBlue.name = buffer_read(buffer, buffer_string);
		o_playerBlue.ready = buffer_read(buffer, buffer_bool);
		o_playerYellow.available = buffer_read(buffer, buffer_u8);
		o_playerYellow.name = buffer_read(buffer, buffer_string);
		o_playerYellow.ready = buffer_read(buffer, buffer_bool);
		o_playerGreen.available = buffer_read(buffer, buffer_u8);
		o_playerGreen.name = buffer_read(buffer, buffer_string);
		o_playerGreen.ready = buffer_read(buffer, buffer_bool);
		o_playerOrange.available = buffer_read(buffer, buffer_u8);
		o_playerOrange.name = buffer_read(buffer, buffer_string);
		o_playerOrange.ready = buffer_read(buffer, buffer_bool);
		o_playerPurple.available = buffer_read(buffer, buffer_u8);
		o_playerPurple.name = buffer_read(buffer, buffer_string);
		o_playerPurple.ready = buffer_read(buffer, buffer_bool);
		break;
	
	case net.join:
	case net.newPlayer:
		var name = buffer_read(buffer, buffer_string);
		var color = buffer_read(buffer, buffer_u8);
		playerInstances[color].available = false;
		playerInstances[color].used = true;
		playerInstances[color].name = name;
		show_ui_message("Nuevo Jugador:", showPlayerName.atEnd, color);
		if (msgId == net.join) {
			playerInstances[color].isYou = true;
			room_goto_next();
		} else {
			playerInstances[color].isYou = false;
			if (room == rm_play) {
				send_syncPosition();
			}
		}
		break;
		
	case net.tryAgainColor:
		show_message("Color en uso, seleciona otro.");
		break;
		
	case net.jumpToPosition:
		var num = buffer_read(buffer, buffer_u8);
		var xx = buffer_read(buffer, buffer_u16);
		var yy = buffer_read(buffer, buffer_u16);
		
		var inst = instance_find(o_piece, num);
		with (inst) {
			x = xx;
			y = yy;
		};
		break;
		
	case net.moveToPosition:
		var movePathTemp = path_add();
		var num = buffer_read(buffer, buffer_u8);
		var steps = buffer_read(buffer, buffer_u8);
		repeat(steps) {
			var px = buffer_read(buffer, buffer_u16);
			var py = buffer_read(buffer, buffer_u16);
			path_add_point(movePathTemp, px, py, o_gameState.moveSpeed);
		}
		path_set_closed(movePathTemp, false);
		
		with (instance_find(o_piece, num)) {
			movePath = movePathTemp;
			path_start(movePath, 15, path_action_stop, true);
		}
		break;
		
	case net.removePlayer:
		var color = buffer_read(buffer, buffer_u8);
		playerInstances[color].available = true;
		playerInstances[color].used = false;
		playerInstances[color].ready = false;
		playerInstances[color].name = "";
		break;
		
	case net.syncSettings:
		with (o_gameState) {
			moveSpeed = buffer_read(buffer, buffer_u8);
			eatReward = buffer_read(buffer, buffer_u8);
			goalReward = buffer_read(buffer, buffer_u8);
			uTurn = buffer_read(buffer, buffer_bool);
			teamPlay = buffer_read(buffer, buffer_bool);
			start = buffer_read(buffer, buffer_bool);
			if (start) {
				show_ui_message("El juego ha comenzado", showPlayerName.dontShow, 0);
			}
		}
		break;
	
	case net.ready:
		color = buffer_read(buffer, buffer_u8);
		playerInstances[color].ready = true;
		break;
		
	case net.gameStart:
		with (o_gameState) {
			start = true;
		}
		show_ui_message("El juego ha comenzado", showPlayerName.dontShow, 0);
		break;
}