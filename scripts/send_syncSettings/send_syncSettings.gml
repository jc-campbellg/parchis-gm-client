var client = o_client.client;
var clientBuffer = o_client.clientBuffer;
var s = o_gameState;

buffer_seek(clientBuffer, buffer_seek_start, 0);
buffer_write(clientBuffer, buffer_u8, net.syncSettings);
buffer_write(clientBuffer, buffer_u8, s.moveSpeed);
buffer_write(clientBuffer, buffer_u8, s.eatReward);
buffer_write(clientBuffer, buffer_u8, s.goalReward);
buffer_write(clientBuffer, buffer_bool, s.uTurn);
buffer_write(clientBuffer, buffer_bool, s.teamPlay);
network_send_packet(client, clientBuffer, buffer_tell(clientBuffer));