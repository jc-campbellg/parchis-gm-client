/// @description Show message
/// @param {string} Message
/// @param {integer} showPlayerName
/// @param {string} Color

var inst = instance_create_depth(1066/2, 600/2, -1000, o_warning);
with (inst) {
	message = argument0;
	showPlayer = argument1;
	name = playerInstances[argument2].name;
	color = playerInstances[argument2].buttonColor;
	colorRGB = playerInstances[argument2].colorRGB;
}