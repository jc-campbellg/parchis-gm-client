{
    "id": "764a106f-99c3-4d55-b99f-6b3d7e82d938",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pieceGreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f634fc2-a9e1-4224-8237-555c2a847b2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764a106f-99c3-4d55-b99f-6b3d7e82d938",
            "compositeImage": {
                "id": "6785ae81-56e6-46da-b4b3-e9f29d9430f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f634fc2-a9e1-4224-8237-555c2a847b2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11cced19-a95b-4bf4-a330-b1baa9eb2b33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f634fc2-a9e1-4224-8237-555c2a847b2d",
                    "LayerId": "ecc79d88-8f63-43de-8194-1241295ef672"
                }
            ]
        },
        {
            "id": "2d374777-28e9-40c3-b3a0-f188e36ca84c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764a106f-99c3-4d55-b99f-6b3d7e82d938",
            "compositeImage": {
                "id": "9b814424-3175-4726-943d-000ff96dfae7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d374777-28e9-40c3-b3a0-f188e36ca84c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf978677-f958-45d0-bff1-feb1b38e7e09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d374777-28e9-40c3-b3a0-f188e36ca84c",
                    "LayerId": "ecc79d88-8f63-43de-8194-1241295ef672"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "ecc79d88-8f63-43de-8194-1241295ef672",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "764a106f-99c3-4d55-b99f-6b3d7e82d938",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 26
}