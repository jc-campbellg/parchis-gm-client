{
    "id": "fc65fd45-d4df-4757-afc5-67e052e8a047",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pieceBlue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdb385e1-a68c-47ce-8eeb-f96a54060a1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc65fd45-d4df-4757-afc5-67e052e8a047",
            "compositeImage": {
                "id": "09ddbf67-1a2b-49f9-81ef-bcf34a1793e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdb385e1-a68c-47ce-8eeb-f96a54060a1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f1152cb-9c8e-4149-85cf-8d7d891df1a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdb385e1-a68c-47ce-8eeb-f96a54060a1a",
                    "LayerId": "4e401708-ce7a-4a88-b584-7658d1fcf3c4"
                }
            ]
        },
        {
            "id": "c92ef366-a1d1-4c3f-8167-d32ae692f3b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc65fd45-d4df-4757-afc5-67e052e8a047",
            "compositeImage": {
                "id": "4146f4f6-f1bf-461b-b118-825c360eba16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c92ef366-a1d1-4c3f-8167-d32ae692f3b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8c0cf25-6c40-4c98-aafd-8f9e05f1d53c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c92ef366-a1d1-4c3f-8167-d32ae692f3b0",
                    "LayerId": "4e401708-ce7a-4a88-b584-7658d1fcf3c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4e401708-ce7a-4a88-b584-7658d1fcf3c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc65fd45-d4df-4757-afc5-67e052e8a047",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 26
}