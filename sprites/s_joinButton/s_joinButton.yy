{
    "id": "f956c7de-d6ac-45a9-918f-8c499d6ffef3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_joinButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 0,
    "bbox_right": 291,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "537ff7ea-6057-499a-9522-1d262d2a1ec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f956c7de-d6ac-45a9-918f-8c499d6ffef3",
            "compositeImage": {
                "id": "a55a8104-0ba3-46dd-90db-be8143d654f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "537ff7ea-6057-499a-9522-1d262d2a1ec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a3f2602-ffea-42d0-9e90-fb2a30a75a07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "537ff7ea-6057-499a-9522-1d262d2a1ec1",
                    "LayerId": "87c67573-2852-4931-a593-ec8b40fc9506"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "87c67573-2852-4931-a593-ec8b40fc9506",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f956c7de-d6ac-45a9-918f-8c499d6ffef3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 292,
    "xorig": 0,
    "yorig": 0
}