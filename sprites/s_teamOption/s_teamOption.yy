{
    "id": "48b6aa04-6a5e-4320-b8d2-df69a953cbb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_teamOption",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 54,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f357aac1-bcbc-42be-828b-5a7cd3cc2ea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48b6aa04-6a5e-4320-b8d2-df69a953cbb6",
            "compositeImage": {
                "id": "5116b999-0f5c-4250-b886-b5abf1b4ac2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f357aac1-bcbc-42be-828b-5a7cd3cc2ea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d9d6d85-5c71-4999-abfa-5b66e9256d85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f357aac1-bcbc-42be-828b-5a7cd3cc2ea0",
                    "LayerId": "a6147cfb-f046-41c8-bc33-229b116e0766"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "a6147cfb-f046-41c8-bc33-229b116e0766",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48b6aa04-6a5e-4320-b8d2-df69a953cbb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}