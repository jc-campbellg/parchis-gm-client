{
    "id": "a6e42031-a218-48f4-aeb0-b3ce9e72540f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pieceOrange",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec2c0179-b471-4b5c-8c88-30ab0969e4c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6e42031-a218-48f4-aeb0-b3ce9e72540f",
            "compositeImage": {
                "id": "196a7bff-23f2-4519-9160-3aca7c3c367e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec2c0179-b471-4b5c-8c88-30ab0969e4c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "310568c0-5cc5-4b8d-8c08-85bcd3e13885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec2c0179-b471-4b5c-8c88-30ab0969e4c3",
                    "LayerId": "3599b651-3687-45f5-a498-f031f683ab05"
                }
            ]
        },
        {
            "id": "133e4929-1ea5-4b03-8191-91e368a4180b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6e42031-a218-48f4-aeb0-b3ce9e72540f",
            "compositeImage": {
                "id": "e8c7e44b-1562-4d4e-be43-94d49b10da5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "133e4929-1ea5-4b03-8191-91e368a4180b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29c2162b-07d0-4e2b-b565-8ed73eff0a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "133e4929-1ea5-4b03-8191-91e368a4180b",
                    "LayerId": "3599b651-3687-45f5-a498-f031f683ab05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "3599b651-3687-45f5-a498-f031f683ab05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6e42031-a218-48f4-aeb0-b3ce9e72540f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 9,
    "yorig": 26
}