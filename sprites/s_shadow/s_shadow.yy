{
    "id": "7a4d2a49-9b9c-4308-955b-5332e6bcc503",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06f4e722-a24a-4fc9-900b-0478e15e75d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a4d2a49-9b9c-4308-955b-5332e6bcc503",
            "compositeImage": {
                "id": "d8da05ac-6dc8-4b0e-8c03-14275bbeda7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06f4e722-a24a-4fc9-900b-0478e15e75d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc4f70de-a148-4ec8-86e4-c16ccf91ad58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06f4e722-a24a-4fc9-900b-0478e15e75d4",
                    "LayerId": "fb5567f6-eb63-44f9-8a7d-188298255d75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "fb5567f6-eb63-44f9-8a7d-188298255d75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a4d2a49-9b9c-4308-955b-5332e6bcc503",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 6
}