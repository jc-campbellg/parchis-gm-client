{
    "id": "94b802c7-52c4-4ca9-8fc5-1ece1d65298d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_blueSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8772a2ad-a2e1-4a57-87cb-c2df31cad14a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94b802c7-52c4-4ca9-8fc5-1ece1d65298d",
            "compositeImage": {
                "id": "29717592-4b40-46bb-8cef-40bbf31e18ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8772a2ad-a2e1-4a57-87cb-c2df31cad14a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d63e030f-cfed-4af9-9ac9-7bf6199438d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8772a2ad-a2e1-4a57-87cb-c2df31cad14a",
                    "LayerId": "3c841b43-8ea0-4fe4-9ffb-88869552f332"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3c841b43-8ea0-4fe4-9ffb-88869552f332",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94b802c7-52c4-4ca9-8fc5-1ece1d65298d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}