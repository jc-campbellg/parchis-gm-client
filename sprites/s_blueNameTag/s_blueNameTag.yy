{
    "id": "0997a1b2-dbb4-46e0-bdf9-a11c5ecb3833",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_blueNameTag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f28e714f-595d-40ef-86a5-a94bf252670e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0997a1b2-dbb4-46e0-bdf9-a11c5ecb3833",
            "compositeImage": {
                "id": "e3ecd19f-64f3-4103-8ab4-c5aaf79309a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f28e714f-595d-40ef-86a5-a94bf252670e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e683034-0981-4d01-9651-8d3e80ef89fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f28e714f-595d-40ef-86a5-a94bf252670e",
                    "LayerId": "36a576da-6b08-4b2c-b5d0-4cd29139a27f"
                }
            ]
        },
        {
            "id": "d095bbe7-8523-4e99-8ad0-11c7d73c771a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0997a1b2-dbb4-46e0-bdf9-a11c5ecb3833",
            "compositeImage": {
                "id": "361a6334-2dd2-42e4-8b10-b6c08d585be6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d095bbe7-8523-4e99-8ad0-11c7d73c771a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00c1ef8a-ec06-491f-8dae-8f10dfa9c05a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d095bbe7-8523-4e99-8ad0-11c7d73c771a",
                    "LayerId": "36a576da-6b08-4b2c-b5d0-4cd29139a27f"
                }
            ]
        },
        {
            "id": "b8a0ec29-493c-4580-8f15-9198ff19accc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0997a1b2-dbb4-46e0-bdf9-a11c5ecb3833",
            "compositeImage": {
                "id": "0cea2fb8-0342-4d70-bb61-f6f096b02940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8a0ec29-493c-4580-8f15-9198ff19accc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c2980d1-07ca-48ff-9202-00a760ce7d37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8a0ec29-493c-4580-8f15-9198ff19accc",
                    "LayerId": "36a576da-6b08-4b2c-b5d0-4cd29139a27f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "36a576da-6b08-4b2c-b5d0-4cd29139a27f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0997a1b2-dbb4-46e0-bdf9-a11c5ecb3833",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 0,
    "yorig": 0
}