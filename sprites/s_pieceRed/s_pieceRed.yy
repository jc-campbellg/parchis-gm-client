{
    "id": "e0825460-c9b0-4eed-a03b-9df9ba10244e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pieceRed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7e30eea-f1fa-4874-bc09-1d77ff269cd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0825460-c9b0-4eed-a03b-9df9ba10244e",
            "compositeImage": {
                "id": "b2e8d2f0-ef00-4228-898e-3b54e65bfb01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7e30eea-f1fa-4874-bc09-1d77ff269cd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b7d8275-3555-4d02-8c96-36f307c17590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7e30eea-f1fa-4874-bc09-1d77ff269cd1",
                    "LayerId": "c59ed6f7-44e8-4730-a721-bc480a36aaf1"
                }
            ]
        },
        {
            "id": "97dc3f6a-e396-4337-9a13-a789b320c51f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0825460-c9b0-4eed-a03b-9df9ba10244e",
            "compositeImage": {
                "id": "dc5c64c5-9023-4790-864e-8b146390fbc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97dc3f6a-e396-4337-9a13-a789b320c51f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5c1bb72-c7b4-4392-bdcb-380dfcd8234c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97dc3f6a-e396-4337-9a13-a789b320c51f",
                    "LayerId": "c59ed6f7-44e8-4730-a721-bc480a36aaf1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c59ed6f7-44e8-4730-a721-bc480a36aaf1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0825460-c9b0-4eed-a03b-9df9ba10244e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 26
}