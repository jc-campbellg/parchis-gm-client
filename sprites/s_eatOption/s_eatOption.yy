{
    "id": "9904674b-7b99-4700-bcbd-60b4830466e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_eatOption",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8657c0e-56a3-4209-a302-975981c8d22d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9904674b-7b99-4700-bcbd-60b4830466e8",
            "compositeImage": {
                "id": "71f37ccf-738e-45d8-aa03-6dca2a3023cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8657c0e-56a3-4209-a302-975981c8d22d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32b0686f-e4b0-4ae3-85bc-f53d33fa2fc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8657c0e-56a3-4209-a302-975981c8d22d",
                    "LayerId": "646ebb33-6357-4f7b-b765-0135be52b159"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "646ebb33-6357-4f7b-b765-0135be52b159",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9904674b-7b99-4700-bcbd-60b4830466e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}