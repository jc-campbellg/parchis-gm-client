{
    "id": "a70aa1a4-6670-4ead-bfb9-1fd942c9dbd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_plusNumbers",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ec5f24f-d498-4dff-ab65-786ec0a4c775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a70aa1a4-6670-4ead-bfb9-1fd942c9dbd2",
            "compositeImage": {
                "id": "42347c81-d714-40ff-a645-1c7a4b99b85a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec5f24f-d498-4dff-ab65-786ec0a4c775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62ec770f-907c-45d0-a02a-f1c367188455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec5f24f-d498-4dff-ab65-786ec0a4c775",
                    "LayerId": "cfa8644c-9abf-4d75-b8cd-9df733fed2a5"
                }
            ]
        },
        {
            "id": "7d26a183-6ee2-409a-b310-7d6a45fbbcf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a70aa1a4-6670-4ead-bfb9-1fd942c9dbd2",
            "compositeImage": {
                "id": "9ef374cf-6f20-4c63-8683-2639f81ee157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d26a183-6ee2-409a-b310-7d6a45fbbcf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a25692b9-78cc-4d5d-9272-76f21108946d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d26a183-6ee2-409a-b310-7d6a45fbbcf9",
                    "LayerId": "cfa8644c-9abf-4d75-b8cd-9df733fed2a5"
                }
            ]
        },
        {
            "id": "94dae6e3-2910-4cc4-ae3e-7de7c4d95b5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a70aa1a4-6670-4ead-bfb9-1fd942c9dbd2",
            "compositeImage": {
                "id": "868fb010-c0c1-4493-87ca-0dad2af51401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94dae6e3-2910-4cc4-ae3e-7de7c4d95b5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "638d1a12-607c-442f-bcb2-1e66ff3a5655",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94dae6e3-2910-4cc4-ae3e-7de7c4d95b5b",
                    "LayerId": "cfa8644c-9abf-4d75-b8cd-9df733fed2a5"
                }
            ]
        },
        {
            "id": "cd8428cc-3af5-4a15-91b2-edba3f8991d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a70aa1a4-6670-4ead-bfb9-1fd942c9dbd2",
            "compositeImage": {
                "id": "bdbeb41b-c06d-4742-8583-80030bfcb6b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd8428cc-3af5-4a15-91b2-edba3f8991d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10562c65-fd05-4625-b15b-ffe245c4e362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd8428cc-3af5-4a15-91b2-edba3f8991d4",
                    "LayerId": "cfa8644c-9abf-4d75-b8cd-9df733fed2a5"
                }
            ]
        },
        {
            "id": "1f430b06-e75d-42d1-94ee-a9882c3723ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a70aa1a4-6670-4ead-bfb9-1fd942c9dbd2",
            "compositeImage": {
                "id": "cf164050-1660-488f-9617-a2bbbe08b91f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f430b06-e75d-42d1-94ee-a9882c3723ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "260e009e-e2af-492a-9e3d-a321d0c1567d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f430b06-e75d-42d1-94ee-a9882c3723ef",
                    "LayerId": "cfa8644c-9abf-4d75-b8cd-9df733fed2a5"
                }
            ]
        },
        {
            "id": "3d5e88aa-cbf6-40af-8626-5415adc085e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a70aa1a4-6670-4ead-bfb9-1fd942c9dbd2",
            "compositeImage": {
                "id": "6b21b575-ec6d-4c09-a006-0346abe6f58c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d5e88aa-cbf6-40af-8626-5415adc085e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c34caef2-d054-4f0b-a486-936c79e3ecaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d5e88aa-cbf6-40af-8626-5415adc085e2",
                    "LayerId": "cfa8644c-9abf-4d75-b8cd-9df733fed2a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "cfa8644c-9abf-4d75-b8cd-9df733fed2a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a70aa1a4-6670-4ead-bfb9-1fd942c9dbd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}