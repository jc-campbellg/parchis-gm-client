{
    "id": "675af782-986b-4c04-bda0-87d1ce2528d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_speed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9847a372-55b7-4783-9976-7bb576d484b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "675af782-986b-4c04-bda0-87d1ce2528d9",
            "compositeImage": {
                "id": "d6619d10-1c1d-4b0c-abc8-ec7890c4f5cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9847a372-55b7-4783-9976-7bb576d484b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61d1a6ec-6f21-4606-b432-3506ebcc1c96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9847a372-55b7-4783-9976-7bb576d484b6",
                    "LayerId": "1d8e7530-af32-48f8-b49b-7dd3fb11abb1"
                }
            ]
        },
        {
            "id": "0216a061-8292-4339-a69e-034cbea16ee9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "675af782-986b-4c04-bda0-87d1ce2528d9",
            "compositeImage": {
                "id": "a1c2c141-1a3a-4496-a774-aaeabd0dc60c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0216a061-8292-4339-a69e-034cbea16ee9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe4bd275-d3b4-41f1-8ac8-6bb09e4f54c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0216a061-8292-4339-a69e-034cbea16ee9",
                    "LayerId": "1d8e7530-af32-48f8-b49b-7dd3fb11abb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "1d8e7530-af32-48f8-b49b-7dd3fb11abb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "675af782-986b-4c04-bda0-87d1ce2528d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}