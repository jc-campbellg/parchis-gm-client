{
    "id": "3dcb5d2a-bad2-4f45-a9a5-9481b8c7b045",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_orangeNameTag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c992483a-91d4-4853-bdeb-0ff2bc68cb6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dcb5d2a-bad2-4f45-a9a5-9481b8c7b045",
            "compositeImage": {
                "id": "eafb0521-47f5-4722-b918-0aaee34fa73e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c992483a-91d4-4853-bdeb-0ff2bc68cb6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a5378a6-60e0-4677-beec-76be9ab177f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c992483a-91d4-4853-bdeb-0ff2bc68cb6a",
                    "LayerId": "2a3c93ff-4d35-47de-a9da-71339fba9536"
                }
            ]
        },
        {
            "id": "85a1e9ec-7e0e-49bd-8821-e5b864dae6e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dcb5d2a-bad2-4f45-a9a5-9481b8c7b045",
            "compositeImage": {
                "id": "7cee6aa1-337c-427a-8b2c-5d23aff95b6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85a1e9ec-7e0e-49bd-8821-e5b864dae6e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1adf49f6-5f5f-4e32-8171-5e8e594dc62c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85a1e9ec-7e0e-49bd-8821-e5b864dae6e9",
                    "LayerId": "2a3c93ff-4d35-47de-a9da-71339fba9536"
                }
            ]
        },
        {
            "id": "07f757f6-059c-400e-84cb-7bf2f81652ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dcb5d2a-bad2-4f45-a9a5-9481b8c7b045",
            "compositeImage": {
                "id": "ba59cf7f-2b57-45cf-8a16-a0f1505b2686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07f757f6-059c-400e-84cb-7bf2f81652ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf63d040-d907-496a-83c3-814c46ca5cea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07f757f6-059c-400e-84cb-7bf2f81652ed",
                    "LayerId": "2a3c93ff-4d35-47de-a9da-71339fba9536"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "2a3c93ff-4d35-47de-a9da-71339fba9536",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dcb5d2a-bad2-4f45-a9a5-9481b8c7b045",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 0,
    "yorig": 0
}