{
    "id": "6b2e14b7-51a8-4749-bdb9-9c4212711d7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_usersButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 1,
    "bbox_right": 66,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15be5067-18c5-4f8f-93db-64008b051c29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b2e14b7-51a8-4749-bdb9-9c4212711d7a",
            "compositeImage": {
                "id": "1876b41e-a529-4bb3-9e30-5b817346c589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15be5067-18c5-4f8f-93db-64008b051c29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c0df278-87e2-44ec-8147-689bb0706dd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15be5067-18c5-4f8f-93db-64008b051c29",
                    "LayerId": "c8a1610f-6a75-40de-ad4e-875e466406b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "c8a1610f-6a75-40de-ad4e-875e466406b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b2e14b7-51a8-4749-bdb9-9c4212711d7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 67,
    "xorig": 0,
    "yorig": 0
}