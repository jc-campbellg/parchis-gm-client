{
    "id": "58155437-6b70-45f2-836c-e589798b754c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_yellowNameTag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "839784d2-a0d9-487a-9ff4-a56381c2ea8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58155437-6b70-45f2-836c-e589798b754c",
            "compositeImage": {
                "id": "7f99b007-3cdb-4fa1-b659-6925d8f3230d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "839784d2-a0d9-487a-9ff4-a56381c2ea8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ae20412-ae61-4d9a-b1d1-1c8b413e932f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "839784d2-a0d9-487a-9ff4-a56381c2ea8e",
                    "LayerId": "1e2b1292-939f-492d-9cea-d781293c8e34"
                }
            ]
        },
        {
            "id": "926ba6f0-6739-44b9-a7f3-bc0e7f76c87f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58155437-6b70-45f2-836c-e589798b754c",
            "compositeImage": {
                "id": "96ba859a-e881-4d26-8c0c-08c75f3387c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "926ba6f0-6739-44b9-a7f3-bc0e7f76c87f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "639ce0a9-5873-4156-bc08-ccfa8fe254ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "926ba6f0-6739-44b9-a7f3-bc0e7f76c87f",
                    "LayerId": "1e2b1292-939f-492d-9cea-d781293c8e34"
                }
            ]
        },
        {
            "id": "22862a73-2470-4764-aa48-db21d83f39df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58155437-6b70-45f2-836c-e589798b754c",
            "compositeImage": {
                "id": "68a717cf-f09b-4272-a77e-8f1a79aaff26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22862a73-2470-4764-aa48-db21d83f39df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75c9c7ef-2b5c-4f11-a000-ee38f46e9183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22862a73-2470-4764-aa48-db21d83f39df",
                    "LayerId": "1e2b1292-939f-492d-9cea-d781293c8e34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "1e2b1292-939f-492d-9cea-d781293c8e34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58155437-6b70-45f2-836c-e589798b754c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 0,
    "yorig": 0
}