{
    "id": "6f4a178e-46e6-44e4-ab46-40dcf284ee38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_radioOption",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 1,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83c60f0d-2d86-4c05-be9c-5138cd43ca48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f4a178e-46e6-44e4-ab46-40dcf284ee38",
            "compositeImage": {
                "id": "75b2021e-569d-4b25-845d-61d7246091dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83c60f0d-2d86-4c05-be9c-5138cd43ca48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e928e90a-6b1b-4325-a175-d8120194bb49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83c60f0d-2d86-4c05-be9c-5138cd43ca48",
                    "LayerId": "9bbf2d97-134f-410f-8f36-70488e8c87eb"
                }
            ]
        },
        {
            "id": "960027e7-8f8b-4e28-93bd-eec90d6eb2df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f4a178e-46e6-44e4-ab46-40dcf284ee38",
            "compositeImage": {
                "id": "fa2ce1ab-932e-48a4-b9d6-af47dfcc95a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "960027e7-8f8b-4e28-93bd-eec90d6eb2df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58a50270-7b18-4f91-a735-8a1c8aa1a979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "960027e7-8f8b-4e28-93bd-eec90d6eb2df",
                    "LayerId": "9bbf2d97-134f-410f-8f36-70488e8c87eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "9bbf2d97-134f-410f-8f36-70488e8c87eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f4a178e-46e6-44e4-ab46-40dcf284ee38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}