{
    "id": "9d28cf79-5402-4ed3-ba3f-0685f4179d91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_playButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 61,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1ff4595-cbb5-4ea0-b8b2-844e98dc4cf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d28cf79-5402-4ed3-ba3f-0685f4179d91",
            "compositeImage": {
                "id": "8106a5df-3bea-47b8-a9f2-75291ca4ba2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1ff4595-cbb5-4ea0-b8b2-844e98dc4cf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c589c11-a7c0-4ae9-adce-cf3899ba0ad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1ff4595-cbb5-4ea0-b8b2-844e98dc4cf3",
                    "LayerId": "9c00b545-1052-4d11-8c6c-04382bb8b8c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "9c00b545-1052-4d11-8c6c-04382bb8b8c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d28cf79-5402-4ed3-ba3f-0685f4179d91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 62,
    "xorig": 0,
    "yorig": 0
}