{
    "id": "15836533-3c24-4b48-9426-8891c64a425c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_diceButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 70,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd8cf6ba-2037-46a6-b92f-58f2f1c677ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15836533-3c24-4b48-9426-8891c64a425c",
            "compositeImage": {
                "id": "23ee58a9-1f17-4c97-bb80-0e9c69348469",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd8cf6ba-2037-46a6-b92f-58f2f1c677ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb01dd60-0dc4-43f4-a3f5-7a7611ad24c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd8cf6ba-2037-46a6-b92f-58f2f1c677ea",
                    "LayerId": "f66054d0-ecbe-4395-bbfd-aac23f5a9b04"
                }
            ]
        },
        {
            "id": "bc31586e-0458-4af7-995b-fc72df38ee1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15836533-3c24-4b48-9426-8891c64a425c",
            "compositeImage": {
                "id": "845556ca-d52d-4684-b09d-f422fb000b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc31586e-0458-4af7-995b-fc72df38ee1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfcbcbff-54be-45c6-baa2-06eb03c27bb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc31586e-0458-4af7-995b-fc72df38ee1d",
                    "LayerId": "f66054d0-ecbe-4395-bbfd-aac23f5a9b04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "f66054d0-ecbe-4395-bbfd-aac23f5a9b04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15836533-3c24-4b48-9426-8891c64a425c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 71,
    "xorig": 0,
    "yorig": 0
}