{
    "id": "bf718da7-56e1-4a10-884a-673f6d32577d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_purpleNameTag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37234966-8f4a-444c-a25f-fc343eae3ebb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf718da7-56e1-4a10-884a-673f6d32577d",
            "compositeImage": {
                "id": "7b238d9a-90d7-4377-a5b8-b92f66e1331a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37234966-8f4a-444c-a25f-fc343eae3ebb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c55c84c-8a4c-4bc8-b16c-bab786f9ba44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37234966-8f4a-444c-a25f-fc343eae3ebb",
                    "LayerId": "04d20361-82a1-4484-b5aa-7ab64d5049e0"
                }
            ]
        },
        {
            "id": "4630850f-3526-443e-9e7d-8896cf194af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf718da7-56e1-4a10-884a-673f6d32577d",
            "compositeImage": {
                "id": "ecc625d8-96d8-4c76-9ad4-82e34f07cea9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4630850f-3526-443e-9e7d-8896cf194af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d29fd01-4840-4841-8e8b-49dadcc27cea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4630850f-3526-443e-9e7d-8896cf194af7",
                    "LayerId": "04d20361-82a1-4484-b5aa-7ab64d5049e0"
                }
            ]
        },
        {
            "id": "a1839c88-8a8b-4b58-bb74-abbf329d073b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf718da7-56e1-4a10-884a-673f6d32577d",
            "compositeImage": {
                "id": "5ec6afbf-1587-47c6-88c0-fb2fae66bdd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1839c88-8a8b-4b58-bb74-abbf329d073b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47716cb8-0961-44f2-bea9-e693982f535a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1839c88-8a8b-4b58-bb74-abbf329d073b",
                    "LayerId": "04d20361-82a1-4484-b5aa-7ab64d5049e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "04d20361-82a1-4484-b5aa-7ab64d5049e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf718da7-56e1-4a10-884a-673f6d32577d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 0,
    "yorig": 0
}