{
    "id": "1c55bbc1-1868-4ef6-ac80-b54fbc30abec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_startButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 0,
    "bbox_right": 291,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8416c56-a1e2-4bce-81b1-50371afce7e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c55bbc1-1868-4ef6-ac80-b54fbc30abec",
            "compositeImage": {
                "id": "3ed71ab5-2a65-4f6c-8146-3788fd7f820f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8416c56-a1e2-4bce-81b1-50371afce7e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0744b75d-718a-4da6-b683-b491dd78d89f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8416c56-a1e2-4bce-81b1-50371afce7e4",
                    "LayerId": "39912377-6008-45cb-8234-b6e334a5e3ca"
                }
            ]
        },
        {
            "id": "3e2d6d0e-ddb1-4c4e-a887-688bb2fbbcc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c55bbc1-1868-4ef6-ac80-b54fbc30abec",
            "compositeImage": {
                "id": "3ba3682f-17ab-4352-8c1c-a1dd20602a64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e2d6d0e-ddb1-4c4e-a887-688bb2fbbcc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff8d9f75-afb2-4afa-8285-4bbab95e97db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e2d6d0e-ddb1-4c4e-a887-688bb2fbbcc4",
                    "LayerId": "39912377-6008-45cb-8234-b6e334a5e3ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 83,
    "layers": [
        {
            "id": "39912377-6008-45cb-8234-b6e334a5e3ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c55bbc1-1868-4ef6-ac80-b54fbc30abec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 292,
    "xorig": 0,
    "yorig": 0
}