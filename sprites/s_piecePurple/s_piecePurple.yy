{
    "id": "abede685-7a71-4fc2-8e5f-39b11363ad84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_piecePurple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6fe0b79-c52d-461c-9c7f-af46a9efdb1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abede685-7a71-4fc2-8e5f-39b11363ad84",
            "compositeImage": {
                "id": "df9ddfbb-b120-41d2-a2f5-c31f64d1b2b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6fe0b79-c52d-461c-9c7f-af46a9efdb1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af1db028-b73e-41f3-986e-2a3ffec7e580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6fe0b79-c52d-461c-9c7f-af46a9efdb1d",
                    "LayerId": "0f25a4e1-d66c-4e6a-8713-3ca1a77af163"
                }
            ]
        },
        {
            "id": "91441ab3-6a5d-4004-bb5b-2ffd64f32709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abede685-7a71-4fc2-8e5f-39b11363ad84",
            "compositeImage": {
                "id": "cd8df879-9ce5-4677-b1f9-c104ee92c319",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91441ab3-6a5d-4004-bb5b-2ffd64f32709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7945933a-543a-483e-af97-25fe5eb2ef93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91441ab3-6a5d-4004-bb5b-2ffd64f32709",
                    "LayerId": "0f25a4e1-d66c-4e6a-8713-3ca1a77af163"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "0f25a4e1-d66c-4e6a-8713-3ca1a77af163",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abede685-7a71-4fc2-8e5f-39b11363ad84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 26
}