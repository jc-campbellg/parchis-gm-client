{
    "id": "238f9900-a531-4974-8215-4da6b03fc844",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_selectColor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 2,
    "bbox_right": 437,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f8b5ae3-eee1-4b61-8f81-f3de2c40ad87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "238f9900-a531-4974-8215-4da6b03fc844",
            "compositeImage": {
                "id": "1b5d4ee9-7a0e-458f-82b2-b24f8ee54829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f8b5ae3-eee1-4b61-8f81-f3de2c40ad87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe280c35-03ce-4bca-8ae1-bb500bc0191f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f8b5ae3-eee1-4b61-8f81-f3de2c40ad87",
                    "LayerId": "6d878f01-7f95-4713-b249-ff39e661cc88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "6d878f01-7f95-4713-b249-ff39e661cc88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "238f9900-a531-4974-8215-4da6b03fc844",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 440,
    "xorig": 0,
    "yorig": 0
}