{
    "id": "ab960c00-bc73-41c6-9386-0e9413a786b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_settingsButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 68,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "811a343c-d9de-4229-8b22-989f903344aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab960c00-bc73-41c6-9386-0e9413a786b3",
            "compositeImage": {
                "id": "b5fbbc8c-d4b2-4ba4-8bf2-ba51f922f7a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "811a343c-d9de-4229-8b22-989f903344aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d208e0cd-14b1-46fa-883b-3fe14d630ab1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "811a343c-d9de-4229-8b22-989f903344aa",
                    "LayerId": "1a141cb2-fae5-428e-9f27-f780224fdaca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "1a141cb2-fae5-428e-9f27-f780224fdaca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab960c00-bc73-41c6-9386-0e9413a786b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 69,
    "xorig": 0,
    "yorig": 0
}