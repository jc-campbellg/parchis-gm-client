{
    "id": "ce5274e6-36ac-41e7-bb7d-34707e449c39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_board",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 600,
    "bbox_left": 0,
    "bbox_right": 590,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93fae7fe-4e41-4384-b2c5-00acf939dc9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce5274e6-36ac-41e7-bb7d-34707e449c39",
            "compositeImage": {
                "id": "6a22b7e4-d3e3-45ec-bdce-2e1d69c74179",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93fae7fe-4e41-4384-b2c5-00acf939dc9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "838bb693-b424-4d7d-9a3c-96755bb08f51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93fae7fe-4e41-4384-b2c5-00acf939dc9c",
                    "LayerId": "13b015f7-6e26-4fc8-891d-0da3daad7ccf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 601,
    "layers": [
        {
            "id": "13b015f7-6e26-4fc8-891d-0da3daad7ccf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce5274e6-36ac-41e7-bb7d-34707e449c39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 591,
    "xorig": 0,
    "yorig": 0
}