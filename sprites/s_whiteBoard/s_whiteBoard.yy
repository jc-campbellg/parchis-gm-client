{
    "id": "bdbccc7b-cff6-4da4-84ce-1fce9cc225e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_whiteBoard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 387,
    "bbox_left": 0,
    "bbox_right": 243,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ec2bb73-7dda-4158-b9a3-ee1d318526e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdbccc7b-cff6-4da4-84ce-1fce9cc225e8",
            "compositeImage": {
                "id": "f45419bf-61f0-49ba-9269-a9fbed0c1e68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ec2bb73-7dda-4158-b9a3-ee1d318526e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ced6a6cb-2621-441f-b85b-a183668f0c65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ec2bb73-7dda-4158-b9a3-ee1d318526e1",
                    "LayerId": "09dacbdf-2ba4-4b67-8f4b-6ef104e5331a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 388,
    "layers": [
        {
            "id": "09dacbdf-2ba4-4b67-8f4b-6ef104e5331a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bdbccc7b-cff6-4da4-84ce-1fce9cc225e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 245,
    "xorig": 0,
    "yorig": 0
}