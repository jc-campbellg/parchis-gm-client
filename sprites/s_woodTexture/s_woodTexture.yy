{
    "id": "0282d76d-1115-42be-8bde-85f6bbdd7813",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_woodTexture",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e0a24e4-e7fb-4a82-8ebe-334f606cc8b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0282d76d-1115-42be-8bde-85f6bbdd7813",
            "compositeImage": {
                "id": "8a8dd2e4-9057-4044-b299-0b2fad5df787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e0a24e4-e7fb-4a82-8ebe-334f606cc8b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16fcd23f-4367-4cb5-bada-1e2d20fabc86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e0a24e4-e7fb-4a82-8ebe-334f606cc8b5",
                    "LayerId": "dbcd550c-a77c-4548-98bb-22b63210db59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "dbcd550c-a77c-4548-98bb-22b63210db59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0282d76d-1115-42be-8bde-85f6bbdd7813",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}