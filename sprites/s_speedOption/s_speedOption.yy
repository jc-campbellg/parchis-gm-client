{
    "id": "8b2eaca4-3969-45ad-adc8-529c69215b7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_speedOption",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "916aa172-3517-4f17-962c-161d074d81e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b2eaca4-3969-45ad-adc8-529c69215b7b",
            "compositeImage": {
                "id": "9171ff1b-f5d9-4619-b62f-4d3eedea9654",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "916aa172-3517-4f17-962c-161d074d81e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc16f0d6-94ec-4e7b-97ab-844ce55fab34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "916aa172-3517-4f17-962c-161d074d81e6",
                    "LayerId": "9cef22ba-382d-4eaa-a514-ebae9eacfbf6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "9cef22ba-382d-4eaa-a514-ebae9eacfbf6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b2eaca4-3969-45ad-adc8-529c69215b7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}