{
    "id": "9ed91033-04fc-4694-95d0-51258a8cc712",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_exitButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 0,
    "bbox_right": 291,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "647d8246-fa9d-474f-b2e5-73eed9a13948",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ed91033-04fc-4694-95d0-51258a8cc712",
            "compositeImage": {
                "id": "d80f22e1-e568-49c5-b2b6-86423a9b0cd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647d8246-fa9d-474f-b2e5-73eed9a13948",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0970a338-d171-44d7-9b20-7a47863b2e6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647d8246-fa9d-474f-b2e5-73eed9a13948",
                    "LayerId": "9db3a95c-6c09-4584-b7c8-116ff9fc83d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 83,
    "layers": [
        {
            "id": "9db3a95c-6c09-4584-b7c8-116ff9fc83d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ed91033-04fc-4694-95d0-51258a8cc712",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 292,
    "xorig": 0,
    "yorig": 0
}