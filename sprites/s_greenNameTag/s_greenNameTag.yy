{
    "id": "0a4cf9e7-04ad-4798-bf17-1ef1a8f2fbbc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_greenNameTag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b690a866-e66a-4212-ba58-6fbb8c8088be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a4cf9e7-04ad-4798-bf17-1ef1a8f2fbbc",
            "compositeImage": {
                "id": "1b201047-cf71-4cb4-92ed-a451a80be96d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b690a866-e66a-4212-ba58-6fbb8c8088be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fae7959d-a5a2-4b06-b130-dfe1bf625d1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b690a866-e66a-4212-ba58-6fbb8c8088be",
                    "LayerId": "193e538a-02db-48ce-8c03-c80560e48617"
                }
            ]
        },
        {
            "id": "8a837f3e-8e56-4e61-86bc-9ca8cfed3e4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a4cf9e7-04ad-4798-bf17-1ef1a8f2fbbc",
            "compositeImage": {
                "id": "89b2e089-632f-474e-980b-ce196b33b1e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a837f3e-8e56-4e61-86bc-9ca8cfed3e4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d498d46b-72a3-450e-bb97-43591d32816d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a837f3e-8e56-4e61-86bc-9ca8cfed3e4a",
                    "LayerId": "193e538a-02db-48ce-8c03-c80560e48617"
                }
            ]
        },
        {
            "id": "66718ddb-3cdb-48c3-a46d-c16b6bf7011c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a4cf9e7-04ad-4798-bf17-1ef1a8f2fbbc",
            "compositeImage": {
                "id": "5999a77e-d6d7-40c1-a565-7e8b389b92eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66718ddb-3cdb-48c3-a46d-c16b6bf7011c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f3b50c5-6a2c-414e-b464-c527d7b2f619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66718ddb-3cdb-48c3-a46d-c16b6bf7011c",
                    "LayerId": "193e538a-02db-48ce-8c03-c80560e48617"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "193e538a-02db-48ce-8c03-c80560e48617",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a4cf9e7-04ad-4798-bf17-1ef1a8f2fbbc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 0,
    "yorig": 0
}