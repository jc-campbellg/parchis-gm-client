{
    "id": "de09a689-7b0f-46c4-b922-1dc027170aa8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_closeButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f676e8f-c768-4985-8ed2-4b2d3d727fee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de09a689-7b0f-46c4-b922-1dc027170aa8",
            "compositeImage": {
                "id": "4ec07380-aa56-4e8c-abf6-7910a4ddd3da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f676e8f-c768-4985-8ed2-4b2d3d727fee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7706bd7b-89f9-42f5-8d77-3e60afa3f235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f676e8f-c768-4985-8ed2-4b2d3d727fee",
                    "LayerId": "935bcb90-ba8f-4c9f-83d2-eaf720f2a9b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "935bcb90-ba8f-4c9f-83d2-eaf720f2a9b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de09a689-7b0f-46c4-b922-1dc027170aa8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 0
}