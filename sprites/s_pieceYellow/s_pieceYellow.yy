{
    "id": "34840d74-b986-4d94-a680-8f105cee2aab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pieceYellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ba27b32-b40c-4272-98fb-e1a82aa164bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34840d74-b986-4d94-a680-8f105cee2aab",
            "compositeImage": {
                "id": "433adafa-2781-4d6e-9dcc-87a2ecbea5ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ba27b32-b40c-4272-98fb-e1a82aa164bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2add597b-5303-4d58-bdab-1e265c2c02f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ba27b32-b40c-4272-98fb-e1a82aa164bb",
                    "LayerId": "b6ad0785-8d5e-44fe-922a-2e89b816b971"
                }
            ]
        },
        {
            "id": "e3eedbf7-c8ee-4d6a-a4cd-691521925ec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34840d74-b986-4d94-a680-8f105cee2aab",
            "compositeImage": {
                "id": "43e59287-0194-4a59-90eb-e359dc3cdd7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3eedbf7-c8ee-4d6a-a4cd-691521925ec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f00690b-1e5c-4b15-bf49-32da09671124",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3eedbf7-c8ee-4d6a-a4cd-691521925ec9",
                    "LayerId": "b6ad0785-8d5e-44fe-922a-2e89b816b971"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b6ad0785-8d5e-44fe-922a-2e89b816b971",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34840d74-b986-4d94-a680-8f105cee2aab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 9,
    "yorig": 26
}