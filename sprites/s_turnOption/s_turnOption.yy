{
    "id": "4173582f-2896-4fc5-b278-43d18989486b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_turnOption",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b5ea605-d533-4e96-ae13-a6e5ac5f6329",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4173582f-2896-4fc5-b278-43d18989486b",
            "compositeImage": {
                "id": "e587edff-5618-4c26-816d-ae396852dd00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b5ea605-d533-4e96-ae13-a6e5ac5f6329",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4abb2195-6826-4c00-861f-4fd5f037a5a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b5ea605-d533-4e96-ae13-a6e5ac5f6329",
                    "LayerId": "ce61b66b-6421-4db7-a81a-f62a85b34066"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "ce61b66b-6421-4db7-a81a-f62a85b34066",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4173582f-2896-4fc5-b278-43d18989486b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 0,
    "yorig": 0
}