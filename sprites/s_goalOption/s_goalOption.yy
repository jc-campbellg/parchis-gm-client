{
    "id": "9c56e56c-4e52-44c2-8fa1-6146602c3d96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_goalOption",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 98,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "962e04f6-9f60-4593-a9f8-8151433d8d25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c56e56c-4e52-44c2-8fa1-6146602c3d96",
            "compositeImage": {
                "id": "e56b76d4-0033-4689-9fc4-6540888dd27f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "962e04f6-9f60-4593-a9f8-8151433d8d25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d958d1d3-f73e-47d4-90cf-84a69020a14a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "962e04f6-9f60-4593-a9f8-8151433d8d25",
                    "LayerId": "22637a8a-8717-4cc0-9ccd-f04dc969491c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "22637a8a-8717-4cc0-9ccd-f04dc969491c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c56e56c-4e52-44c2-8fa1-6146602c3d96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 99,
    "xorig": 0,
    "yorig": 0
}