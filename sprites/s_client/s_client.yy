{
    "id": "0eb1a118-aaf8-46ac-804b-b97c50ea780a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_client",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18f1ec23-6669-4253-a835-6800a0739f83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0eb1a118-aaf8-46ac-804b-b97c50ea780a",
            "compositeImage": {
                "id": "bf13fb19-2cf6-4cee-b0d9-8e3338644177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f1ec23-6669-4253-a835-6800a0739f83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "712a947f-0747-406f-8c4b-36ebed6bf2e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f1ec23-6669-4253-a835-6800a0739f83",
                    "LayerId": "f467ab78-466b-4cd0-97f4-6745bd964708"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "f467ab78-466b-4cd0-97f4-6745bd964708",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0eb1a118-aaf8-46ac-804b-b97c50ea780a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 736,
    "yorig": 173
}