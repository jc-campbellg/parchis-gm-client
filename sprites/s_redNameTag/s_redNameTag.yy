{
    "id": "004f5863-9dc0-4ac6-bd4a-cc48470b8c97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_redNameTag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1aeecbbf-258e-4845-a6d2-9861a6dfefb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004f5863-9dc0-4ac6-bd4a-cc48470b8c97",
            "compositeImage": {
                "id": "7d3e844e-70e8-48b2-a9b4-834d0a62b994",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aeecbbf-258e-4845-a6d2-9861a6dfefb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17e29600-4ae8-4739-9d86-a1a9f02c8913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aeecbbf-258e-4845-a6d2-9861a6dfefb3",
                    "LayerId": "dc54611b-00a3-49e7-8025-9549ab315016"
                }
            ]
        },
        {
            "id": "5e1dd733-f7b9-4cc3-b648-dc2ffeb0c726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004f5863-9dc0-4ac6-bd4a-cc48470b8c97",
            "compositeImage": {
                "id": "af3b76ad-e0f9-44e6-9f16-6be8cd67e0c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e1dd733-f7b9-4cc3-b648-dc2ffeb0c726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ae9eafc-88ab-437b-a25a-d8818de5a764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e1dd733-f7b9-4cc3-b648-dc2ffeb0c726",
                    "LayerId": "dc54611b-00a3-49e7-8025-9549ab315016"
                }
            ]
        },
        {
            "id": "93c9e3dc-e18e-43da-97b9-b9a22bc24754",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004f5863-9dc0-4ac6-bd4a-cc48470b8c97",
            "compositeImage": {
                "id": "8519938c-ec2f-4753-a810-e2642982755f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93c9e3dc-e18e-43da-97b9-b9a22bc24754",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b899517-cade-403a-9d09-bad310737e6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93c9e3dc-e18e-43da-97b9-b9a22bc24754",
                    "LayerId": "dc54611b-00a3-49e7-8025-9549ab315016"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "dc54611b-00a3-49e7-8025-9549ab315016",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "004f5863-9dc0-4ac6-bd4a-cc48470b8c97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 0,
    "yorig": 0
}